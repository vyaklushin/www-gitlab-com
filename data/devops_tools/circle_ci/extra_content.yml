pages:
  - path: bdm
    content:
      header: includes/devops-tools/headers/no-button.html.haml
      page_title: CircleCI vs. Gitlab for Business Decision Makers
      css: extra-content-devops-tools.css
      page_body: |
        ## CircleCI Strengths

        * YAML File:  CI build jobs are configured in a YAML file
        * Security Accreditations: CircleCI is FedRamp authorized and SOC 2 compliant
        * Forrester Wave: CircleCI was recognized as a Leader in The Forrester Wave™: Cloud-Native Continuous Integration Tools, Q3 2019: speed, scale, security, and compliance.
        * Catering to Large Enterprise:
          - CircleCI provides a large number of preconfigured environments, which is highly favored by enterprises
          - Despite a team size of just over 300 employees, CircleCI has a Customer Success Team and Enterprise support packages, also favored by enterprises
        * iOS application testing on macOS: CircleCI offers support for building and testing iOS projects in macOS virtual machines (available on CircleCI Cloud only, not currently available on self-hosted installations).
          - GitLab is actively working on integrating this functionality, more details can be found [here](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/5720)

        ## CircleCI Limitations and Challenges


        * Free Plan Trap: Their free plan offers a generous amount of free credits, 2,500/week.  However, these credits are used to pay for CI run times across medium sized Linux and Windows machines only; and are used for Orb usage, Workspaces and Dependency Caching.
        * No Single Integrated DevOps Application: CircleCI is a tool that automates the Continuous Integration stage of the Software Development Life Cycle.  To extend the functionality beyond CI, integration with third party plugins is required.  Plugins are expensive to maintain, secure, and upgrade. In contrast, GitLab is [open core](https://about.gitlab.com/blog/2016/07/20/gitlab-is-open-core-github-is-closed-source/) and anyone can contribute changes directly to the codebase, which once merged would be automatically tested and maintained with every change.
        * Missing Enterprise Features: CircleCI lacks native support for key enterprise features such as Feature Flags, Kubernetes Support and Canary Deployments.
        * Hybride CI: CircleCI lacks the ability to orchestrate a customers private server CI builds with their cloud hosted CI server

        ## GitLab Strengths over CircleCI

        |         GitLab Differentiator        |                                                                                                                                                                                                                Why Is This Important?                                                                                                                                                                                                                |
        |:------------------------------------:|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
        | Intuitive and Customizable Dashboard | CircleCI customers have reported to Forrester analyst the desire to have a more customizable UI experience when using CircleCI                                                                                                                                                                                                                                                                                                                       |
        | Fully Integrated CI/CD               | Fully integrated, means less context switching between tools and more time delivering software.  CircleCI is a CI tool that uses plugins for CD and other functions                                                                                                                                                                                                                                                                                  |
        | Integrated Container Registry        | Fully integrated container registry offers more security, governance and efficient management.  CircleCI does not offer this                                                                                                                                                                                                                                                                                                                         |
        | Local Artifact Storage               | Fully integrated artifact storage  offers more security, governance and efficient management.  CircleCI does not offer this, they store Artifacts on Amazon S3.                                                                                                                                                                                                                                                                                      |
        | Rich Insights and Analytics          | Detailed analytics and insights helps customers improve software development times.  Aggregating insights and analytics across projects provides users with easy access to valuable information.  GitLab does this well while CircleCI does not.  CircleCI’s on-prem CI server offers very shallow insights.  Their SaaS CI solution provides deeper insights but requires a separate self-managed server and is not integrated into their Cloud UI. |
        | Hybrid CI Model                      | More and more customers are attracted to SaaS tools but don’t want to place their source code in the cloud.  For this reason Hybrid CI solutions that combine a cloud-native SaaS CI server with on-premises agents are becoming more desirable because it simplifies tool maintenance while dodging security concerns.                                                                                                                              |
        | Single Application                   | Built-in CI/CD transforms how you deliver today and simultaneously prepares your SDLC for evolving needs tomorrow.                                                                                                                                                                                                                                                                                                                                   |
        | End-to-End Automation                | Powerful enough alone yet flexible enough to integrate with other tools, GitLab ensures quality code gets to production faster.                                                                                                                                                                                                                                                                                                                      |

        ## CircleCI vs GitLab Summary

        |  GitLab |                                                                            |  CircleCI |
        |:-------:|:--------------------------------------------------------------------------:|:---------:|
        | YES     |                            Self Hosted and .com                            | YES       |
        | YES     |               Ecosystem (https://about.gitlab.com/partners/)               | YES       |
        | No      |                                 Marketplace                                | YES       |
        | YES     |            CD fully integrated - no 3rd party Plugins/tools need           | No        |
        | YES     |                Built in Kubernetes Deployment and Monitoring               | No        |
        | YES     |                       Auto CI  Pipeline Configuration                      | No        |
        | YES     |      Built in CI Security Scanning - no 3rd party Plugins/tools needed     | No        |
        | YES     |           Security Dashboard enabling Security Team collaboration          | No        |
        | YES     | Supports Hybrid CI Orchestration Model (SaaS CI Server  and On-Prem Agent) | No        |

  - path: license
    content:
      header: includes/devops-tools/headers/no-button.html.haml
      page_title: CircleCI License Overview
      css: extra-content-devops-tools.css
      page_body: |
          ## Pricing Plans

          * Free ($0)
            - 2,500 free credits/week
            - Run 1 job at a time
            - Build on Linux or Window (no macOS support)
            - Does not support flexible payment option (credit card or invoice)
            - Support Options:
            - Community
          * Performance (starting at $30/month)
            - $15/month for the first 3 users and then $15/month for each added user
            - Starts at 25,000 credits for $15
            - Does not support flexible payment option (credit card or invoice)
            - Support Options:
              - Community
              - Support Portal
              - Global Ticket Support
              - 8×5 SLAs available
              - Account Team: Customer Success Manager
          * Custom
            - Plan customized for the customer
            - Support Options:
              - Community
              - Support Portal
              - Global Ticket Support
              - 24×5 and 24×7 SLAs available
              - Account Team: Customer Success Manager, Customer Success Engineer, Implementation Manager

  - path: key-differentiators
    content:
      header: includes/devops-tools/headers/no-button.html.haml
      page_title: CircleCI vs. GitLab Key Differentiators
      css: extra-content-devops-tools.css
      page_body: |
        ## GitLab vs. CircleCI Analytics: Repo Insights

        ![GitLab CircleCI Comparison Chart](/images/devops-tools/repo-insights.png)

        ## GitLab vs. CircleCI Analytics: Per Project Insights

        ![GitLab CircleCI Comparison Chart](/images/devops-tools/per-project-insights.png)

        ![GitLab CircleCI Comparison Chart](/images/devops-tools/per-project-insights2.png)

        ## CircleCI CI SaaS Analytics

        ![GitLab CircleCI Comparison Chart](/images/devops-tools/saas-analytics.png)

        ## GitLab vs. CircleCI Dashboard: Viewing Pipelines

        ![GitLab CircleCI Comparison Chart](/images/devops-tools/viewing-pipelines.png)

        ## GitLab vs. CircleCI Dashboard: Viewing Job Logs

        ![GitLab CircleCI Comparison Chart](/images/devops-tools/viewing-job-logs.png)

        ## GitLab vs. CircleCI Hybrid CI Orchestration

        ![GitLab CircleCI Comparison Chart](/images/devops-tools/hybrid-ci.png)
  - path: workflow-stages
    content:
      header: includes/devops-tools/headers/no-button.html.haml
      page_title: CircleCI Workflows vs. GitLab Stages
      css: extra-content-devops-tools.css
      page_body: |

        ## CircleCI determines the run order for jobs with workflows. This is also used to determine concurrent, sequential, scheduled, or manual runs. The equivalent function in GitLab CI/CD is called stages.

        ## This table compares CircleCI Workflows to GitLab Stages

           |                                                           Features                                                           	| CircleCI Workflows 	| GitLab Stages 	|
           |:----------------------------------------------------------------------------------------------------------------------------:	|--------------------	|---------------	|
           | <br>Configuration File                                                                                                       	|     config.yml     	| gitlab-ci.yml 	|
           | Run Job Independently                                                                                                        	|         Yes        	|      Yes      	|
           | Troubleshoot Jobs Independently                                                                                              	|         Yes        	|      Yes      	|
           | Fan-Out/Fan-In<br>Run a common job first, then run multiple concurrent jobs (Fan-Out), then run another command job (Fan-In) 	|         Yes        	|      Yes      	|
           | Trigger on run on manual Approval                                                                                            	|         Yes        	|      Yes      	|
           | Schedule runs                                                                                                                	|         Yes        	|      Yes      	|
           | Use a context to share environment variables.                                                                                	|         Yes        	|      Yes      	|
           | Execute jobs at branch level                                                                                                 	|         Yes        	|      Yes      	|
           | Execute workflows on a Git tag                                                                                               	|         Yes        	|      Yen      	|
           | Cache files in a container filesystem to share among jobs                                                                    	|         Yes        	|      Yes      	|
           | Rerun a failed job                                                                                                           	|         Yes        	|      Yes      	|
           | Use API with Workflows                                                                                                       	|         Yes        	|      Yes      	|
           | Auto-cancel feature with Workflows                                                                                           	|       <br>Yes      	|    <br>Yes    	|
           | Launch jobs on multiple environments at one time?  (example Linux and Mac)                                                   	|       <br>Yes      	|      Yes      	|
           | Split .yml final into different files                                                                                        	|       <br>No       	|    <br>Yes    	|
  - path: product-gaps
    content:
      header: includes/devops-tools/headers/no-button.html.haml
      page_title: CircleCI Product Gaps
      css: extra-content-devops-tools.css
      page_body: |
          ## The following table lists CircleCI product gaps and its associated impact.  This information is derived from user feedback in the CircleCI Community Group

            **Source:** [CircleCI Community Forum](https://discuss.circleci.com/c/bug-reports/product-feedback/98)

            | CircleCI Issue                                  	| Impact                  	| Details                                                                                                                                                                                                                                         	| Reference                                                                                                                                                                                 	|
            |-------------------------------------------------	|-------------------------	|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	|
            | Open Source Project authentication Requirements 	| Poor User Experience    	| Viewing a workflow for an open source project requires authentication. A person would need to create an account, or link an account, to view the result of a build.                                                                             	| [CircleCI Community Discussion](https://discuss.circleci.com/t/open-source-project-build-log-requires-authentication-to-view/37287)                                                       	|
            | Timestamp on build logs                         	| Limited Insight         	| It’s unclear how long each command in a build took to execute.   A timestamp timestamp added to each line in a build log would provide the necessary insight into how long it takes to execute each command.                                    	| [CircleCI Community Discussion](https://discuss.circleci.com/t/timestamps-on-build-logs-repeat-request/37173/4)                                                                           	|
            | File type support                               	| Poor User Experience    	| In addition to config.yml, config.yaml should also be supported                                                                                                                                                                                 	| [CircleCI Community Discussion](https://discuss.circleci.com/t/support-config-yaml-because-they-officially-recommend-yaml/35938)                                                          	|
            | Unable to remove team members                   	| Increased security risk 	| Removing a team member from the repository does not remove the user from the CircleCI project.  Users will still be listed on the CircleCI team page as a teammate.                                                                             	| [CircleCI Community Discussion](https://discuss.circleci.com/t/removing-teammates-not-possible/7579)                                                                                      	|
            | **New UI Updates**                              	|                         	|                                                                                                                                                                                                                                                 	|                                                                                                                                                                                           	|
            | Workflow testing insights                       	| Limited Insight         	| It would be helpful to see the total number of tests passed.                                                                                                                                                                                    	| [CircleCI Community Discussion](https://discuss.circleci.com/t/improvement-new-ui-display-test-results-whereever-possible/36465)                                                          	|
            | Pipeline filtering                              	| Poor User Experience    	| The new UI updates limit the filtering options available for the Pipelines page.  More filtering options will improve the user experience on the Pipeline page.  For example, the ability to filter by status or the ability to filter workflow 	| [CircleCI Community Discussion](https://discuss.circleci.com/t/new-ui-filters/35730)<br>[CircleCI Community Discussion](https://discuss.circleci.com/t/ability-to-filter-by-status/35701) 	|
            | Single Pipeline View                            	| Poor User Experience    	| The new UI does not provide the ability to view a Pipeline on a single webpage.                                                                                                                                                                 	| [CircleCI Community Discussion](https://discuss.circleci.com/t/webpage-view-of-a-single-pipeline/35381)                                                                                   	|
            | Poorly Re-designed (General Feedback)           	| Poor User Experience    	| The new UI has been receiving poor reviews                                                                                                                                                                                                      	| [CircleCI Community Discussion](https://discuss.circleci.com/t/terrible-terrible-usability/35332)                                                                                         	|
            | Missing old chat notifications                  	| Poor User Experience    	| The new UI now requires the use of Orbs for functionality such as chat.  This cause an issue with access old chat messages in new UI                                                                                                            	| [CircleCI Community Discussion](https://discuss.circleci.com/t/where-is-chat-notification-on-the-new-circleci-ui/35115)                                                                   	|
            | Increased page load times                       	| Poor User Experience    	| Within the new UI, certain errors will result in increased page loading times.                                                                                                                                                                  	| [CircleCI Community Discussion](https://discuss.circleci.com/t/the-test-summary-page-takes-9-times-longer-to-load/35276)                                                                  	|
            | Canceling a running build                       	| Poor User Experience    	| Within the new UI, the option to cancel a running build is not moved under “Rerun” which makes it more difficult to find                                                                                                                        	| [CircleCI Community Discussion](https://discuss.circleci.com/t/new-ui-how-to-cancel-running-builds-from-list/35167)                                                                       	|
