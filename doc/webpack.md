# Webpack Asset Build

## What is it
Webpack has been added to the www-gitlab-com repo to compile and build assets for the static website during a build. We are using the Middleman external asset pipeline to do this. Here are some helpful links: 
https://middlemanapp.com/advanced/external-pipeline/

## Where is it
The webpack build is located at the root directory of the repository and located at webpack.config.js.

## Production build
During a production build of the website, we use a CI job to run webpack and build assets with yarn. 


## How to test locally
Runing a middlman locally from the `/sites/handbook/` or `/sites/marketing/` site will invoke the webpack build automatically. You can test just runing webpack without middlman by using the following command from the project root: `yarn run build-webpack`

