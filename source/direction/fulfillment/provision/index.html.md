---
layout: markdown_page
title: "Group Direction - Provision"
description: This is the direction page for the Provision group which is part of the Fulfillment stage. Learn more here!
canonical_path: "/direction/fulfillment/provision"
---

- TOC
{:toc}


### Introduction and how you can help



### Overview



## Problem and Jobs to be Done



#### Challenges to address




### Where we are Headed 




#### What's Next & Why



#### What is Not Planned Right Now



