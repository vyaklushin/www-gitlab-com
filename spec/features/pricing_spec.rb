require 'spec_helper'

describe 'pricing page', :js do
  before do
    visit '/pricing'
  end

  context 'when looking at the offerings' do
    it 'contains a total of 4 plans with their characteristics' do
      expect(page.assert_selector('.plans .plan', count: 4)).to be_truthy
    end

    context 'when clicking the Start now button' do
      before do
        find('.btn[data-target="#free-modal"]').click
      end

      it 'shows a modal with 2 buttons' do
        expect(page.assert_selector('.hosted .btn-pricing', text: 'Sign-up for free', visible: true)).to be_truthy
        expect(page.assert_selector('.self-managed .btn-pricing', text: 'Install GitLab for free', visible: true)).to be_truthy
      end
    end

    context 'when clicking a Buy now button' do
      before do
        find('.btn[data-target="#bronze-starter-modal"]').click
      end

      it 'shows a modal with 2 purchase buttons' do
        expect(page.assert_selector('.hosted .btn-pricing', text: 'Purchase SaaS', visible: true)).to be_truthy
        expect(page.assert_selector('.self-managed .btn-pricing', text: 'Purchase Self-managed', visible: true)).to be_truthy
      end
    end
  end

  context 'when looking at the FAQ' do
    it 'contains a list of questions' do
      expect(page.assert_selector('.faq-item', minimum: 1)).to be_truthy
    end

    xit 'shows the contents of a question when it is clicked' do
      find('.js-faq-question', match: :first).click

      expect(page).to have_selector('.faq-item.is-open')
    end
  end

  context 'when looking at the CTA' do
    it 'is able to contact sales' do
      expect(page).to have_css('.contact')
      expect(page).to have_selector(:link_or_button, 'Contact Sales')
      expect(find('.contact a')[:href]).to include('/sales')
    end
  end
end
