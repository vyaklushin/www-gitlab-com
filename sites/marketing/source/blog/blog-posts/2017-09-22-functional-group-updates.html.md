---
title: "GitLab's Functional Group Updates: September 19th - September 22nd" # replace "MMM" with the current month, and "DD-DD" with the date range
author: Chloe Whitestone
author_gitlab: chloemw
author_twitter: drachanya
categories: company
image_title: '/images/blogimages/functional-group-update-blog-cover.jpg'
description: "The Functional Groups at GitLab give an update on what they've been working on from September 19th - September 22nd"
tags: inside GitLab, functional group updates
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Thursday, right before our [GitLab Team call](/handbook/#team-call), a different Functional Group gives an [update](/handbook/people-operations/group-conversations/) to our team.

The format of these calls is simple and short where they can either give a presentation or quickly walk the team through their agenda.

<!-- more -->

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Engineering Team

[Presentation slides](https://docs.google.com/a/gitlab.com/presentation/d/1jR436tBPeoWI94xVn65hZDdKOwj57bR-3psNpxWkvts/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/V4ho9SPL6ps" frameborder="0" allowfullscreen="true"></iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Prometheus Team

[Presentation slides](https://docs.google.com/presentation/d/1CQDMPr85hf9f8ixFgPrPmzpgGc7Svj8VfOeugPWRFKY/edit)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/3Qn4i5Y-dAw" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Frontend Team

[Presentation slides](https://drive.google.com/file/d/0B6SrD3vaBHUeYWNIYTh6TURpZlk/view)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/m6DfA_wTybY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### UX Team

[Presentation slides](https://docs.google.com/presentation/d/1XZk4y1_gf3Pk2bkT0QPZJXR46qp3jpE5MpqyTED0oAc/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/GXaW7Mn0pgk" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!
