---
layout: markdown_page
title: "Project Management"
description: "GitLab enables lean and agile project management from basic issue tracking to scrum and kanban style project management. Learn more!"
canonical_path: "/solutions/project-management/"
---
# Plan and manage projects
GitLab enables lean and agile project management from basic issue tracking to scrum and kanban style project management.  Whether simply tracking a few issues, to managing the complete DevOps lifecycle, GitLab has you covered.

### Track and manage issues

| ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾<br> ● Collaborate and define specific business needs. <br> <br> ● Track effort, size, complexity, and priority of resolution. <br> <br> ● Eliminate silos and enable cross-functional engagement. | ![Sprint Burndown chart](https://docs.gitlab.com/ee/user/project/milestones/img/burndown_chart.png) |

### Visualize work with issue boards

| ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾<br> ● Visualize the status of work across the lifecycle. <br> <br> ● Manage, assign and track the flow of work.  <br> <br> ● Enable Kanban and Scrum styles of agile delivery.  | ![Scrum Board](https://docs.gitlab.com/ee/user/project/img/group_issue_board.png)  |

### Maintain traceability through the DevOps Pipeline

![GitLab CI/CD Pipeline](https://docs.gitlab.com/ee/ci/pipelines/img/pipelines.png)

●  Link issues with actual code change needed to resolve issues.  <br> <br> ● Visualize and track the status of builds, testing, security scans, and delivery.  <br> <br> ● Enable entire team to share a common understanding of status.


<center><a href="/sales" class="btn cta-btn orange">Contact sales to learn more</a></center>
-----

Want to know more about how Gitlab can accelerate your adoption of agile software delivery practices?  **Keep reading**.
<br><br>

# GitLab project management

## Track and manage issues

GitLab delivers a flexible and powerful issue tracker that scales from small teams to large complex organizations. GitLab project management makes it easy to capture issues, collaborate on the details, and then prioritize and deliver business results.

| Capture [issues/user stories](https://docs.gitlab.com/ee/user/project/issues/index.html) in GitLab's robust issue tracker. Assign ownership, weight, priority, and then collaborate to refine and define the specific user story details. GitLab maintains strong traceabilty to execution, connecting your **issues/user stories** to the actual design and development through [merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/index.html) where developers collaborate to implement and deliver the actual code changes, | ![User Story / Issue](https://docs.gitlab.com/ee/user/project/issues/img/issues_main_view.png){: .image-width90pct }  ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾ |
| Organize, sort and manage related issues with [labels](https://docs.gitlab.com/ee/user/project/labels.html) allowing you to create views and representations of the backlog to help plan, prioritize and align your team to the business needs.  | ![Labels](search_labels.png) ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾ |
| Efficiently add labels when and where needed to organize and categorize your issues to meet your reporting and tracking needs.  |      ![Add Labels](add_labels.png) ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾ |

## Issue boards to visualize work

Boards in GitLab are a flexible and powerful tool that makes it easy to visualize the work as it flows through the project from your backlog through development, testing and delivery.

| [GitLab issue boards](https://docs.gitlab.com/ee/user/project/issue_board.html) are flexible, making easy to define the columns that match your workflow, such as *Backlog*, *In progress*, *Completed*, and *Accepted*.  The total 'weight' of the issues is summarized for each column, so you can estimate team capacity to accommodate the workload. When an issue is *done* simply move it to the next step. | ![Scrum Board](https://docs.gitlab.com/ee/user/project/img/group_issue_board.png) ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾ |
| Issue boards also make it easy to assign work to specific teams or team members so you can visualize where the work status from a team perspective.  |![Issue Baord - Team Assignments](https://docs.gitlab.com/ee/user/project/img/issue_boards_core.png) ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾ |

## Agile project management

| **Manage Sprints**: GitLab's milestones and burndown charts enable teams to establish time-boxed work intervals and then focus on delivery and velocity. The team simply assigns milestones a start date and a due date to capture the time period of the sprint. The team then pulls issues into that sprint by assigning them to that particular milestone. | ![Sprint Burndown chart](https://docs.gitlab.com/ee/user/project/milestones/img/burndown_chart.png) ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾ |
| **Track your Backlog**: In GitLab, dynamically generated issue lists can be viewed to track backlogs. Labels are created and assigned to individual issues, which then enables filtering of issue lists by a single label or multiple labels. Labels can also be prioritized to assist in ordering the issues in those lists. | ![Issue Backlog List](https://docs.gitlab.com/ee/user/project/issues/img/project_issues_list_view.png) ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾ |

## DevOps pipeline traceability

Enable traceabilty from user stories(issues) to the actual development and delivery with GitLab [merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/index.html).

| The merge request establishes traceability from the issue to the code changes and is the central hub where development teams collaborate on the design and actual implementation of the code.  This is where code reviews, discussions and security testing comes together to help make developers more productive and efficient.  The [**review app**](https://docs.gitlab.com/ee/ci/review_apps/index.html) is a powerful feature in the merge request, deploying an isolated copy of the application changes for testing and immediate review. |  ![Review App](https://docs.gitlab.com/ee/ci/review_apps/img/review_apps_preview_in_mr.png) |
| Traceability to the **DevOps pipeline** is maintained in GitLab through the [merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/index.html), where anyone on the team can track status of development, testing and delivery. | ![GitLab CI/CD Pipeline](https://docs.gitlab.com/ee/ci/pipelines/img/pipelines.png) ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾ |

<center><a href="/sales" class="btn cta-btn orange">Contact sales to learn about GitLab Agile</a></center>
