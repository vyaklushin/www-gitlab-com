---
layout: handbook-page-toc
title: Performance/Potential Matrix and Succession Planning
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Overview

There are many Talent Assessment Options, we have opted to use a Performance/Potential Matrix (commonly known as "9-Box" in the US). GitLab's Performance/Potential Matrix is a type of Talent Assessment that forms part of our [Talent Development Program](https://about.gitlab.com/handbook/people-group/talent-development-program/). 

To ensure we remain efficient, we built a [Performance/Potential Assessment Tool](https://about.gitlab.com/handbook/people-group/engineering/assessment-tool/), this is currently [(CY20-Q3 / FY21-Q3)](/handbook/communication/#writing-style-guidelines) being tested by a few teams, to provide feedback and iteration prior to launching company-wide.   


# The Performance/Potential Matrix

| Performance ↑ Potential →  | **Low Potential** | **Medium Potential** | **High Potential** |
|-----------------|---------------|---------------|----------------|
| **Exceeding** | Team member is performing well in their current job but needs to continue development in current role, or has not exhibited greater technical and/or leadership potential. Team member has not demonstrated willingness to take on significantly greater scope and responsibility in the next 12-24 months. | Team member performs well in their current job, makes valuable contributions and consistently demonstrates competencies required. May be ready to take on greater scope and responsibility in the next 12 months | Team member is developing faster than the demands of their current position and/or division. Team member has been given additional assignments and has demonstrated high-level commitment/achieved significant results. Team member is ready to broaden their skill set and take on significantly greater scope and responsibility |
| **Performing** | Team member is currently meeting expectations of their role. Team member is not prepared to absorb additional scope or complexity in the next 12-24 months. | Team member is currently meeting expectations but may not be willing or able to advance; may not be ready to absorb additional scope or complexity in  the next 12 months. | Team member is contributing as expected and is meeting performance expectations. Team member may be ready to take on greater technical and/or leadership responsibility in  the next 6-12 months. |
| **Developing** | Team member is not meeting performance expectations and there is still more to learn in the current position. There are questions about their ability  to succeed in the current role long-term. | Team member has not been in the position long enough to adequately demonstrate their technical abilities, or may have lost pace with changes in the organization. | Team member is not meeting the requirements in their current role. It is possible that team member could be more successful in the current role with more  direction or in another role or department that more appropriately suits their skill set. |


This matrix is an individual assessment tool that evaluates both a team members current contribution to the organization and their potential level of contribution.
It is commonly used in succession planning as a method of evaluation an organization's talent pool, identifying potential leaders, and identifying any gaps or risks.
It is regularly considered a catalyst for robust dialogue (through a calibration process) and is considered more accurate than one person's opinion.
The performance/potential matrix can be a diagnostic tool for career development.

## What is "Performance"?

Performance is broken into three areas: Developing, Performing, and Exceeding.

### Developing

Around 10-15% of the team generally fall into the “Developing” category. Based on the Job Family responsibilities, [values](https://about.gitlab.com/handbook/competencies/#values-competencies) and [remote working competencies](https://about.gitlab.com/handbook/competencies/#remote-work-competencies), team members in this category are not meeting all of the expectations. There is still more to learn and improve on in the current position to achieve the desired results. This may be due to the following: 

- The team member has not been in the position/at GitLab long enough to adequately demonstrate performance.
- The team member has been recently promoted and needs time to ramp up performance in the new role. 
- The team member may have lost pace with changes in the organization. 
- The team member is not committed to the job responsibilities and/or GitLab. 
- The team member finds it difficult to perform work in a way that is aligned with our values and/or competencies. 
- The team member's performance is aligned to our values and competencies, however, they lack results for Job Family responsibilities. 
- Team member was part of a recent [transfer](https://about.gitlab.com/handbook/people-group/promotions-transfers/#department-transfers) and is still learning how to be successful on their new team.

Examples: 

- The team member needs a lot of guidance from the manager with the majority of tasks to understand requirements and deliverables expected.
- The team member is missing important due dates which is affecting the team, GitLab and/or customers. 
- The team member still needs to adapt remote working best practices. For example, the team member does not manage their own time or work to achieve results or may have difficulty with asyncronous communication. 
- The team member lacks (a part of) the knowledge, skills and abilities which are required for the role, resulting in a low quality of work being delivered.
- There is a significant mis-alignment (I.E. The team member struggles to collaborate with others, the team member does not work iteratively and key metrics are impacted, etc.) 

### Performing

Most team members (60-65%) fall into the “Performing” category. Based on Job Family responsibilities, [values](https://about.gitlab.com/handbook/competencies/#values-competencies) and [remote working competencies](https://about.gitlab.com/handbook/competencies/#remote-work-competencies) team members in this category are “on track” and meeting all expectations. They can independently and competently perform all aspects of the Job Family responsibilities in a way that is aligned with our values and competencies. Their performance consistently meets the requirements, standards, or objectives of the job, and they can occasionally exceed expectations. They deliver results in a timely and accurate fashion. This performance may be expected as a result of: 

- The team member is consistent and stable in their performance.  
- The team member has the required knowledge, skills and abilities to perform in the role successfully and also applies values and competencies in their work.
- The team member has adapted to remote working and their knowledge, skills, and abilities align seamlessly to their role at GitLab. 

Examples: 
- Team member is a dependable member of the team. Their manager can trust them to achieve key metrics.
- Team member consistently works in accordance with our value and remote working competencies. 
- While consistently meeting expectations, the team member does not regularly exceed expectations.
- Team member does not frequently express interest in additional projects, responsibilities, or work outside of their immediate scope.
- The team member is exemplifying our values in their work, but might have some competencies to improve on. 

### Exceeding

A relatively small portion of team members (about 25%) generally fall into the “Exceeding” category. Team members that are exceeding consistently surpass the demands of their current position. They demonstrate unique understanding of work beyond the assigned area of responsibility. They contribute to GitLab’s success by adding significant value well beyond Job Family requirements, [values](https://about.gitlab.com/handbook/competencies/#values-competencies), and [remote working competencies](https://about.gitlab.com/handbook/competencies/#remote-work-competencies). This performance may be expected as a result of: 

- Individual is developing more quickly than the demands of their current position. 
- They rise to the challenge when give additional assignments and achieve significant results.
- A “go-to” team member for their peers for tough problems and day-to-day guidance.
- High commitment and engagement to GitLab combined with extensive knowledge, skills, and abilities to perform in the role. 

Examples: 

- Constantly seeks opportunities to improve both self and organization. 
- The team members exemplify great ways of giving and receiving feedback - incorporating this directly in their work. 
- The team members are ambassadors for the values, take full ownership of delivering projects, and very rarely miss due dates. 

### The Performance Factor

While the primary objective of the performance axis of the Performance/Potential Matrix is to calibrate team member performance, this axis of the matrix also directly impacts the [Performance Factor](/handbook/total-rewards/compensation/compensation-review-cycle/#performance-factor), which is a determining factor in the annual compensation review. Please reference the [Total Rewards Performance Factor page](/handbook/total-rewards/compensation/compensation-review-cycle/#performance-factor) for more detail on compensation impact.

### Measuring Performance

How can managers determine whether their team members are Developing, Performing, or Exceeding? "The Performance Factor" = Job Family responsibilities (60%) + Competencies (40%). It is _optional_ to use [The Performance Factor Workheet](https://docs.google.com/spreadsheets/d/1HHe-Vb6y6F4HXsek3sObV4IVNAGaTvXGi_9fbatT0Uo/edit#gid=241716076) for weighing the different Performance Factors.

#### Job Family

Performance against Job Family role, requirements, and performance indicators should be weighted at 60%. 

* Review Job Families: Look at the Responsibilities section/how this previously was feeding into Compa Groups for the roles in your department.
    * Example: [Software Engineer in Test](https://about.gitlab.com/job-families/engineering/software-engineer-in-test/#responsibilities)
* Review Performance indicators per Job Family: Look at the Performance indicators.
    * Example: [Software Engineer in Test](https://about.gitlab.com/job-families/engineering/software-engineer-in-test/#performance-indicators)

For management roles, please also review and consider the [levels](https://about.gitlab.com/company/team/structure/#levels) outlined in the Organizational Structure. 

#### Competencies

Performance against competencies should be weighted at 40%. 

* [Values Competencies](https://about.gitlab.com/handbook/competencies/#values-competencies)
* [Remote Working Competencies](https://about.gitlab.com/handbook/competencies/#remote-work-competencies)
* _(if applicable)_ [Functional Competenceis](https://about.gitlab.com/handbook/competencies/#functional-competencies) 
* _(if applicable)_ [Manager and Leadership Competencies](https://about.gitlab.com/handbook/competencies/#manager-and-leadership-competencies) 

## What is "Potential"?

While performance is focused on the past and present, potential is focused on the future. Because of the nature of the future-focus associated with potential, it is more difficult to measure than performance, and inherently more qualitative than quantitative. A key element in determining potential is the manager and leadership observation and experience working with team members.  Managers can gauge team member potential against the expectations in their current role or their potential to take on different roles across GitLab. 

Potential refers to the ability and desire of a team member to successfully assume increasingly more broad or complex responsibilities and learn new skills, as compared to peers and the roles' responsibilities outlined in their respective Job Family. This could include the potential to move up to the next level in their job family, and/or a lateral move. 

Potential *can change* over time as team members develop new interests, as new opportunities for growth arise, and as team members continue to broaden their knowledge, skills, and abilities. 

### Low

Low potential generally refers to a team member who is not working at full potential against the roles and responsibilities outlined in their Job Family. There could be a variety of reasons for this, including:

* Lack of motivation to learn new skills or take on new projects
* Motivated to do what is needed in current job, not in what is required in a higher level
* Not expressing interest or demonstrating desire to move up or laterally 

Please note that low potential *does not* equate to low performance, but has more to do with the team member's readiness and promotability for future opportunities. 

### Medium

Medium potential generally refers to a team member who is growing in their current role and demonstrating interest in advancing (up or laterally) and they exhibit knowledge, skills, and abilities that indicate this. Team members with medium potential generally:

* Show interest in areas outside of their immediate scope occasionally
* Are comfortable in their current role
* Are a stable counterpart for other team members (especially for peers and more junior team members)
* Learn and apply new skills when the job calls for it; apply lessons learned to enhance success

### High

High potential generally indicates that a team members will be ready for promotion within the next year (or when an opportunity arises). High potential team members:

* Frequently seek involvement in stretch project/projects outside of their scope
* Invest in their development; seeks feedback to improve and applies that feedback on the job 
* Demonstrates ability to learn new skills
* Actively pursues increased opportunities to influence decisions and inspire others

## Calibration Session Guidelines

The portion of the Performance/Potential matrix that often entails the most significant time commitment is the live calibration session of team members with leadership. The calibration session is very valuable to ensure consistency across the Job Family and level, raise any questions, and provide cross-departmental and/or cross-divisional feedback on team members to capture the assessment of different managers as opposed to the opinion of the direct manager exclusively. 

### Pre Work

It is **absolutely essential** that managers complete the required pre-work to ensure that the live calibration session is as efficient and productive as possible. Pre work includes:

- Review the Job Family/Families that will be reviewed in detail
- Review our competencies 
    - [Values competencies](/handbook/competencies/#values-competencies)
    - [Remote work competencies](/handbook/competencies/#remote-work-competencies)
    - [Functional competencies](/handbook/competencies/#functional-competencies) _(if applicable)_
- Review our [Unconscious Bias](https://about.gitlab.com/company/culture/inclusion/unconscious-bias/) handbook page and consider watching our [Recognizing Bias Training](https://about.gitlab.com/company/culture/inclusion/unconscious-bias/#recognizing-bias-training) if you haven't already. 
- Review the [Performance/Potential Matrix Training Information](https://docs.google.com/presentation/d/151ys8xkOak9ifU9IPXQydZ44sb_BoMpMocWmjVonLHE/edit), which delves into definitions for each box. 
- Determine the appropriate box for each of your team members
- Add notes for each of your team members to the agenda 
    - An example of notes could include:
        - 2-3 strengths/accomplishments (and supporting examples)
        - 2-3 improvement areas
        - Anything else noteworthy (I.E. recently promoted, COVID impact, etc.)
- Review the notes of your peers for other team members and add feedback/questions/thoughts for discussion

### Calibration Session

The calibration session is one of the most important pieces of the Performance/Potential Matrix process, as it provides time for managers, their peers, and their manager to calibrate. Guidelines to ensure efficiency during calibration sessions are:
* It may not be needed to discuss every team member in detail. Calibration sessions typically focuses on gaps, outliers and areas that might require additional management attention like the timeline and process for team members ready for promotion, how to improve performance for developing team members, etc.
* Be conscious of time and consider setting a (reasonable) time limit per team member being discussed. 
* Review program guidelines and avoid leniency bias.
* Refer to performance data that you may have taken on a team member throughout the past year - including 360 review feedback, performance against metrics over time, key accomplishments, etc.; this will help avoid bias like recency bias or the halo effect. Please note that when reviewing 360 data for the Q4 talent assessment, it's important to take into account areas in which the team member has improved. Two quarters is a fairly significant amount of time and team members have (hopefully) take action on improvement areas during this period, which should be reflected in the assessment. Managers and other participants in calibration discussions should be prepared with this information if asked how they determined a rating. 
* Leaders should feel comfortable highlighting team members in other functions
* **Ask questions and provide feedback!** This may seem obvious, but it is import aspect to help identify any potential bias


## Matrix Tool

Our goal is to have the whole company use the the Matrix [Assessment Tool](https://about.gitlab.com/handbook/people-group/engineering/assessment-tool/) for the Performance/Potential matrix. However, while the tool is being developed and tested, groups who want to get a head start on the Performance/Potential matrix can use the [Performance/Potential Matrix team template](https://docs.google.com/presentation/d/123fS20lqXt-bhDEqwMCyL2K1BAcgVCEd61pQW6FmfRw/edit#slide=id.g817046ecd3_0_44).

## Identifying Action 

There is an overview of recommended actions to take after the calibration session included in the [Performance/Potential Training Information](https://docs.google.com/presentation/d/151ys8xkOak9ifU9IPXQydZ44sb_BoMpMocWmjVonLHE/edit#slide=id.g88f412eeb7_1_13) from slide 26- 35. Additionally, this matrix can facilitate: 
* [Career Development Conversations](https://about.gitlab.com/handbook/people-group/leadership-toolkit/career-development-conversations/)
* Promotion planning 

## Timeline

### Regular Cadence

Our e-group completes Performance/Potential Matrix quarterly for all their direct reports. The rest of GitLab does this twice annually, ideally in Q2 and Q4. In Q2, the exercise would ideally take place after the [360 annual feedback review](/handbook/people-group/360-feedback/), and in Q4 the exercise would ideally take place in November prior to the [annual compensation review](/handbook/total-rewards/compensation/compensation-review-cycle/#annual-compensation-review). 

#### Eligibility

Anyone hired on or before October 31st is eligible to participate in the Q4 Performance/Potential matrix annual review. However, if the team member being assessed has been with GitLab for a period of 3 months or less, it is likely that these team members will fall into `Developing` for performance and should not expect a compensation adjustment, although they are still eligible to be reviewed.

This is because team members generally need several months to get adjusted to their role and responsibilities, and we ideally would have hired the team member at their accurate market rate. 

While there are exceptions, this is the general rule of thumb. 

### Initial Launch Timeline

GitLab is kicking off the Performance/Potential Matrix in FY'21 Q3 in preparation for the [annual review](/handbook/total-rewards/compensation/compensation-review-cycle/#annual-compensation-review) in FY'21 Q4. The timeline for launch, training, and completion of the exericse is as follows:

*September*
* 2020-09-30: Review and iterate based on feedback provided in the [issue](https://gitlab.com/gitlab-com/people-group/General/-/issues/965)
* 2020-09-30 (or prior): Update [compensation calculator](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/calculator/) in preparation for October training

*October*
* 2020-10-07: Handbook fully updated with first iteration of Performance definitions
* 2020-10-14: Meaning of "potential" defined and in handbook
* 2020-10-16: Communication to the whole organization
* 2020-10-21: Performance Factors Training Session for Managers (2 sessions 8:30am PST and 5pm PST)
* 2020-10-28: AMA Performance Factors (2 sessions 8am PST and 5pm PST)
* 2020-10-29: Performance Factors Office Hours (2 sessions 8am PST and 5pm PST)

*November*
* 2020-11-02: First version of the Performance/Potential matrix tool ready
* 2020-11-02: Kick off formal assessment period
* 2020-11-04: Performance Factors Training for all team members (2 sessions 8:30am PST and 5pm pST)
* 2020-11-30 Performance/potential matrix is completed company wide

*December*
* 2020-12-04: Performance rating finalized for all team members and approved through the e-group level
* 2020-12-04: Communication training and guidance shared with managers
* 2020-12: Performance axis results communicated with team members

*January*
* 2020-01: Manager review will open in Compaas for managers to advise on dollar amounts for increase (guided by Performance Factor results)

*February*
* 2020-02: Annual compensation review changes are effective February 1, 2021

### Matrix Assessment Timeline 

The Performance/Potential Matrix typically takes 4-6 weeks to complete from beginning to end. Steps are as follows:

1. Managers complete a Performance/Potential Matrix for their respective teams
1. Live session takes place for calibration 
1. Executive Review (leadership meets to review the matrix results, promotion requests, development and performance actions, review discrepancies, and measure against previous matrix results).
1. Performance axis results communited with team members. 
1. Retrospace takes place with leadership to determine next steps and iterate. 

## Communication 

In most companies the Performance/Potential Matrx is used exclusively as a management tool and results are not typically shared with team members. In the spirit of our transparency value, we want to encourage feedback resulting from calibration sessions to be communicated with team members, but refrain from communicating box-specific placement. **Discussion topics that arise during calibration sessions (or at any other point during the assessment process) are confidential. Please do not share with anyone other than each individual team member.**

We hope to increase the scope of our communication as we iterate on the the Performance/Potential Matrix process further and continue to define criteria and receive feedback after the first iteration in Q4 FY`21. 

### Do Communicate
Managers are *encouraged* to communicate the following directly with each individual on their team:

1. Discussion points from calibration sessions 
1. "Performance" axis results (AKA: The [Performance Factor](/handbook/total-rewards/compensation/compensation-review-cycle/#performance-factor) as this impacts compensation. _Please refrain from communicating Performance axis final placement until apprvoed through the e-group level._

We recommend communicating the points above directly with team members because:

* This information should not come as a surprise to team members (as career development conversations and feedback should be ongoing)
* It provides an opportunity to receive input/feedback from team members
* It serves as a platform that can feed into a development plan

##### Communicating Matrix Feedback

Below are a few recommendations for managers when communicating themes from the calibration discussion:

* Discuss areas the team member is doing well
* Discuss areas that need improvement
* Provide examples
* Provide recommendations for future development
* Prepare the communication in advance

### Do Not Communicate
Managers are *discouraged from communicating* the following:

1. Box placement in the matrix
1. "Potential" axis results 

We discourage communicating the points above directly with team members because:

* We want to move away from the "box" and focus on actionable items and development (the box is simply a manager tool to help think through the process).
* To stay iterative, we wanted to rollout the first iteration of the talent assessment this year. As a result, we have prioritized the development of Performance axis guidelines and criteria (as it will impact compensation during the [annual review](/handbook/total-rewards/compensation/compensation-review-cycle/#annual-compensation-review)), and the Potential axis criteria is still being developed. We want to make sure our guidelines and defining criteria are clear before communicating Potential. 
* To allow an opportunity to receive feedback on the Performance/Potential matrix process to determine whether this is the talent assessment tool we will use moving forward. 

### Performance / Potential Review FAQs
* **How is this different from what we did in the past?** <br>
    * **Addition of the Talent Review Process:** In the past, we did not have a company-wide talent review process. Some groups leveraged a 9-box format, skill/will matrices but there was no formal program to support the review of team members. With this newly launched process, we plan on having talent review cycles company-wide twice a year using the same assessment template. <br>
    * **Changes to the Annual Compensation Review Program:** In the past, in preparation for the Annual compensation review - managers go through a process to assess someone’s knowledge, skills and abilities also known as their compa group. This would directly feed as an input to the compensation review program in addition to location factor changes and cost of living adjustments. For this year, we are replacing the compa group assessment with a performance factor. Location factor changes will continue to be incorporated into the program and we are removing cost of living adjustments. <br>

* **Why does this matter to me?** <br> 
This change will impact both team members and managers. 
    * Team Members’ performance will be assessed (developing, performing, or exceeding) as well as potential. Team Members should be prepared to have a conversation with their manager about their performance in December. <br>
    * Managers will need to assess their team member’s performance (developing, performing or exceeding). They will also need to attend a calibration meeting in which team member’s ratings are discussed and evaluated for consistency across the team. Once the program closes, managers will be responsible for communicating performance to team members and in some organizations, potential will be communicated to team members as well. <br>

* **How does this impact the teams I lead?** 
    * As a manager, you will be required to assess your direct team member’s performance and potential. You will also be required to participate in calibration discussions in which ratings are reviewed to ensure consistency and minimize bias. After the program is approved, you will be responsible for communicating the performance axis with your team member. In some organizations, potential will be communicated as well. <br>

* **Who I can reach out to in supporting me with rolling this out with my team?**  
    * You can reach out to your manager or your [aligned People Business Partner](https://about.gitlab.com/handbook/people-group/#people-business-partner-alignment-to-division) to assist you with this process. <br>

* **How will this impact the way I work at GitLab?** 
    * This will not impact the way that you work at GitLab. Performance is measured by your job description responsibilities and competencies (remote work, values and if applicable department specific competencies). Both of these things have been in place for a while so this should not impact what you focus on or how you work. <br>

* **When should I expect these changes to occur?** 
    * We are launching the talent review process at the beginning of November. Calibration discussions will happen throughout the month. The program will be approved by early December. The Annual Compensation Review program will kick off in January. For a more detailed timeline, please review the [initial launch timeline](https://about.gitlab.com/handbook/people-group/performance-assessments-and-succession-planning/#initial-launch-timeline) above. <br>

* **As a manager, am I required to assess both performance and potential for the first iteration? (FY'21 Q4)?** 
    * For the reasons stated above in the [communication dos and don'ts](/handbook/people-group/performance-assessments-and-succession-planning/#do-communicate) section, we recommend that groups assess both performance and potential. However, all that is required in FY'21 Q4 is the assessment of performance, as this will impact compensation. There should be assessment consistency within each department at GitLab. Please confirm with your manager or People Business Partner whether your group will be assessing both performance and potential, or just performance for this iteration.

* **As a manager, when reviewing my team members, what is the difference between Developing and Underperformance?**
    * [Underperformance](/handbook/underperformance/) is generally ongoing for a period of time with multiple attempts to remediate. "Developing" implies more of a stage of progression that is also consistent (I.E. not a sustained underperformance to meet expectations). 
    While everyone within the Developing category isn't necessarily Underperforming, a subset of team members who might be Underperforming would be grouped into the Developing category along with those team members who are not underperforming but are simply new to the role. 
    Team members who are on a formal Underperformance remediation plan, such as a [Performance Enablement Plan](/handbook/underperformance/#performance-enablement-plan-pep) or [Performance Improvement Plan](/handbook/underperformance/#performance-improvement-plan-pip), will not be eligible for a compensation increase as a result of the [Annual Compensation Review](/handbook/total-rewards/compensation/compensation-review-cycle/#performance-factor). 

We will continue to add additional questions and answers as the program progresses. 
## Succession Planning

The succession planning process starts with leaders doing a [performance/potential matrix](/handbook/people-group/performance-assessments-and-succession-planning/#the-performancepotential-matrix) of each of their direct reports.

The resulting charts are reviewed with peers, e.g. all other executives, all other senior leaders in the function, etc.

| Person    | Jane Doe | John Doe |
|-----------|---|---|
| Role      | Job Title  | Job Title  |
| Emergency (ER) | Someone who could take over this role if the current person were affected by a [lottery factor](/handbook/total-rewards/compensation/#competitive-rate) or had to take emergency leave |   |
| Ready Now (RN) | Someone who could be promoted into the role today  |   |
| Ready in less than 1 year (R1) | Someone who could be trained and elevated into the role in less than 1 year  |   |
| Ready in 1 year or more (R1+) | Someone who could be trained and elevated into the role in 1 year or more  |   |

## Resources

| Resource | Purpose |
| ------ | ------ |
| [Performance/Potential Training Slides](https://docs.google.com/presentation/d/151ys8xkOak9ifU9IPXQydZ44sb_BoMpMocWmjVonLHE/edit#slide=id.g817046ecd3_0_44) | All managers/leaders should review this content prior to beginning the Performance/Potential matrix assessment process. |
| [Performance/Potential Team Template](https://docs.google.com/presentation/d/123fS20lqXt-bhDEqwMCyL2K1BAcgVCEd61pQW6FmfRw/edit) | This slide deck provides a template for the assessment for teams that choose to do the exercise prior to the completion of the [assessment tool](https://about.gitlab.com/handbook/people-group/engineering/assessment-tool/). |
| [Values competencies](/handbook/competencies/#values-competencies) | Values competenices form an important part of the Performance Factor evaluation and are important to review prior to beginning the assessment period. |
|[Remote work competencies](/handbook/competencies/#remote-work-competencies) | Remote work competenices form an important part of the Performance Factor evaluation and are important to review prior to beginning the assessment period. |
| [Functional competencies](/handbook/competencies/#functional-competencies) | Functional competenices (for groups that have them developmed) can also influence the Performance Factor and should be reviewed prior to the assessment period. |
| [Unconscious Bias](https://about.gitlab.com/company/culture/inclusion/unconscious-bias/) handbook page and [Recognizing Bias Training](https://about.gitlab.com/company/culture/inclusion/unconscious-bias/#recognizing-bias-training) | It is important to me mindful of unconscious bias always, and especially during talent reviews and assessments. It is highly recommended that you review the handook page and watch the training. |
| [Performance Factor](/handbook/total-rewards/compensation/compensation-review-cycle/#performance-factor) handbook page | This page is the SSOT to review the Performance Factor's impact on compensation. |





