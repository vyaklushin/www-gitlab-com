---
layout: handbook-page-toc
title: "Business Systems Analysts"
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## <i class="far fa-building" id="biz-tech-icons"></i> Business Systems Analysts

### Who we are

**Barbara Roncato - Business Systems Analyst**  
GitLab handle: [@broncato](https://gitlab.com/broncato)  
Slack handle: @barbara  
Job Family: [Business Systems Analyst](/job-families/finance/business-system-analyst/#business-systems-analyst)

**Lis Vinueza - Business Systems Analyst**  
GitLab handle: [@lisvinueza](https://gitlab.com/lisvinueza)  
Slack handle: @lis 
Job Family: [Business Systems Analyst](/job-families/finance/business-system-analyst/#business-systems-analyst)

**Phil Encarnación - Business Systems Specialist**  
GitLab handle: [@PhilEncarnacion](https://gitlab.com/PhilEncarnacion)  
Slack handle: @Phil Encarnacion 
Job Family: [Business Systems Specialist](/job-families/finance/business-system-analyst/#business-systems-specialist)


### Contacting us
Slack: `#TBD`  
GitLab: [Create an issue](TBD)

### Our mission

TBD

### Our priorities

TBD

### What can we help you with?

##### 1. Retrospectives

We can host your project retrospective.
[Open an issue.](https://gitlab.com/gitlab-com/business-ops/Business-Operations/-/issues/new)

##### 2. Application Evaluations

We provide templates for vendor evaluations, can help write and review your user stories, and are happy to participate in tool evaluations that integrate with other applications or intersect with multiple departments.

Please involve us in all tool evaluations that integrate into the enterprise application ecosystem before beginning demos with vendors.
[Open an issue.](https://gitlab.com/gitlab-com/business-ops/Business-Operations/-/issues/new)

## Templates
*  [Rollout Plan](https://gitlab.com/gitlab-com/www-gitlab-com/issues/new?issuable_template=public-rollout-plan)
*  [Change Management: Third Party Applications Changes](https://gitlab.com/gitlab-com/business-ops/change-management/issues/new?issuable_template=Third%20Party%20Change%20Management)
*  [Change Management: Internal Tool Changes](https://gitlab.com/gitlab-com/business-ops/change-management/issues/new?issuable_template=Internal_Change_Management)
*  [Software application selection: Request for Proposal](https://docs.google.com/document/d/1_Q2b5opYUQ9TlGmF2vOJ6anu0spVFMkNO6YCR4UjYXM/edit?usp=sharing)
*  [Software application selection: User Stories](https://docs.google.com/spreadsheets/d/1c1R0pqKr8YwXXATzFVEUaofF2luNrHbmcNkKAWisebs/edit?usp=sharing)

##### 3. Solving Business Problems/Architectural Troubleshooting

We have high level views of the enterprise application ecosystem and can help troubleshoot where a business process has broken down or a system flow is not working as expected.
[Open an issue.](https://gitlab.com/gitlab-com/business-ops/Business-Operations/-/issues/new)

### Results we've delivered

##### Finance

<a href="https://gitlab.com/groups/gitlab-com/-/boards/1580145?assignee_username=broncato&&label_name[]=BTG-Project" class="btn btn-purple">Work Management Board</a>

- Work with the Finance and Accounting teams to improve workflows, processes, and application ecosystem
- Project work to implement new modules to tools
- Work with stakeholders from other teams like Field Ops and Fulfillment team that integration with the financial ecosystem.


##### Portal integrations and operations

<a href="https://gitlab.com/groups/gitlab-com/-/boards/1586460?assignee_username=j.carey&" class="btn btn-purple">Work Management Board</a>

- Portal Analysis/Research, Documentation, and Architecture Recommendations
- Partner to Go to Market Operations (Marketing and Sales Ops), Sales Systems, Customer Success Operations, and Channel Operations.

##### IT Operations and People Operations

<a href="https://gitlab.com/groups/gitlab-com/-/boards/1596495?assignee_username=lisvinueza&" class="btn btn-purple">Work Management Board</a>

- Partner to Team Member Enablement and People Operations
- Business Technology Operations and Workflows


