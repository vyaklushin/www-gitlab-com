---
layout: handbook-page-toc
title: "VP Scheduling"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

When scheduling for VPs or directors at GitLab, please check the scheduling preferences below.

## Scheduling for Dave Gilbert, VP of Recruiting

* Please schedule all meetings through the People Function's EBA, Trevor Knudsen
* Ask before scheduling over personal events (Noted by purple color in calendar)
* Keep meetings to 25 or 50 minutes unless otherwise specified
* Direct Report 1:1's occur weekly
* Skip level 1:1's occur every other month
* 1:1 meetings are light blue, internal meetings are red, external meetings are orange, coffee chats are dark green, reminders are in yellow, interviews are in dark blue.
* Scheduling should ideally be kept between 9:00 am and 5:00 pm MT

## Scheduling for Carol Teskey, Sr. Director, People Success

* Please schedule all meetings through the People Function's EBA, Trevor Knudsen
* Keep meetings to 25 or 50 minutes unless otherwise specified
* Direct Report 1:1's occur weekly
* 1:1 meetings are dark dark green, internal meetings are purple, external meetings are orange, coffee chats are light blue, reminders are in yellow, interviews are in dark blue, anything where attendance is essential and Carol plays a huge part in the meeting should be noted in red.
* Scheduling should ideally be kept between 9:00 am and 6:00 pm Irish Standard Time

## Scheduling for Sales VPs

**David Hong, David Sakamoto, Mike Pyle, Ryan O’Nell, Michelle Hodges, Brandon Jung**

* Please reach out to Katie Gammon (EBA) to schedule or reschedule any meetings 
* Don't schedule over DNB or any CRO calls without reaching out for approval from VP and EBA
* For interviews: CES should tag EBA on Greenhouse to review times submitted by the candidate, or to provide times for the VP
* If you add any meetings to calendar directly, tick the box “Attendees can modify” so EBA can change if necessary
* External meeting requests (with those outside of GitLab): email EBA and VP and loop them in with external parties. EBA will then schedule directly with external parties.
* Internal meeting requests (with other GitLab Team Members)
  * Please slack EBA in #sales-vp channel 
  * If the meeting is confidential (ex: interviews), please slack EBA directly
* Please include the following in your slack/email
  * Must have/optional attendees
  * Meeting type: internal prep, client facing/customer, prospective customer, etc
  * Urgency/ Desired timeframe: in the next two days, in the next week or two, etc
  * Duration: 25 mins, 50 mins, etc.
  * Subject of the meeting
  * Provide context: include agenda to be covered. Share google doc if available to be included in invite or link relevant issues, slides, etc.

## Scheduling for Finance VPs and Sr. Leadership

**Bryan Wise, VP of IT - Craig Mestel, VP of FP&A - Dale Brown, PAO - Igor Groenewegen-Mackintosh, Director of Tax - Tony Righetti, Sr. Director of IR - Paul Machle, Finance Special Projects Advisor**

* Please reach out to Jaclyn Grant (Sr. EBA) to schedule or reschedule any meetings 
* Don't schedule over DNB blocks 
* For interviews: CES should tag Sr. EBA on Greenhouse to review times submitted by the candidate, or to provide times for the VP
* If you add any meetings to calendar directly, tick the box “Attendees can modify” so EBA can change if necessary
* Working blocks on the calendar are OK and can be moved to prioritize other meeting requests, team members should check in with the Sr. EBA to request a meeting using the meeting request requirements as a guideline
* A calendar key is available for viewing in their daily schedule
* External meeting requests (with those outside of GitLab): email Sr. EBA and VP / Sr. Leadership and loop them in with external parties. Sr. EBA will then schedule directly with external parties.
* For internal meeting requesting please contact Sr. EBA, include the following in your slack/email:
  * Must have/optional attendees
  * Urgency/ Desired timeframe: in the next two days, in the next week or two, etc
  * Duration: 25 mins, 50 mins, etc.
  * Subject of the meeting
  * Provide context: include agenda to be covered, share google doc if available, include a link to any relevant issues, slides, etc.
