---
layout: markdown_page
title: "Virtual Events Best Practices"
---

{::options parse_block_html="true" /}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

General best practices for virtual event planners, speakers, and attendees that apply across all [virtual event](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/) types. You may also want to review the [GitLab Video Playbook](https://about.gitlab.com/handbook/communication/video-playbook/), which offers guidance on creating engaging video content, much of which also applies to virtual events.

## Event planners
* Choose tech based on your event goals and structure of event/ presentations. 
* Consider your narrative - people will be more likely to tune in if you are telling a story throughout the day vs. it being one of talks that aren't related. 
* Have all presenters test their A/V setup in advance of event.
* Ask speakers who will be presenting live to test (and if necessary troubleshoot) their wifi connection prior to the event. Especially important for live demos and coding.
* If you have time, do speaker run through with all speakers to review slides and give feedback and answer any flow questions.
* Have a final check in meeting with MC's, moderators, and speakers. If you can have moderator meet their speaker on video call before the event to build rapport. 
* Give moderators extensive instructions, especially if coordinating in-person as well as online presenters.
* Do the same for the speakers. What do they have access to that's different than the attendees? 
* Give quick video instructions on how attendees can use the platform. Also provide written instructions. 
* Have presenters sign in at least 20 min before to retest their audio and setup. Once they are all set, allow them to go odd and make the most use of their time and ask that they rejoin 5 mins before starting. This will give proper time to troubleshoot if needed. 
* Make sure no one other than specific people can close the stream as people might accidentally otherwise. 
* Prep back up plans for speakers who have technical issues, don't show, or other things that pop up. Have a back up for all live demos.
* If possible make sure people don't need to re-join (i.e. it's one long hangout/zoom) through the day, attrition greatly increases if they keep needing to re join.
* Have intermissions/ transition slides with tips for the audience. You've got your audience, make the best use of it! 
* Keeping the audience engaged:
  * Survey the audience.
  * Add a giveaway or chance to get swag - be sure to build the process on the back end on how you can execute this in advance. i.e. if your swag vendor will allow you to ship out 25 items at no cost, then do a raffle for 25 or use a swag platform where people can enter in their own contact information. 
  * Create a Slack channel or something similar for attendees to build community & keep the convo going there -- Remember your CTA? 
  * If possible allow community/ attendees engage with speakers post talk.
* Find ways to keep the discussion going- the event shouldn't end with the camera stops. Decide on a CTA. Let folks know next steps and how to continue to engage and educate themselves. 
* Have a plan for content use and distribution post-event. Be sure to communicate to the audience if they will receive the slides/recording and also a timeframe in which they will receive the content. 
 
#### Specifics for Event Planners at Virtual Sponsored Conferences/Events
* Unlike in-person events, there are not usually drink receptions, food and other activities that drive traffic to a virtual booth. If there is gamification, it also is not a guarantee someone will actually talk to someone in the booth. Do not expect to get the same type of traffic you would at an in person event. 
* To help drive traffic, staff and schedule SDRS and others to attend and network outside of the booth as well, whether that be through direct messages, in sessions, or any other networking features the event provides. Be mindful and encourage SDR’s to direct messages strategically, avoiding mass messaging everyone at the conference. This is often not an appreciated outreach and will be disregarded. 
* Have staff reach out to booth leads as soon as they connect with you to help drive engagement and conversations during the event. Aim to set post event appointments. 
* Create a list of sessions/tracks relevant to GitLab and encourage staff to attend. 
* Post incremental updates in the internal event slack channel or have a daily sync about progress on the event goals, any updates, what to expect that day such as speaking slots or other relevant sessions. 
* If you are the event planner, you may want to create an event profile as “GitLab Moderator” so you can create more brand awareness and manage more aspects of the event without being tied to yourself. It also gives another opportunity for people to interact with the brand if they don’t know where to start. 
* If the event is free or inexpensive, you will likely get people who reach out about hiring. Have a script ready for these questions.  




## Speakers
* Test your technology in advance - including any applications you will be using (ex: Zoom, WebEx).
* Have backup headphones and a phone available in case you need to dial in. 
* Have a dedicated microphone (as oppossed to using your laptop mic). We recommend a wired microphone (can be connected to your headphones) as many Bluetooth microphones suffer from latency and quality issues. 
* If you can, have someone fielding questions for you.
* Have slides or visuals to supplement your talk if it is just you so that it doesn't feel like just a meeting or lecture. 
  * Share slides in advance when possible and at the beginning of your session. This can help people follow along in the event of any technical difficulties.
* Wear camera-ready colors- [see doc](https://docs.google.com/document/d/1rPvewsTWm8uqGv-6Wr4-_j4ZmBVjL75fU5_YGV98d24/edit?ts=5e74125a)
* Remember your background: this will be recorded and published online. 
    * Recommend using GitLab branded virtual backgrounds. Please refer to [New Background GitLab Connect Day](https://drive.google.com/drive/folders/1krLvehY7j19ZHZeiSYLNYq82k2Nbgrp-?usp=sharing) for examples. 
* Slides: use a large clear font. You do not know what device the viewer will be on. 
* Don’t move around a lot.
* Look at the camera.
* Dress as if you were at an actual conference.
* If someone will be introducing you, ensure they know how to pronounce your name, title, and GitLab correctly. 
* Ask for a copy of the recording and post the recording on the relevant channel. 
* If there is a chat function on the speaking session: 
     * Have designated staff to attend and show up before the speaking slot starts. 
     * Have staff post in the chat that they are a representative of GitLab and happy to answer any questions. 
     * Know what links or topics are mentioned in the presentation and have UTM links to them ready to post. Good ones to have on hand in addition are the company handbook, our all-remote guide, link to the free trial
     * If at a sponsored event with a booth- At the end, encourage participants to stop by the virtual booth if they still have questions, with a link to the booth. 


## Attendees: How to get the most out of attending a virtual event
* Guide and enable people to share on social .
* Remind them how they will be able to access content post-event.
* Teach them how to use the platform you are doing the event on.
* Add “- GitLab” and the end of your name or “- your title” to your event profile depending on what is displayed in the event when in chat rooms or other interactive areas. If the event platform accepts emojis (often possible if there is a mobile app) feel free to use the fox emoji or something else that shows your personality. 
* Add a profile picture to your virtual profile if you have the option. It adds a more humanistic touch. If you do not feel comfortable using your image, please use one of the company logos instead. 

#### Specifics for attendees/staff at virtual sponsored conferences: Simulating Hallway Traffic and Connections
* One of the biggest advantages of virtual conferences is that you can often search the attendee list by company and title. Sales should especially take advantage of this as an easy way to find and connect with other attendees from your account. Be mindful to use a custom outreach that is relevant to that contact, as sending a generic message to thousands of attendees does not yield a positive image or create meaningful connections. 
* Attend and participate in discussions at sessions, in chat rooms, and other areas the event platform provides in topics that are relevant to you. This helps show us as thought leaders beyond just our own speaking slots and helps gain connections across the event. 
* Treat commitments to virtual events like you would to in-person events. While virtual events offer more flexibility to be able to avoid long flights and always having a quiet place to take a phone call, you should still be devoting your time to working the event like you would at an in person event. Block off a majority of your schedule during the event to really be engaged and participate.



## Partners
* Best practices for managing a virtual booth- suggestions to get the most out of your investment, have the most personable interactions and give best attendee experience. 
  * Always have a virtual offering, discount or giveaway.
  * If you can have a person or multiple people staff the booth and be there to answer questions.
  * have a video or digestible slide deck ready for viewing.

