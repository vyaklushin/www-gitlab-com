---
layout: handbook-page-toc
title: "Developer Evangelism"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Goal

To build GitLab's technical brand with deep, meaningful conversations on engineering topics relevant to our industry by leveraging our community of team-members and the broader ecosystem.

## What we do

The GitLab developer evangelists are engineers who enjoy learning and teaching the latest technology topics with the wider community. The evangelists are supported by program management within the team and also collaborate closely with the content and PR teams in corporate marketing to amplify their voices. There are two specific goals for the team:

1. **Thought leadership:** Drive GitLab awareness and brand value as full-time Developer Evangelists participating in thought leadership on au courant topics in the cloud native and cloud computing ecosystem.
1. **Ecosystem engagement:** Establish GitLab's technical thought leadership by building GitLab's influence in the tech community through participation in open source projects, foundations, and other consortiums. A description of consortiums can be found in our [Open Source Program handbook page](/handbook/marketing/community-relations/opensource-program/#consortium-memberships-and-sponsorships).
1. **Marketing value:** Work with other teams in GitLab (such as content, social, marketing programs) to repurpose, maximize the value of, and measure the impact of content created for thought leadership and ecosystem engagement.

### Social media

We build our thought leadership on social media. See [Developer Evangelism on Social Media](/handbook/marketing/community-relations/developer-evangelism/social-media/) to learn more about our strategies and become an evangelist yourself.

### Content creation 

We build out content to help educate developers around best practices related to DevOps, GitLab, remote work, and other topics where we have expertise. Content includes presentations, demos, workshops, blog posts, and media engagements. 

We maintain a [YouTube playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq-bYO9jCJaN45BBpzWSLAQ) with our talks, workshops and community engagements.

### CFPs

Our Developer Evangelists directly contribute to the wider community by speaking at conferences themselves. We also [support and manage responses to CFPs](/handbook/marketing/community-relations/developer-evangelism/cfps/) for team members across GitLab through our issue boards.

### Community engagement

Part of our role is to respond to engage with and answer questions from community members. We do this organically on social media, when prompted by our social media team or other GitLab team members, and by [monitoring GitLab and other selected keywords on Hacker News](/handbook/marketing/community-relations/developer-evangelism/hacker-news/).

### Projects

Our team maintains many projects to help show off technical concepts, engage with communities, provide examples of using GitLab with other technologies, and automate our team processes. See [Developer Evangelism Projects](/handbook/marketing/community-relations/developer-evangelism/projects/) for a list of all of those projects.

### OSS Contributions

We actively contribute to OSS projects and share our technical expertise. You can learn more about our ideas and visions in our [OSS contributions](/handbook/marketing/community-relations/developer-evangelism/oss-contributions/) handbook page.

## How we work 

### Issue tracker

We work in the open using the GitLab's [Corporate Marketing issue tracker](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues). We own the label `dev-evangelism` which can be applied to issues in any project under the [gitlab-com](https://gitlab.com/gitlab-com) and [gitlab-org](https://gitlab.com/gitlab-org) parent groups.

You can follow the latest our team is working on by looking at [our label-based issue board](https://gitlab.com/groups/gitlab-com/-/boards/1565342?&label_name%5B%5D=dev-evangelism).

### Issue labels

Using the [dev-evangelism](https://gitlab.com/groups/gitlab-com/-/labels?search=dev-evangelism) label on an issue means we are working on it or participating in the ongoing conversation. Team members are subscribed to issue/MR updates for this label.

The `dev-evangelism` label is accompanied by some labels we use to organize our work.

<i class="fas fa-info-circle" style="color: rgb(49, 112, 143)
;"></i>You only need to use the `dev-evangelism` label on issues requiring the attention of the Developer Evangelism team. Other labels are applied by the team as their state change or as contained in various issue templates.

{: .alert .alert-info}

#### General labels

| **CFP Labels** | **Description** |
| ---------- | ----------- |
| `DE-DueSoon` | This is used to monitor DE issues that are due soon |
| `DE-Peer-Review` | Feedback is needed on the issue from DE team members |
| `DE-Ops` | Used to label issues related to the Developer Evangelism `Ops in DevOps` theme |
| `DE-Dev` | Used to label issues related to the Developer Evangelism `Dev in DevOps` theme |
| `DE-k8s` | Used to label issues related to the Developer Evangelism `Kubernetes` theme |

### Issue management

The team creates issues for iteration, team discussions, and other issues for internal processes. These issues are tracked using the following labels:

| **Process Labels** | **Description** |
| -------------- | ----------- |
| `DE-Process::Open` | Process related issues that are still being discussed or worked |
| `DE-Process::Pending` | Process related issues on hold due to an external factor |
| `DE-Process::Done` | Completed Process issues |
| `DE-Process::FYI` | Issues that require no action from the team, but need to be aware of |

### Find us on Slack

GitLab team members can also reach us at any time on the [#developer-evangelism](https://app.slack.com/client/T02592416/CMELFQS4B) Slack channel where we share updates, ideas, and thoughts with each other and the wider team.

### Important bookmarks

- CFP
    - [Create CFP Meta Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=CFP-Meta)
    - [Create CFP Submission Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=CFPsubmission)
- [Create Developer Evangelist Request Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=developer-evangelist-request)
- [Events Spreadsheet](https://docs.google.com/spreadsheets/d/1E4J4Kx7Eq8JXuh_o4RfSRbGyjj8IeVCiPRLOGslLcfU/edit#gid=0)
- [Events Calendar](#our-events-list)
- [Team Shared Drive](https://drive.google.com/drive/u/0/folders/0AEUOlCStMBC9Uk9PVA) (Internal)
- [Weekly Meeting Agenda](https://docs.google.com/document/d/1oPXtlWNDbeut-dbFDLXBCfiBJLWwbzjjK9CFu5o8QzU/edit#) (Internal)
- [Team Collab Session Agenda Doc](https://docs.google.com/document/d/1Hs2DrUf9QJQR8fSsNO9WgX7c9sS46NSjEDuKHrrvPv0/edit#heading=h.vgcz1npa9iy) (Internal)
- Team Issue Boards
    - [General](https://gitlab.com/groups/gitlab-com/-/boards/1565342?&label_name%5B%5D=dev-evangelism)
    - [CFP](https://gitlab.com/groups/gitlab-com/-/boards/1616902?&label_name%5B%5D=DE-CFP)
    - [Content](https://gitlab.com/groups/gitlab-com/-/boards/1624080?&label_name%5B%5D=dev-evangelism)
    - [Milestones](https://gitlab.com/groups/gitlab-com/-/boards/1672643?label_name%5B%5D=dev-evangelism)

## Team members and focus areas

We are members of the [Community Relations team](/handbook/marketing/community-relations/).

1. [Abubakar Siddiq Ango](/company/team/#abuango) - Developer Evangelism Program Manager
    - DevSecOps with a focus on the Cloud Native Ecosystem
        - Kubernetes
        - CI/CD
    - Program management
        - Running the CFP process for conferences
        - Organizing Developer evangelism team's content creation and repurposing efforts
    - Language skills: English, Yoruba, Hausa
1. [Brendan O'Leary](/company/team/#brendan) - Senior Developer Evangelist
    - DevOps with a focus on the application developer perspective
        - SCM
        - GitOps
        - CI
        - .NET and Javascript communities
    - Language skills: English
1. [Michael Friedrich](/company/team/#dnsmichi) - Developer Evangelist
    - DevOps with a focus on the SRE, Ops engineers' perspective
        - CI/CD
        - Serverless
        - Observability
    - Language skills: English, German, Austrian
1. [John Coghlan](/company/team/#john-coghlan) - Manager, Developer Evangelism
    - Developer Evangelism
    - Meetups
    - Heroes

## Learn more about Developer Evangelism as a practice

A good overview with specific area definitions can be found in the [DevRel Notebook](https://github.com/konradsopala/devrel-notebook). 

We engage with Developer advocacy, relations and evangelism friends on social media:

- [Twitter list: Dev Avocados](https://twitter.com/i/lists/1012393598262874112/members) by Quintessence Anx (DevRel Collective founder)
- [Twitter list: DevRel](https://twitter.com/i/lists/1288789359865606145/members) by Michael
- [DevRel Contacts](https://docs.google.com/document/d/1ZX4BIwJTL0nVdkpRvLYDdk67jQfkRD_ErJWWHn-4KP8/edit) (Internal)

Our KPIs and processes follow industry best practices. We regularly iterate on new ideas and different strategies. The following articles can be helpful to explore new ways of Developer Evangelism:

- [Measuring Success and KPIs in Developer Relations - Community Contributed Outline](https://dev.to/tessamero/measuring-success-and-kpis-in-developer-relations-community-contributed-outline-1383)
- [Measuring the Impact of Your Developer Relations Team](https://openviewpartners.com/blog/measuring-the-impact-of-your-developer-relations-team/)
- [Developer evangelism and GitHub metrics - Or why stars are not the answer](https://devrel.net/strategy-and-metrics/developer-evangelism-github-metrics)
- [Developer Evangelism with Tessa Mero of Cisco](https://openchannel.io/blog/developer-evangelism-tessa-mero-cisco/)
- [What Do You Do as a Developer Advocate (🥑) at Elastic?](https://xeraa.net/blog/2020_what-do-you-do-as-a-developer-advocate-at-elastic/)

## Useful links

1. [How to be an evangelist](/handbook/marketing/community-relations/developer-evangelism/how-to-be-an-evangelist/)
1. [How to submit a successful conference proposal](/handbook/marketing/community-relations/developer-evangelism/writing-cfps/)
1. [Consortiums we work with](/handbook/marketing/community-relations/opensource-program/#consortium-memberships-and-sponsorships)
1. [Speaking logistics](/handbook/marketing/community-relations/developer-evangelism/speaking-logistics/)
