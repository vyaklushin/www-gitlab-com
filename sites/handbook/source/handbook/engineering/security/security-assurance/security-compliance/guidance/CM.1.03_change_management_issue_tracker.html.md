---
layout: handbook-page-toc
title: "CM.1.03 - Change Management Issue Tracker"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# CM.1.03 - Change Management Issue Tracker

## Control Statement

A production issues board is available in GitLab that outline project details for the production environment.

## Context

Tracking changes provides assurrence a change is not lossed or overlooked.

## Scope

This control applies to all changes that support the business of GitLab.com.

## Ownership

* Control Owner: `?`
* Process owner(s):
    * Finance
    * Business Operations
    * Infrastructure

## Guidance

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Change Management Issue Tracker issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/issues/1691).

Examples of evidence an auditor might request to satisfy this control:

* Screenshots of change management boards

### Policy Reference

## Framework Mapping

* SOC2 CC
  * CC8.1
