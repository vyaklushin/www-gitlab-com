---
layout: handbook-page-toc
title: "RM.1.04 - Service Risk Rating Assignment Control Guidance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# RM.1.04 - Service Risk Rating Assignment

## Control Statement

Risk management activities are prioritized based on an assigned residual risk rating as defined in the risk management policy.

## Context

The purpose of this control is to ensure that risk treatment activities are carried out in accordance to assigned risk ratings defined in the

## Scope

This control applies to all security risks identified in GitLab's environment as part of the annual risk assessment process or ad-hoc through other means.

## Ownership

* Control Owner: `Security Compliance`
* Process owner(s): `Security Compliance`

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Service Risk Rating Assignment control issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/issues/869).

Examples of evidence an auditor might request to satisfy this control:

* A screenshot of the risk treatment folder, showing risk treatment plans that are active against the total risks in the Risk Register

### Policy Reference

* [STORM Policy](/handbook/engineering/security/security-assurance/field-security/risk-management.html)
* [STORM Methodology](/handbook/engineering/security/security-assurance/field-security/operational-risk-management-methodology.html)

* [DPIA Assessments](/handbook/engineering/security/dpia-policy/#privacy-review)

## Framework Mapping

* SOC2 CC
  * CC2.1
