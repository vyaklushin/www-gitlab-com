---
layout: markdown_page
title: "IAM.4.03 - Remote Maintenance: Authentication Sessions"
---
 
## On this page
{:.no_toc}
 
- TOC
{:toc}
 
# IAM.4.03 - Remote Maintenance: Authentication Sessions
 
## Control Statement

Vendor accounts used for remote access are enabled only during the time period needed, disabled when not in use, and monitored while in use.
 
## Context

Limiting the ability for vendor accounts to access GitLab data directly reduces the attack surface of the organization.
 
## Scope
This control applies to:
   * GitLab.com
   * System that support the operation of GitLab.com
   * All applications that store or process financial data

## Ownership
TBD
 
## Guidance
TBD
 
## Additional control information and project tracking
Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Remote Maintenance: Authentication Sessions issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/issues/828) . 
 
### Policy Reference
TBD
 
## Framework Mapping
TBD
