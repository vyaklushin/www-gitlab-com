---
layout: handbook-page-toc
title: "GitLab SOC2 Response Process"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
[Security Team](/handbook/engineering/security/) > [Field Security Team Page](/handbook/engineering/security/security-assurance/field-security/index.html) > [Field Security Procedures](/handbook/engineering/security/security-assurance/field-security/procedures/index.html)

# Processing Requests for GitLab's SOC2 Report
GitLab's [Risk and Field Security Team](/handbook/engineering/security/security-assurance/field-security/index.html) is responsible for the Triage and response to [SOC2 Report Requests](/handbook/engineering/security/security-assurance/security-compliance/soc2.html#requesting-a-copy-of-the-gitlab-soc2-type-1-report)

This Handbook page covers the procedure for responding to SOC2 Requests

## Valid Sources
The following are valid sources for receiving a SOC2 request:
1. [Slack: #sec-fieldsecurity](https://gitlab.slack.com/archives/CV5A53V70)
1. Zendesk Queue: `security@gitlab.com`
1. Issue on the SA Triage Boards

## Valid Requesters
1. Account Owner - GitLab Team Member
1. Solutions Architect - GitLab Team Member
1. Technical Account Manager - GitLab Team Member
1. Employee of Eligible Account
- **NOTE** - GitLab will not deliver SOC2 report to an auditor of an Eligible Account; in this case an information security consultant from the Eligible Account is responsible to request the report and appropriately present to their auditing firm

## Flowchart
Below is a high-level flowchart for Responding to SOC2 requests. Procedure nodes link to more detailed instructions
```mermaid
graph TD
new[/New SOC2 Request Received\]
el[[Verify elegibility to receive report - link]]
yesel[Can Recieve]
noel[Can NOT Recieve]
rev{Does this request support Revenue?}
yesrev[yes]
norev[no]
prepare[[Prepare the SOC2 Report - INTERNAL link]]
track(Enter in Metrics Spreadsheet - INTERNAL link)
respond[[Send appropriate response - link]]

new --> |Risk & FS| el
el --> yesel --> rev
el --> noel --> |Risk & FS| respond
rev --> yesrev --> |Risk & FS| track
rev --> norev --> |Risk & FS| prepare
track --> |Risk & FS| prepare
prepare --> |Risk & FS| respond

click el "https://about.gitlab.com/handbook/source/handbook/engineering/security/security-assurance/field-security" "Vendor Contracts Marketing Events Issue"
click prepare "https://gitlab.com/gitlab-private/sec-compliance/field_security/watermarker/-/blob/master/runbook-gitlabci.md" "Prepare with SOCoNator"
click track "https://docs.google.com/spreadsheets/d/1nGlg_8PYRpEsr1bkrnTUijy6s3JDaA_Fls6YC9bssM4/edit?usp=sharing" "Field Security Metrics Spreadsheet"
click respond "https://about.gitlab.com/handbook/source/handbook/engineering/security/security-assurance/field-security" "Responses"

style yesel fill: #00ff00
style yesrev fill: #00ff00
style noel fill: #ff0000
style norev fill: #ff0000
```
### Verify Elegibility
```mermaid
graph TD
sfdc{Valid account in SFDC?}
sfyes[yes]
no
noreport[NOT able to receieve report]
yesreport[Able to send report]
cust{Current Customer under Terms?}
yescust[yes]
nocust[no]
nda{Does Account have valid NDA?}

sfdc --> sfyes
sfyes --> cust
sfdc --> |No entry in SFDC| no --> noreport
cust --> yescust --> yesreport
cust --> nocust --> nda
nda --> |No confidentiality protection| no
nda --> yescust

style sfyes fill: #00ff00
style yescust fill: #00ff00
style yesreport fill: #00ff00
style no fill: #ff0000
style noreport fill: #ff0000
style nocust fill: #ff0000
```
- It is important to understand why a report is not elegible to be sent. It will be because either:
1. `No entry in SFDC`
1. `No confidentiality protection` - No terms (Customer), No NDA (Prospect)
- The reason we can't send a report will directly impact how the Risk and Field Security Team responds to the request.
<br/>[Back to Flowchart](#flowchart)

## Respond
### Eligible to Receive Report
#### Initial Receipt - #sec-fieldsecuirty
- React with an emoji to the request (indicating analyst is aware)
- Reply with the `:page_facing_up:` emoji to indicate SOC2 report
- Reply **in thread** to indicate you will handle report and set expectations
>Hello {user}, I will get this out {timeline}
- Follow further instructions as indicated in the [INTERNAL: runbook for preparing a SOC2 report](https://gitlab.com/gitlab-private/sec-compliance/field_security/watermarker/-/blob/master/runbook-gitlabci.md)
<br/>[Back to Flowchart](#flowchart)

#### Initial Receipt - Zendesk Queue
- Reply with the following text and submit the ticket as `Solved`
>Hello [requester],

>Thank you for your inquiry and we are thankful for your business!

>We are confirming receipt of your request and you have been added to the queue of reports to be sent out. You will be receiving the requested report as an encrypted file within 1 business day, you will receive a second email containing the password for opening the password protected file.

>While you are waiting on the report, you may be interested in browsing our Customer Assurance Package (https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/customer-assurance-package.html). This package includes instructions on accessing our completed Cloud Security Alliance (CSA) Consensus Assessments Initiative Questionnaire (CAIQ).
- Follow further instructions as indicated in the [INTERNAL: runbook for preparing a SOC2 report](https://gitlab.com/gitlab-private/sec-compliance/field_security/watermarker/-/blob/master/runbook-gitlabci.md)
<br/>[Back to Flowchart](#flowchart)

#### Initial Receipt - SA Triage Board Issue
- Respond as a note to the issue indicating you will deliver
>Hi {handle}, I will get this sent out {timeline}
- Follow further instructions as indicated in the [INTERNAL: runbook for preparing a SOC2 report](https://gitlab.com/gitlab-private/sec-compliance/field_security/watermarker/-/blob/master/runbook-gitlabci.md)
<br/>[Back to Flowchart](#flowchart)

### NOT Eligible to Receive Report
#### Initial Receipt - #sec-fieldsecurity
- React with an emoji to the request (indicating analyst is aware)
- Reply with the `:page_facing_up:` emoji to indicate SOC2 report
- Reply **in thread** to explain
>Hello {user}, we can't currently send this report because {reason}

#### Initial Receipt - Zendesk Queue
- If there is a valid acount in SFDC:
    - CC the Account Owner
    - Attach **a blank copy** of the [GitLab Mutual NDA](https://drive.google.com/a/gitlab.com/file/d/1hRAMBYrYcd9yG8FOItsfN0XYgdp32ajt/view?usp=sharing)
    - Reply with the following text and submit the ticket as solved
    >Hello [name],

    >Thank you for your inquiry and we are thankful for your interest in GitLab!
    
    >We are confirming receipt of your request and you have been added to the queue of reports to be sent out. Your account team (cc'd) needs to get an NDA on file for your company prior to us providing you the report because of our contractual obligations with our auditor. Once an NDA is on file, we will send the report as an encrypted archive (zip) file and then send a second email containing the decryption key. You may need a third party tool like 7zip or keka to properly extract the report from the archive.
    
    >While you are waiting on the report, you may be interested in browsing our Customer Assurance Package (https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/customer-assurance-package.html). This package includes instructions on accessing our completed Cloud Security Alliance (CSA) Consensus Assessments Initiative Questionnaire (CAIQ).
    
    >For convenience I'm attaching our GitLab mutual NDA. Once this is executed and provided to your GitLab Account Representative, we should be able to provide you the report within 1 business day.
- If there is no account in SFDC, respond with the following and submit the ticket as `Solved`
>Hello [name],

>Thank you for your inquiry and we are thankful for your interest in GitLab!

>I am having trouble locating you within our Customer Relationship Management (CRM) system. If you are a current customer I apologize for the inconvenience, please contact your GitLab Account Executive or Technical Account Manager and they should be able to further assist. If you are not currently a paying GitLab customer you can start the process by reaching out to our GitLab Sales team (https://about.gitlab.com/sales/).
