---
layout: handbook-page-toc
title: "Field Flash Newsletter"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview 
Field Flash is a monthly newsletter that recaps all important Sales/CS-related information from the month and highlights important upcoming information. The main goal of this communication is to **help the field sell better, faster, and smarter.**

Ongoing feedback and participation from the field is imperative to the success of this program. If you have feedback on the current process or would like to request a certain type of content or update for future newsletters:
- [Capture it using this quick process](/handbook/sales/field-communications/#sharing-feedback)
- Reach out in the #field-enablement-team Slack channel

## Target Audience
The newsletter is sent to members of the field (sales + customer success) and stakeholders (i.e. SDRs). **Please note that this email is considered confidential and is, therefore, intended for internal audiences only.** Please do not forward externally. 

## Opportunities/Requirements 
After interviewing GitLab team members from across the org and surveying the field, we identified the following as key requirements of the Field Flash newsletter: 

The newsletter will...

**Uphold our [values](/handbook/values/) of transparency**
- The newsletter is for communication, the Handbook is for documentation. This means that the newsletter will disseminate updates but lean on the Handbook (and other relevant resources) as the main source of documentation, linking back to it wherever possible. 
   - Note: A tangential benefit of this approach is that it will allow us to improve the [Sales Handbook](/handbook/sales/) during this process. 

**Prioritize repetition, brevity, user-friendliness and added value**
- The newsletter will focus on short lists and bullet points and will link out to more robust resources. 
- Repetition is key to adoption. We will not focus on a single channel, but rather notify the field of a new newsletter via Slack and (eventually) Salesforce. 
- We don't want the team to see the newsletter as more noise. A key to adoption will be successfully positioning it as THE resource to learn whats new and recap important information. Everything will be tied back to the payoff to the seller when possible. 
- We must reconcile the fact that this newsletter is yet another increase in communication. We will leverage it to cut down on other communications when possible. 

**Be fun to look at and read**
- A focus on multimedia is important in order to help the newsletter break the monotony of text we sift through each day. 
- We will use images, gifs, emojis, and video where possible. For example, instead of doing a written win-wire, we will interview the individual and embed that 30-60 second video in the newsletter.

**Help the field operationalize key messages** 
- We will organize information around our 3 main value drivers when possible. 
- We will frequently reiterate SKO messages through video clips and use-case examples. 

**Be an opportunity for the field to "learn themselves"**
- A peripheral goal of the newsletter is to advertise helpful resources to the field. We will provide helpful information in hopes that it will encourage team members to seek out the source of that information and look for additional information once there. 

**Highlight *all* aspects that make a big win possible**
- There are a lot of new reps who are still ramping, and they want to learn from others. We will spotlight everything that went into winning a deal, including any customer-facing presentations, RFPs, whitepapers, etc. Other team members can reference these later, and it may save them cycles by not having to duplicate work.
- We will include context around partners and alliances when they play a role in a deal.

**Keep the onus on individuals to stay informed**
- GitLab is asynchronous, and this update is no exception. The onus remains on individual team members to stay informed and connected. The newsletter is not a substitute for the Handbook or other resources salespeople should be leveraging on the day-to-day.

**Be general enough to allow us to remain segment-agnostic**
- The newsletter will include general updates and resources that are applicable to most, if not all, team members. Future field communications programs will enable individual field segments (CS, ENT, COMM, PubSec, etc.) with more frequent updates that are tailored to their teams. 

**Be built out in the open**
- The newsletter content will be compiled in an issue each month within the [Enablement project](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement). Any team member is welcome to contribute or make requests. See more information in the Process section below. 

**Uphold our value of "everyone can contribute" – we will measure success and [gather feedback often](/handbook/sales/field-communications/#sharing-feedback)** 
- We will measure success using a combination of quantitative and qualitative success metrics. See Measurement section below. 
- Giving feedback or making requests will be easy, and all input will be considered and addressed.
- The team is committed to upholding the value of the newsletter – information should be relevant, feedback should be actioned on, and leadership should help reiterate by pointing to it as a useful resource for their teams.

**Bolster our sense of entity and team**
- Each newsletter will include a spotlight on a sales or CS team member so the wider team has a chance to get to know them better and learn something from them.

## Format 
Based on the requirements above, this is the first iteration of the newsletter format: 

1. Featured
   - The announcement we think is most impactful to the field. We will try to communicate this in an image with 1-2 lines of text + 1-2 links to references.
1. Deal of the month
   - Video of sales/CS team member(s) overviewing the opportunity and/or customer and explaining how they won the deal + links to any customer-facing collateral they used (that can be publicly shared).
1. New and noteworthy resources
   - New skills and tools like competitive intel, customer case studies, or a customer proof point highlight. 
1. Did you know? 
   - Updates from sales ops and other stakeholders related to changes in the way we work or interact with other teams. 
1. What's new in GitLab
   - The top 3 takeaways from the latest GitLab release, mapped to one of the three value drivers and framed in the context of the customer value. 
1. Upcoming events
   - Where GitLab will be over the next two months, divided by region. What webinars and other virtual events we will participate in. 
1. Team member spotlight
   - Short, written Q&A highlighting a member of the Sales/CS/SDR teams or supporting teams (can be new or tenured). They answer these questions:
      - Where did you grow up, and where are you based now?
      - What is your favorite thing about your job? 
      - Which of our [GitLab Values](https://about.gitlab.com/handbook/values) resonates most with you? 
      - Do you have a helpful tip for working at GitLab and/or in the Field org to share?
      - What is a hobby or skill you have that the team might not know about?
      - Icebreaker question of the month (i.e. What are your favorite songs from your teenage years that you still rock out to when nobody else is listening?)
1. Enablement corner
   - Updates from field enablement regarding new or updated trainings + opportunities to reinforce SKO learning objectives. (i.e. Did you know...?/Did you remember that...?)

## Process 
The newsletter is sent out **on the first Tuesday of each month at 9 am PT.** Based on feedback from the field, we will iterate on timing. 

We build the newsletter in an issue in the [Enablement project](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement). The process for the issue includes: 
- The issue for the upcoming newsletter is opened at least two weeks before first Tuesday of the next month. 
- Relevant stakeholders/contributors are tagged in the issue each month to provide content or review suggested content in the outline. 
- Once the outline is complete, Field Communications drafts the newsletter in a Google document linked to in the issue  and then imports the content into the newsletter template in MailChimp. 
- Field Communications sends the newsletter test email to relevant leaders and stakeholders for review no more than two business days before planned send date.
- Reviewers provide feedback no later than 3 pm CT on the business day before the planned send date to allow time for revisions and scheduling.
- Field Communications schedules the newsletter to send at 9 am PT on the planned send date. 
- Once the newsletter goes live, Field Communications sends a reminder to the field in the #sales, #customer-success and #sdr_global Slack channels 
- On the Friday following the newsletter send, Field Communications captures the newsletter performance data in the issue and then closes it. 

To be added to the newsletter distribution list, [use this signup form.](http://eepurl.com/gXvm8r) **Note:** You can only sign up for this newsletter with a GitLab email domain. 

## Deal of the Month Nominations
Deals that qualify for the Deal of the Month video meet most of the criteria below. 

While submissions often come from Sales leadership, nominations can and should be made by any GitLab team member - everyone can contribute! To make a Deal of the Month nomination, use this [Deal of the Month Nomination Form](https://forms.gle/7AYwzgFStrai5D2F7). Nominations close 7 business days prior to the target publish date (first Tuesday of each month). Any nominations received after this date will be considered for the following month's edition. Note that, in an effort to feature recent deals, nominations are only valid for one edition cycle, i.e. nominations do not carry over to a future month if not selected.

**Deal of the Month Criteria**
- This deal is a shining example of what great looks like. New and tenured team members would benefit from learning about all of the work and various components that went into it.
- The deal is a good example of "landing with vision and expanding with purpose."
- The team member articulated a defensible "Why do Anything."
- Team member and/or account team successfully formed a meaningful relationship with the customer/prospect, earning their trust as a part of this deal.
- The Close Plan for high fidelity line items includes dates with actions/outcomes.
- Team member and/or account team used the value framework to guide the conversation with the customer/prospect.
- Team member and/or account team successfully used the MEDDPICC methodology for opportunity management.
- Team member and/or account team exhibited [GitLab's values](/handbook/values/) in this process.
- Team member and/or account team received positive feedback from the customer/prospect or from another GitLab team member as a part of this deal. (Please share if possible.)

## Team Member Spotlight Selection Process

The Team Member Spotlight features a member of the Field organization or supporting team (i.e. legal or product) and is an opportunity for the Field team to get to know them better. The team member featured in the monthly spotlight is chosen by a Field Enablement team member on a rotating basis in coordination with the team they support. For example, the Field Enablement Program Manager supporting Enterprise has an opportunity to nominate someone in the Enterprise segment, followed by the Field Enablement Program Manager supporting Commercial the following month, and so on.

**Team Member Spotlight Criteria**
- This person goes above and beyond in their role.
- This person is a high-performer and makes an impact across the field organization – both excelling at their role and successfully and meaningfully collaborating with other teams.
- The Field has something to learn from this person – from their role, to their work ethic, to their attitude, etc.
- This person lives the [GitLab values](/handbook/values/).

## Measurement
Quantitative Success Metrics
- Email open rate - Average open rate of 55% in first 6 months. 
- Click rate - Average click rate of 10% in first 6 months.
- Increased sentiment around information flow - 20% increase in "agree/strongly agree" responses to this survey question: "As a sales/CS team member, I feel informed on all critical information related to my role."

Qualitative Success Metrics
- Increased engagement from field team members/leaders and stakeholders in regards to the newsletter – feedback, requests, suggestions, etc. 
- Usefulness of newsletter content as shown by other stakeholders using newsletter content for their own work. 
- Improve the Sales section of the Handbook as a result of work on the newsletter. 

## Past Newsletters

| **Edition** | **Subj Line** | **Highlights** |
| ------ | ------ | ------ |
| [March 2020: 2020-04-02](/handbook/sales/field-communications/field-flash-newsletter/edition-2020-04-02) | We're Live! Time to Level Up with Field Flash. ⚡️ | Partner Program launch, COVID-19 resources & sales process changes |
| [April 2020: 2020-05-05](/handbook/sales/field-communications/field-flash-newsletter/edition-2020-05-05) | GitLab Field Flash: Your Top Field Highlights from April ⚡️ | GitHub competitive resources, GTM Field Update, requirements management |
| [May 2020: 2020-06-02](/handbook/sales/field-communications/field-flash-newsletter/edition-2020-06-02) | GitLab Field Flash: Your Top Field Highlights from May ⚡️ | Pricing overview, QBR ask follow-ups, GitLab 13.0 milestone release |
| [June 2020: 2020-07-07](/handbook/sales/field-communications/field-flash-newsletter/edition-2020-07-07) | NEW ⚡️ GitLab Field Flash: June Edition ⚡️ | GitLab Compete page, GitLab vs. BitBucket, Ecosystem Technology Partners |
| [July 2020: 2020-08-06](/handbook/sales/field-communications/field-flash-newsletter/edition-2020-08-06) | NEW ⚡️ GitLab Field Flash: July Edition ⚡️ | Enablement resources, QBRs, Channel momentum |
| [September 2020: 2020-09-01](/handbook/sales/field-communications/field-flash-newsletter/edition-2020-09-01) | NEW ⚡️ GitLab Field Flash: September Edition ⚡️ | Gitlab Virtual Commit 2020 recap, President's Club 2021, Sales Ops updates |
| [October 2020: 2020-10-08](/handbook/sales/field-communications/field-flash-newsletter/edition-2020-10-08) | NEW ⚡️ GitLab Field Flash: October Edition ⚡️ | Q3 Cannonball Run, ROI Calculators, APAC Momentum, Public Competitive Resources |
