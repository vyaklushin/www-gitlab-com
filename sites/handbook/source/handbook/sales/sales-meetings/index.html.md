---
layout: handbook-page-toc
title: "Sales Meetings"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The below page overviews the primary meetings for the WW Field Team. 

## Bi-Weekly WW Field Sales Call

* When: Every other Monday 9:00am to 9:25am Pacific time.
   * The call is rescheduled on observed US holidays that fall on Mondays.
* What: This meeting includes all members of the CRO organization as well as stakeholders of the field team. Its primary purpose is to drive knowledge-sharing across the field organization, with an emphasis on CRO and leadership business/organizational updates and Q&A time for field team members and leadership.
* If you would like to add topics to the agenda, please coordinate with Field Communications using their [announcement request process.](/handbook/sales/field-communications/#requesting-field-announcements).

### Format
1. CRO State of the Union (10 min)
1. CRO Leadership & Guest Speaker Updates (5 min)
1. Update Ticker (read asynchronously)
1. Q&A (5-10 min)

### General Guidelines for Attendees
1. This call should be a discussion/review rather than a presentation. Please ask questions – this is our chance to learn together as a team. Add questions to the Q&A section of the agenda.
1. Any updates that are not covered on the call will be addressed asynchronously in the agenda. Please read the agenda in full each time.
1. The agenda can be found by [searching for `sales agenda`](https://drive.google.com/drive/u/0/search?q=sales%20agenda) in Google Drive.
1. Recordings of past calls stored in [Google Drive](https://drive.google.com/drive/u/0/folders/0B5OISI5eJZ-DTndfelNnbXViUjA) and uploaded to the [YouTube GitLab Unfiltered: WW Field Sales Call Playlist.](https://www.youtube.com/playlist?list=PL05JrBw4t0KpO-kgCZK0QEC6x1-PtojhZ)
1. If you cannot join the call, consider reviewing the recorded call, or at minimum, read through the meeting agenda.
1. If you have feedback on how we can improve the WW Field Sales Call, [share it with Field Communications.](/handbook/sales/field-communications/#sharing-feedback)

### General Guidelines for Presenters
1. Provide your final agenda times by end of day on the business day before the meeting. Your agenda updates should link to relevant references (Handbook pages, issues, MRs, etc.) that provide more context, and you should include where the field can go with more questions and/or feedback that isn't discussed on the call.
1. Please be respectful of time and keep your updates to 1-2 minutes max – focusing on the most impactful learning or takeaway. We want to leave ample time for questions.
   1. If you feel your update requires more context, consider recording a quick video with more background that the field can watch asynchronously.
1. Slides are not necessary, but if you would like to present slides, ensure that you also include a link to the presentation in the agenda.
1. Once you are done presenting, please handoff to the next presenter on the agenda.

## Weekly Thursday Sales Enablement Call led by the Training and Enablement team

* When: Happens every Thursday at 9am Pacific time

## Sales Monthly [Key Meeting](https://about.gitlab.com/handbook/finance/key-meetings/)

* When: Happens once every month
* Where: Zoom link varies - please check the calendar invitation
* What: Michael McBride (CRO) will present to the CEO, CFO, and other company leaders a monthly Sales update. 

This update will include the current status of:
   * [Sales KPI's](https://about.gitlab.com/handbook/sales/performance-indicators/)
   * [OKR's](https://about.gitlab.com/company/okrs/fy21-q3/)

The meeting will cover:
   * the quarterly forecast
   * QTD performance
   * most recent month's performance

We will provide key takeaways as to what is on-track (green), needs improvement (yellow), or is of urgent concern (red). 
We will review the standard IACV/ARR look back on historical performance, as well as the Pipeline X-Ray, which includes future-looking metrics like pipeline generation, coverage, and pacing for the current and next quarters.

## Sales Group Conversation

* When: Happens once every 8 weeks at 8am Pacific time
* Where: Zoom link varies - please be sure to check the calendar invitation
* What: The CRO Leaders will rotate and provide the company with an update on their business, overall team performance, notable wins, accomplishments, and relevant strategies planned.
You can view previous updates in the "Group Conversation Sales" (previously "Functional Group Update Sales") folder on the Google Drive.
   * CRO Rotation: Michael McBride (CRO), Commercial Sales, Customer Success, Enterprise Sales, Channels

## Quarterly Business Reviews
For a full overview of the QBR planning and execution process, QBR request tracking, best-practices for organizers and attendees, and where to find past QBR content, [see here](/handbook/sales/qbrs). 

## WW GTM Field Update
This update takes place in the first week of each new quarter and is meant to give Field team members a summary of activities and results across all go-to-market (GTM) functions in preparation for their QBR presentations. The meeting includes updates from: 
- Sales 
- Product
- Marketing
   - General Marketing 
   - Revenue Marketing 
   - Strategic Marketing 
- Customer Success
- Channel
- Alliances  
- Field Operations 

The format of this update changes - from a synchronous meeting with department presentations followed by a Q&A to an asynchronous video recording with an issue or office hours to address questions – based on the timing of EOQ and its proximity to QBRs. 

## Focus Fridays
The Field team is piloting Focus Fridays through the end of CY 2020. The goal is to encourage all field team members to clear out their calendars on Fridays as much as possible in hopes that this will help all of us be more efficient, allow space within our weeks for focused work, and reduce potential burnout. Guidance for Focus Fridays includes:  
- Cancel or move any standing meetings occurring on Fridays
- Customer/prospect/partner/external meetings are exceptions
- Some one-off internal meetings might not be avoidable, but aim to minimize them as much as possible
- Consider blocking off your calendar as "busy" on Fridays to block your work time
- Consider looking into apps like [Clockwise](https://www.getclockwise.com/) that can provide recommendations on how to refactor your calendar for focused work 

You are encouraged to talk to your manager for guidance on how best to embrace Focus Fridays on your team and with your individual schedule. 
