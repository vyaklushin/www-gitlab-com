---
layout: handbook-page-toc
title: "Channels"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Channels OKR

[OKRs](/company/okrs/)

### Channels Value 

The Channel is a critical part of our strategy moving forward as it will help us:
1. Drive growth ARR through services capacity and capability to drive customer adoption and usage of the GitLab platform.
1. Drive and increase new customer ARR through their relationships, service engagements, and knowledge of accounts.

![GitLab Channel Value](/handbook/sales/channel/images/channel_handbook1.png)

The purpose of this page is to provide the  GitLab Sales team insights on the GitLab channel partner community and how to best work with our partners.

Additional related channel resources:



*   [Channel Operations Page](/handbook/sales/field-operations/channel-operations/)  - Learn how to manage partner deals in Salesforce and other operational tips when working with partners.
*   [Channel Partner Program page]( /handbook/resellers/) - This page provides an overview of the GitLab channel partner program, the expectations of partners, benefits for partner and how partners can work with GitLab. 


## **Channels Partner Types**


### **Resellers**

Primary monetization is through reselling GitLab licenses and services. Resellers can be a service partner too (often known as a Solution Provider).



1. VAR/VAD (Value Added Reseller or Distributor): Channel services including resale, implementation, contracting, support, financing etc.
2. DMR (Direct Market Reseller): Primary business is resale of the software, often does not implement. Value are the contracts that these partners have in place with customers.


### **Services partners**

Primary monetization is through the sale of services. This can be a one-time implementation, ongoing support or advisory, managed services, or outsourcing. Services partners will resell GitLab services, deliver services on behalf of GitLab or deliver GitLab certified services. Services partners can be a reseller partner too (often known as a Solution Provider).



1. Global Systems Integrators have a large global workforce and can deliver on almost any customer need. Examples: Accenture, Deloitte, TCS, Wipro
2. Regional Systems Integrators are large workforce but with single continental focus and a more limited offering of services. Examples: CI&T, Slalom
3. Boutique Systems Integrators are very focused DevOps partners that could be deep experts on GitLab and the nuances of getting it setup and running it. Examples - CloudReach, Flux7
4. Managed Service Providers provide ongoing support for solutions/applications. Examples: Rackspace


### **Alliances**

[Alliances](/handbook/alliances/)

**How can GitLab sellers benefit from our channel partners?**

GitLab channel partners have established sales forces that help us multiply our sales coverage.  Through that coverage and deep customer relationships, partners can identify new customer opportunities and new sales opportunities with existing customers, resulting in ARR growth.  Through their service offerings and vendor partnerships, they can deliver more complete solutions than GitLab can alone, and drive customer adoption of GitLab.  As a result, the GitLab channel will:



1. Improve customer reach and experience
    1. Resellers/SI’s that know & can make intros to  your customers
    2. Ease of order processing
2. Channel leverage
    3. Resellers/SI’s that bring us into new opportunities & grow existing
    4. Decreased Customer Acquisition Cost 
    5. Improved Net & Gross Retention
3. Services Capacity to Adopt and Expand GitLab deployments
    6. SI’s that help our customers deliver more value 
    7. Stage Monthly Active Users Acceleration
4. Market Position
    8. Acquire partners from the Atlassian/Cloud Bees channel 
    9. Monetize the GitHub channel 
    10. Align to the AWS channel

**Partners can help you scale and working with them is comp neutral**

**[Channel Neutral Compensation](/handbook/sales/commissions/#channel-neutral-compensation)**

To incentivize working with our Channel partners, 'Channel Neutral' means that we will not reduce $ value to individual sellers even if the Channel business reduces total iACV to GitLab (via disocunts or rebates).
More information can be found on the [compensation page](/handbook/sales/commissions/#channel-neutral-compensation).

FY21 commissions will be channel neutral for all deals through partners (including Distributors, if applicable), which means standard partner discounts are credited back to the salesperson and must follow [order processing procedures](/handbook/business-ops/order-processing/) to correctly flag partner attribution. Total IACV on the deal after all discounts will count towards quota credit, but the channel neutral amount does not qualify for quota credit and only pays out for compensation at BCR. See Channel Neutral section referenced in the [FY21 Commission Plan presentation](https://docs.google.com/presentation/d/1lPZAmdHPPJwhJ4felwsgDXopIt7Usn3UnK5siOpQMlY/edit#slide=id.g7da31a6494_6_0)

Partner Services also qualify for [FY21 Professional Services Spiff (v2)](/handbook/sales/commissions/#fy21-professional-services-spiff-v2).  So as partners help customers adopt and expand their GitLab deployments, they can earn a Services Attach rebate and the GitLab sales team can earn the Professional Services Spiff.

**Maximizing value by working with Channel Partners**

Developing successful relationships with channel partners in your region requires some effort, but has a great payback.  Following are a few key tips that will help you develop relationships with GitLab channel partners and greatly increase your sales effectiveness.



*   **Honor Partner ←→ Customer Relationships**
    *   **Respect Deal Reg’s from Partners:  <span style="text-decoration:underline;">No</span>** direct sales of opportunities with approved Partner Deal  Reg without prior approval of VP, Global Channel Sales 
    *   **Recognize Incumbent Partners***: Do not bring another Partner into your customer convos
*   **For new Deal Reg’s in your pipeline**
    *   Be prepared to construct a simple joint pursuit plan in SFDC (CAMs are here to support same)
    *   Strive to team with the trained Partner people
*   **The program determines the partner discount**
    *   Our agreement with partners is that by initiating, assisting or fulfilling sales opportunities, or selling services, partners will earn set amount based on their partner track.
    *   These discounts are determined by the program and not GitLab Sales on individual opportunities.
*   **Respect incumbent partners**
    *   Support them in driving renewals and incremental sales
    *   Do not bring competing partners into the account
*   **Stay Focused on the Customer’s Needs & Priorities**
    *   Partners can complete a customer solution with additional products and services
*   **Give to Get (LevelUp coming)**
    *   Bring Select/Open partners into GitLab opportunities
    *   Ask them about opportunities they may have for GitLab
*   **Collaborate with Partners in your Region (LevelUp coming)**
    *   Treat them as an extension of your team
    *   Plan with key partners in your region
    *   Communicate regularly with them around sales opportunities and pipelines
    *   Help keep GitLab top of mind with them.  Educate them on GitLab.
*   **If misalignments or escalations arise during deal pursuits**
    *   Contact the CAM
    *   Chat with your peers that have channel co-selling experience
    *   Escalate to your manager 
    *   No customer collisions: Strive to stay aligned \
on customer calls - take differing POVs offline 

### GitLab Channels Program Updates - April 2020

1. Building a channel of enabled, DevOps & Digital Transformation focused resellers and services providers
1. Provide eStore access for SMB & Midmarket channel partners
1. Net neutral to GitLab seller compensation
1. Incentives to identify net new customers & opportunities in existing customers
1. Incentives to attach product, operational & strategic services
1. Referral fees for non reselling services partners
1. MDF available for Demand Generation activities and events
1. Sales & SE Enablement available on demand; certifications - H2’FY21
1. Services Certifications - H2’FY21
1. Renewals incumbent protection: If a partner sells a deal and is in good standing (actively supporting the customer, etc) that partner receives first right of refusal for renewal; unless otherwise stated by the customer


##  Channel Operations

Information on managing channel opportunities, deal registrations and other valuable information about how to work with your partners can be found on the [Channel Operations page](/handbook/sales/field-operations/channel-operations/#welcome-to-the-channel-operations-page).


##  Definitions

Program and incentive definitions can be found on the [Channel Operations page](/handbook/sales/field-operations/channel-operations/#program-and-incentive-definitions).


## I need help!  How to do I reach out to channel experts?

The quickest way to get help is by using the following Slack channels:
- #channel-sales
- #channel-ops
- #alliances


## Channels groups, projects, and labels

**Groups**
Use the GitLab.com group for epics that may include issues within and outside the Channels Team group. 
- https://gitlab.com/groups/gitlab-com/-/boards/1508300?label_name[]=Channel

- Guidelines for  Partner Folders:  
  - The partners Group is further divided into regional sub-groups
  - Within each region sub-group Partners will get their own group
  - Partner groups contain a collaboration project and internal project
  - Partner employees should be explicitly invited to collaboration group
  - Internal group should not be visible to non-GitLab employees and may contain licensing details or other sensitive information
  - Additional projects may be created within the partner subgroup to contain code bases for prototypes or PoV
  - Please avoid creating additional subgroups within partner groups 

**Projects**
- Create issues under the “Channels” project

**Epics**
- From FY21 Q3 - OKRs will be formed as EPICs and issues related to that Epic should be associated 
- Broad initiatives will have EPICs with several issues to support that project 

**Labels**
  - **Team labels**
      - Channel- issue initially created, used in templates, the starting point for any label that involves Channels
      - Channel Ops - label for issues that directly impact the Channel Ops & team.  DRI will be defined in the intro of the issue
      - Channel Program  - label for issues that directly impact the Dir of Channel Programs  & team. DRI will be defined in the intro of the issue
      - Channel Services- label for issues that directly impact the Channel Services Manager. DRI will be defined in the intro of the issue
      - Channel Marketing- label for issues that directly impact the Channel Marketing team. DRI will be defined in the intro of the issue
      - Channel Distribution- label for issues that directly impact the Distribution leader. DRI will be defined in the intro of the issue
      - Channel GSI - label for issues that are owned by the Dir of GSI. DRI will be defined in the intro of the issue
      - Internal Channel Enablement- label for issues that are focused on Internal Channel Enablement issues. DRI will be defined in the intro of the issue
      - Channel Handbook Needs- label for issues that are about pending or planned Channel Handbook changes. DRI will be defined in the intro of the issue
      - QBR - Requests from Sales QBRs

   - **Priority Weighting (using Eisenhower matrix and weighted tabs in Gitlab)** 
      - WEIGHT 1 ~ Channel Priority:1 - Home runs (high value to GitLab and high likelihood of success that align to Sales & Channel OKRs) and committed to completion within stated milestones. This category will be limited because not everything can be a priority.  These are both URGENT & IMPORTANT 
      - WEIGHT 2 ~ Channel Priority:2 - Big Bets (high value to GitLab, lower time urgency, longer dependencies or lower likelihood of success) within stated milestones.  These are not urgent but IMPORTANT to our success
      - WEIGHT 3 - Channel Priority:3 - Small wins within stated milestones.  These are URGENT but not strategically important.  Delegate or push out 
      - WEIGHT 4 - Channel Priority:4 - Small wins that are important but high value.  Should be slotted in where backlog allows 
      - WEIGHT 5 - Channel Priority:Backlog - Things in the queue not currently being worked LABEL 

**DRI** 
To be stated in intro of issue and assigned to that person.  There maybe 1 or more assignee but the DRI should be stated intro of issues

**Milestones**
- Milestones are within 2 week windows 
- Channel - FY2xQxM(month#)a (1st-15th) or b(16th-30th),
- Like this: Channel - **FY21Q2 3a**

**Due Dates**
What is the expected due date of completion or NBA (next best action - next key iteration and should be mentioned in the issue)? 




