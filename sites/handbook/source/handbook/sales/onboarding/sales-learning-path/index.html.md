---
layout: handbook-page-toc
title: "Sales & Customer Success Quick Start Learning Path"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Sales & Customer Success Quick Start Learning Path - Core Curriculum

### 1.Welcome to GitLab Sales!
*  **ACTIVITY: Let's Git To Know Each Other! (Assignment, 5 minutes, 1 Point - Pre Work)**
   - Please complete this brief [google form](https://docs.google.com/forms/d/e/1FAIpQLScXH3QSAcqUP4mRJsqUWbn7BUJS_SYJVjFg2oXqOoOwzBMzLA/viewform) to introduce yourself.
*  **What is GitLab? (Material - Video, 31 Minutes)**
   - GitLab is a single application for the entire DevOps lifecycle. [Watch the video](https://www.youtube.com/watch?v=MqL6BMOySIQ).
   - GitLab customers buy GitLab for 3 primary reasons: 1) Increase Operational Efficiencies, 2) Deliver Better Products Faster, and 3) Reduce Security and Compliance Risk. Check out the [GitLab Customer Deck](https://docs.google.com/presentation/d/1SHSmrEs0vE08iqse9ZhEfOQF1UWiAfpWodIE6_fFFLg/edit?usp=sharing) and [watch the video](https://www.youtube.com/watch?v=MqL6BMOySIQ).
*  **Company Infomercial (Material - Video, 5 Minutes)**
   - Check out our [company infomercial](https://youtu.be/gzYTZhJlHoI)!
*  **GitLab Culture (Material - Video, 3 Minutes)**
   - Every year, our entire remote workforce gets together in one location for the GitLab Summit. We use this time to bond, build community, and get a bit of work done. It's an essential part of the GitLab experience—watch the video to learn more about our culture, and what it's like to be on our globally distributed team.
   - [Watch the video](https://www.youtube.com/watch?v=Mkw1-Uc7V1k)
   - Read the [Handbook](https://about.gitlab.com/company/culture/gitlab-101/)
*  **Everyone Can Contribute (Material - Video, 3 Minutes)**
   - Learn more about how we live out our Contribute value! [Watch the video](https://www.youtube.com/watch?v=V2Z1h_2gLNU).
*  **Short Toes (Material - Handbook - 15 minutes)**
   - Learn about how we live the collaboration value at GitLab.
   - Read the [Handbook](/handbook/values/#short-toes)
*  **Org Chart (Material - Handbook - 10 Minutes)**
   - You can see more information about team members and who reports to whom on the team page. Throughout the course you will be asked to schedule a few brief meetings with your peers. Keep in mind that it is always ok to put a meeting on someone's calendar, if they can't make it and decline it is not a problem. We hope you enjoy getting to know your super cool co-workers!
   - Check out the [org chart](https://about.gitlab.com/company/team/org-chart/) and the [Team Page](https://about.gitlab.com/company/team/)

### 2. DevOps Technology Landscape
*  **The Software Development Lifecycle (Material - Video - 9 Minutes)**
   - This video provides a nice overview of the SDLC. For added context, check out the GitLab page that covers each stage in the life cycle.
   - [YouTube - Software Development Lifecycle in 9 Minutes! ](https://www.youtube.com/watch?v=i-QyW8D3ei0)
   - [Dev Ops Lifecycle - Handbook](https://about.gitlab.com/stages-devops-lifecycle/)
   - [YouTube - How is Software Made? ](https://www.youtube.com/watch?v=UuX-GnYWNwo)
*  **GitLab SDLC Quiz (Quiz - 9 Points - Pre Work)**
   - Take a look at the [handbook page on the SDLC](https://about.gitlab.com/stages-devops-lifecycle/) and take this short [quiz](https://forms.gle/Ed9JjNmKsXPhJ4Fr7)
*  **What is Cloud Native (Material - Handbook - 45 minutes)**
   -Taking full advantage of the power of the cloud computing model and container orchestration, cloud native is an innovative way to build and run applications. Cloud native applications are built to run in the cloud, moving the focus away from machines to the actual service.
   - [What is Cloud Native? Video](https://www.youtube.com/watch?v=WsIM034RnAc)
   - [What is Cloud Native? Slides](https://docs.google.com/presentation/d/1mcg5E4dztQZQTN4WvJ4sdqnRGZ6Wz7IRDa8_o8-p4vg/edit?usp=sharing)
   - [What is Cloud Native? Company Website](https://about.gitlab.com/cloud-native/)
*  **What is Complete DevOps? (Material - Video - 25 Minutes )**
   - In this video, we compare the traditional DevOps daisy chain to GitLab's vision for a Complete DevOps solution. Even with the adoption of DevOps, serious challenges continue to exist. Developers and operators used to be separate groups with separate tools. Complete DevOps reimagines the scope of tooling to include both developers and operations teams in one unified solution.
   - [GitLab Blog ](https://about.gitlab.com/blog/)
   - [DevOps Terms ](https://enterprisersproject.com/article/2019/8/devops-terms-10-essential-concepts)
   - Concurrent Dev Ops white paper ([gated link](https://about.gitlab.com/resources/whitepaper-concurrent-devops/)) ([internal link](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/source/resources/downloads/gitlab-concurrent-devops-whitepaper.pdf)
   - [YouTube - Why Concurrent DevOps](https://www.youtube.com/watch?v=bDTYHGEIeM0)
*  **DevSecOps Overview (Material - Sales Collateral - 25 minutes)**
   - How to integrate and automte security in the DevOps Lifecycle
   - A Seismic Shift in Application Security white paper ([gated link](https://about.gitlab.com/resources/whitepaper-seismic-shift-application-security/)) ([internal link](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/source/resources/downloads/gitlab-seismic-shift-in-application-security-whitepaper.pdf))
   - 2019 Global Developer Report: DevSecOps ([gated link](https://about.gitlab.com/developer-survey/)) ([internal link](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/source/resources/downloads/2019-global-developer-report.pdf))
*  **Increase Operational Efficiencies (Material - Video - 5 Minutes)**
   - Compare the traditional DevOps Daisy Chain of disparate tools to an integrated solution for Complete DevOps. GitLab delivers a seamlessly integrated platform for developers and operators to collaborate in real-time and move ideas into production faster.
   - [YouTube - Traditional DevOps Daisy Chain](https://www.youtube.com/watch?v=YHznYB275Mg)
   - Manage Your Toolchain Before It manages You! white paper ([gated link](https://about.gitlab.com/resources/whitepaper-forrester-manage-your-toolchain/)) ([internal link](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/source/resources/downloads/201906-gitlab-forrester-toolchain.pdf))
   - [YouTube - You’re Living Like This? ](https://www.youtube.com/watch?v=w6X4Ha1oC6I)
*  **QUIZ: Increase Operational Efficiencies (Assignment - 25 Minutes - 6 Points - Pre Work)**
   - GitLab is a single application for the entire DevOps lifecycle! Please read the "Manage Your Tool Chain Before it Manages You" white paper from Forrester (see link above) to answer the questions in the [Google Form Quiz.](https://forms.gle/namX7dcg1dbo5KQQ7)

### 3. Our Customers
*  **Customer Segments (Material - Handbook - 15 minutes)**
   - Sales Segmentation is based on Total Employee count of the Global Account. If a Global Account has a lower segment than any of its child accounts, the Global Account will adopt the largest sales segment of any of its child accounts.
   - To learn more read the [Handbook](/handbook/business-ops/resources/#segmentation)
*  **Personas & Pain Points (Material - Video / Handbook - 20 Minutes)**
   - Introducing Personas and Pain Points. David Dulany, Sales Development Consultant, reviews the concepts of Personas and Pain Points and why they are important in setting the context for your messaging.
   - [You Tube - Introducing Personas and Pain Points](https://www.youtube.com/watch?v=-UITZi0mXeU)
   - [Handbook - Roles & Personas](/handbook/marketing/strategic-marketing/roles-personas/)
   - [Buyer Personas](/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/)
*  **VP of App Dev (Material - Video - 10 Minutes)**
   - Watch Product Marketing Manager William Chia talk about the VP of App Dev persona.
   - [YouTube - VP App Dev](https://www.youtube.com/watch?v=58qDalA5o6Q)
*  **DevOps Director (Material - Video - 10 Minutes)**
   - [YouTube - Director of DevOps](https://www.youtube.com/watch?v=5_D4brnjwTg)
   - [DevOps Director Persona Presentation](https://drive.google.com/open?id=1C4wSY2g8vPjNdhOyRFH_bjtDGM0psZJViMfTACgIVCU&authuser=0)
*  **Head of IT (Material - Video - 10 Minutes)**
   - [YouTube - Head of IT](https://www.youtube.com/watch?v=LUh5eevH3F4)
*  **Chief Architect (Material - Video - 10 Minutes)**
   - [YouTube - Chief Architect](https://www.youtube.com/watch?v=qyELotxsQzY)
   - [Chief Architect Persona Presentation](https://drive.google.com/open?id=1HEelJip2Gnu0RDUVqfOwRYrxCtMS3Xf_qJBJ9Zr9ZTg&authuser=0)
*  **QUESTION: Personas & Pain Points (Assignment - 1 Point - Pre Work)**
   - Please share your thoughts on the question below. This is a classroom discussion board, let's crowdsource some knowledge! Why is it important to understand different buyer personas as a salesperson?
*  **Customer Success Stories & Proof Points (Material - Handbook - 10 Minutes)**
   - References are an age old tenet of sales pros! Your prospective clients will definitely be impressed by the positive business outcomes of our customers. Check out our [customer case studies](https://about.gitlab.com/customers/) and [proof points](/handbook/sales/command-of-the-message/proof-points/) on GitLab value.
*  **Marketing Materials Repository (Material - Gitlab.com - 10 Minutes)**
   - Check out the GitLab [marketing materials repository](https://docs.google.com/spreadsheets/d/1NK_0Lr0gA0kstkzHwtWx8m4n-UwOWWpK3Dbn4SjLu8I/edit?usp=sharing) to see all of our best and most relevant sales collateral. Please be careful to only view or download these files, and feel free to send copies to your clients to help them understand GitLab value. If you would like to share one of these assests on social media please be careful to only use the gated links to content.
* **QUIZ: Buyer Personas (Assignment - 5 Points - Pre Work)**
   - Please take a moment to complete this short [quiz](https://docs.google.com/forms/d/e/1FAIpQLSeIDnYn4kWcBZSeRJ98AjUZK7pfj0DqOsnVIcbZjE-atYFExg/viewform) based on information in the [handbook.](/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/)

### 4. Our Portfolio
*  **GitLab Pitch Deck (Material - Video - 15 Minutes)**
   - Check out the latest corporate pitch deck that you can use to speak with your customers about GitLab. Watch the [video](https://youtu.be/UdaOZ9vvgXM) to hear Saumya Upadhyaya - Sr. Product Marketing Manager deliver the GitLab [pitch deck](https://docs.google.com/presentation/d/1SHSmrEs0vE08iqse9ZhEfOQF1UWiAfpWodIE6_fFFLg/edit?usp=sharing)
*  **QUESTION: Pitch Deck Video (Material - Video - 15 Minutes)**
   - Please write one thing you learned or found interesting from watching the GitLab Pitch Deck video.
*  **GitLab Features & Functionality**
   - GitLab is a complete DevOps platform, delivered as a single application, fundamentally changing the way Development, Security, and Ops teams collaborate. GitLab helps teams accelerate software delivery from weeks to minutes, reduce development costs, and reduce the risk of application vulnerabilities while increasing developer productivity. Check out this [reference guide](https://about.gitlab.com/product/) to learn more about what is included in each of the different stages of GitLab.
*  **GitLab Feature Comparison and Tiers (Material - Handbook - 30 Minutes)**
   - Check out the [feature comparison](https://about.gitlab.com/pricing/self-managed/feature-comparison/) chart to learn what's included in each package
   - Review the [GitLab Tiers](/handbook/marketing/strategic-marketing/tiers/) in the Handbook
   - Spend some time checking out the [Why Premium/Silver?](https://about.gitlab.com/pricing/premium/) and [Why Ultimate/Gold?](https://about.gitlab.com/pricing/ultimate/) pages.
* **Ten Reasons Not to Start with Starter**
   - In the [video](https://youtu.be/Np8MyDorw5U) Haydn Mackay (GitLab Sales Director, Americas Enterprise West) highlights the top capabilities not available in GitLab Starter that help organizations deliver better products faster, increase operational efficiencies, and reduce security and compliance risk.
* **Pricing (Material - Handbook - 10 Minutes)**
   - Check out the chart to understand our [pricing model](https://about.gitlab.com/pricing/). For additional context take a look at the [handbook page on pricing](/handbook/ceo/pricing/).
* **Product Tiering Quiz (Assignment - 25 Minutes - 10 Points - Pre Work)**
   - Take a moment to complete this short knowledge check [quiz](https://docs.google.com/forms/d/e/1FAIpQLScahosIQoghrijLvRacseegt65O3SBycTIubvzJsaFn5wp2RQ/viewform)
*  **Use Cases (Material - Handbook - 10 Minutes)**
   - A [customer use case](/handbook/use-cases/) is: A customer problem or initiative that needs a solution and attracts budget. Defined In customer terms: Often aligned to industry analyst market coverage (i.e. Gartner, Forrester, etc. write reports on the topic) These are discrete problems that we believe GitLab solves and are reasons customers choose GitLab (hence which we should seek out in prospects)
   - [Use Case Driven GTM](/handbook/marketing/strategic-marketing/usecase-gtm/)
   - [Version Control and Collaboration Use Case](/handbook/marketing/strategic-marketing/usecase-gtm/version-control-collaboration/)
   - [CI Use Case](/handbook/marketing/strategic-marketing/usecase-gtm/ci/)
   - [DevOps Platform](/handbook/marketing/strategic-marketing/usecase-gtm/devops-platform/)  
* **GitLab Security Solutions (Material - Slides and Video - 30 minutes)**
   - Security is one of the biggest differentiators to Ultimate. In this course, you will learn detailed information about GitLab security capabilities with a click through demo.
   -Using [GitLab for Software Security Video](https://youtu.be/SP0VSH-NqJs)
   -[GitLab Secure Doc](https://docs.gitlab.com/ee/user/application_security/index.html)
   -[GitLab Security and Compliance Capabilities Deck](https://docs.google.com/presentation/d/1WHTyUDOMuSVK9uK7hhSIQ_JbeUbo7k5AW3D6WwBReOg/edit)
*  **Selling Professional Services (Material - Video - 30 Minutes)**
   - Our [Professional Services](/handbook/customer-success/professional-services-engineering/selling/) team is made up of not only GitLab subject matter experts but seasoned DevOps professionals who have experience in deploying and maintaining both large-scale applications as well as creating and teaching best practices throughout the SDLC. Our experts help lead Concurrent DevOps Transformations, providing direct support to our customer’s strategic business initiatives. GitLab's Professional Services team exists to enable your clients realize the full value of their GitLab installation. We can provide direct implementation support to ensure the GitLab installation is resilient and secure. We also offer migration services to facilitate the transition to GitLab by delivering a clean dataset to resume operations at once. Our education and specialized training provide training in best practices, such as CI/CD, version control, metrics, and more.
   - You can also watch the [Sales Enablement Session](/handbook/customer-success/professional-services-engineering/sales-enablement/) about how to sell services

### 5. Field Roles & Expectations
*  **Sales Roles (Material, - Handbook - 30 minutes)**
   - Review the sales roles based on the customer segment you will cover.
   - Read the [Handbook](/handbook/sales/)
   - Read the [Strategic Account Leader (Enterprise) Role Description](https://about.gitlab.com/job-families/sales/strategic-account-leader/)
   - Read the [Account Executive (Mid-Market) Role Description](https://about.gitlab.com/job-families/sales/account-executive/)
   - Read the [Customer Advocate (SMB) Role Description](https://about.gitlab.com/job-families/sales/smb-customer-advocate/)
   - Read the [Sales Development Representative Role Description](https://about.gitlab.com/job-families/marketing/sales-development-representative/)
*  **Solutions Architects (Material - Handbook - 15 minutes)**
   - GitLab's Solutions Architects (SAs) are trusted advisors to GitLab prospects and customers during the presales motion, demonstrating how the GitLab application and GitLab Professional Services address common and unique business requirements.
   - Read the [Solutions Architect Role Description](https://about.gitlab.com/job-families/sales/solutions-architect/)
   - Read about the [Proof of Value Deliverable](/handbook/sales/POV/)
*  **Technical Account Managers (Material, - Handbook - 15 minutes)**
   - GitLab's Technical Account Managers serve as trusted advisors to GitLab's customers. This section outlines to TAM role and their key deliverables: Health Checks, Sucess Plans, and Executive Business Reviews.
   - Read the [TAM Handbook](/handbook/customer-success/tam/#what-is-a-technical-account-manager-tam)
   - Read about [Customer Health Scores in the Handbook](https://about.gitlab.com/handbook/customer-success/tam/health-score-triage/)
   - Read about [Success Plans in the Handbook](/handbook/customer-success/tam/success-plans/)
   - Read about [Executive Business Reviews in the Handbook](/handbook/customer-success/tam/ebr/)
*  **Professional Services Engineers (Material - Handbook - 10 minutes)**
   -Professional Services Engineers (PSEs) provide professional services on-site or remote deployment of GitLab technology and solutions as well as training. The PSE will act as the technical representative leading the direct interaction with the customer’s personnel and project teams by rolling out best practices.
   - Read the [PSE Role job description](https://about.gitlab.com/job-families/sales/professional-services-engineer/)
   - Read the [Handbook](/handbook/customer-success/professional-services-engineering/)
*  **Customer Success Handbook (Material - Handbook - 10 minutes)**
   - Skim and bookmark the [Handbook](/handbook/customer-success/)

### 6. Sales Process
*  **Command of the Message Intro Course (Assignment - 2 Hours - 10 Points - Pre Work)**
   - Please complete the [Command of the Message](https://classroom.google.com/c/MTIxMzIxNzE2NjIw) learning path. Instructions to join the class on your own:
   - Go to classroom.google.com.
   - On the top right of the Classes page, click + then > Join class.
   - Enter the code: **rjs22en** and click Join.
   - Time to complete is approximately 1.5 hours
*  **MEDDPPICC Full E-Learning Course (Assignment - 2 Hours - 10 Points - Pre Work)**
   - Please sign in to [MEDDPPICC](https://classroom.google.com/c/MTIxMzIyMzc3ODA2?cjc=fi3gcwd) learning path and complete the MEDDPPICC full eLearning course. Instructions to join the class on your own:
   - Go to classroom.google.com.
   - On the top right of the Classes page, click + then > Join class.
   - Enter the code: **fi3gcwd** and click Join.
   - Time to complete is approximately 1.5 hours
*  **GitLab Value Framework (Material - Sales Collateral - 35 Minutes)**
   - The GitLab [value framework](/handbook/sales/command-of-the-message/#resources-core-content) is one of the most useful tools available for salespeople. Take a look to understand our value drivers, how to uncover customer needs, and how to articulate value and differentiation. A [framework summary](/handbook/sales/command-of-the-message/#resources-core-content) is also avaliable for quick reference.
* **Seller Deficit Disorder (Material - Sales Collateral - 15 Minutes - Pre Work)**
   - Please read this [short document](https://drive.google.com/file/d/1heCPCI9bT1sc05Xj0hrp8BJrlzMc8VEc/view?usp=sharing) by Force Management to prepare for the Command of the Message training in Sales Quick Start.
*  **Social Selling 101 ( Material - Video - 20 Minutes)**
   - Social selling is the art of using social networks to find, connect with, and nurture your customers and prospects. Watch the [video](https://www.youtube.com/watch?v=w-C4jts-zUw) and use this [guide](https://drive.google.com/open?id=1UCRF6PC6al8XxT8E_4rDKkQjkW6WGPA6gybWeuRIg7A&authuser=0) to learn how to make a profile that will resonate with your prospects.
*  **QUESTION: Business Development (Assignment - 15 Minutes - 10 Points - Pre Work)**
   - Business development is hard... because not everyone gets marketing qualified leads or has a big referral network. Take a look at the [Sales Development](/handbook/marketing/revenue-marketing/sdr/) handbook page to understand more about your XDR partners and their processes. Please share 1-2 insights on strategy or best practices from your experience. How do you consistently keep the pipeline full of leads? Let's crowdsource some best practices!
*  **QUESTION: Account Development (Assignment - 15 Minutes - 10 Points - Pre Work)**
   - One of the most important parts of a salesperson's job is account development. Your clients bought from you once, and they are very likely to do it again! Please share 1-2 insights on strategy or best practices from your experience. How do you keep your clients buying more and sending referrals? Let's crowdsource some experience!

### 7. Sales Action and Role Plays
*  **ACTIVITY: Buyer Brief: Role Play Scenario (Assignment - 30 Minutes - 10 Points - Pre Work)**
   - You've got the money and the power, you're the buyer! Create your own business, or use an account you're currently working on to role play as the buyer with a partner. Once completed, please share the [attached document](https://docs.google.com/document/d/1Zuy4z2YHZR0GXdQB_zexiknDKllgRab59wFWj3kpVnU/edit?usp=sharing) with your role play partner so they can prepare to be the seller in your role play.
*  **ACTIVITY: Role Play Preparation (Assignment - 30 Minutes - 10 Points - Pre Work)**
   - Please fill out the [attached document](https://docs.google.com/document/d/1jwLo3GYA81VNcXg7vHTRF7iMkF7YihV7a362yPtZx0o/edit?usp=sharing) to prepare for your mock call based on the information in the buyer brief provided by your partner. HINT: Use the [Value Framework](https://drive.google.com/open?id=1GV1WGyJIRuor0jxG-9ABu9ZSIBUFtPq1pqAxV9yJOvQ&authuser=0) to help fill in the blanks!
*  **ACTIVITY: First Mock Call (Assignment - 30 Minutes - 10 Points - Pre Work)**
   - Record your mock call with zoom, then upload the recording to Google Drive to submit the assignment.


### 8. Our Competition
*  **The Industry In Which GitLab Competes (Material - Video - 11 Minutes)**
   - Sid Sijbrandij, CEO of GitLab, discusses the overall industry where GitLab competes.
   - [You Tube - The Industry GitLab Competes In](https://www.youtube.com/watch?v=qQ0CL3J08lI)
*  **ACTIVITY: Review & Subscribe: Industry Insights (Assignment - 1 Point - Pre Work)**
   - Review and subscribe to the following blogs recommended by Sid to get the latest industry insights.
   - [https://news.ycombinator.com/](https://news.ycombinator.com/)
   - [https://thenewstack.io/](https://thenewstack.io/)
   - [https://martinfowler.com/](https://martinfowler.com/)
   - [https://about.gitlab.com/blog/](https://about.gitlab.com/blog/)
*  **Competitor Overview (Material - Handbook - 30 Minutes)**
   - There are a lot of [DevOps tools](https://about.gitlab.com/devops-tools/) out there. As a single application for the entire DevOps life cycle, GitLab can remove the pain of having to choose, integrate, learn, and maintain the multitude of tools necessary for a successful DevOps tool chain. However, GitLab does not claim to contain all the functionality of all the tools listed here. Click on a DevOps tool to compare it to GitLab. Last thing dont forget to log into [Crayon](https://app.crayon.co/intel/gitlab/battlecards/), our competitive intelligence platform to view all of our latest competitive advantages.
*  **GitLab vs. GitHub (Material - Video - 3 Minutes)**
   - No... we are not the same company! This short YouTube video on [GitLab vs. GitHub](https://www.youtube.com/watch?v=s8DCpG1PeaU) covers some basic differences.
*  **Cloudbees + Jenkins (Material - Video and Battlecard - 35 minutes)**
   - Watch the [YouTube Video](https://www.youtube.com/watch?v=a95DQqRTOHw&feature=youtu.be)
   - Check out the [Crayon Battlecard](https://app.crayon.co/intel/gitlab/battlecard/4162/)
*  **ACTIVITY: Phone-A-Friend: Competitors (Assignment - 45 Minutes - 10 Points - Pre Work)**
   - Talk with 3 tenured GitLab sales professionals (individual contributors or people managers) and ask them for their tips and tricks on successfully beating the competition. Please take notes from your calls and briefly summarize one or two things you learned from each conversation in the [Google Form](https://docs.google.com/forms/d/e/1FAIpQLSdCwvGRS_-fV9J57KGYIjkXNEUHDigKoPXss-kf9r3Zjozo9w/viewform?usp=sf_link) to submit the assignment.
* **QUIZ: Competitors (Assignment - 5 Points - Pre Work)**
Please take a moment to answer the questions in this short [quiz.](https://docs.google.com/forms/d/e/1FAIpQLSc6-LgwwSBBnsXDn1spi68FCseBY8OjP0rur_DKqa9RmEwQIg/viewform) The information in this quiz is covered in the handbook pages on [Competitive Intelligence](/handbook/marketing/strategic-marketing/competitive/) and [DevOps Tools Landscape.](https://about.gitlab.com/devops-tools/)

### 9. Tools to Get Your Job Done
*  **Using GitLab: The Basics (Material - Handbook - 45 minutes)**
   -This module includes information to help you learn GitLab, and walks you through Projects, Issues, Merge Requests, Pipelines, Settings, User & Admin Area. It includes the Git Basics Training commonly delivered to customers by Professional Services and documentation to help you feel more comfortable with using GitLab.
   -[Learning GitLab with Git Basics](https://gitlab-training.gitlab.io/basics/deck/both.html#/1)
   -[GitLab Basics Guides](https://docs.gitlab.com/ee/gitlab-basics/)
   -[Introduction to GitLab Workflow](https://docs.gitlab.com/ee/topics/gitlab_flow.html)
   -[Description Templates](https://gitlab.com/help/user/project/description_templates)
*  **GitLab Tech Stack Details (Material - Handbook - 10 Minutes)**
   - Take a look to understand all the [tools that GitLab uses](/handbook/business-ops/tech-stack/) to keep the business running smoothly.
*  **ACTIVITY: You've Got Issues! (Assignment - 20 Minutes - 10 Points - Pre Work)**
   - Please find the [Sales Enablement Sandbox](https://gitlab.com/gitlab-com/sales-team/sales-enablement-sandbox) Project, and read the handbook page ["No Tissues with Issues"](/handbook/marketing/strategic-marketing/getting-started/101/). Create an issue with a label, due date, weight, and assign it to yourself. Submit the link to your issue to complete this assignment. Also take a look at the [Markdown syntax guide](https://www.markdownguide.org/basic-syntax/) to learn more about how to code using Markdown. This will be very helpful as you create issues and merge requests.
*  **ACTIVITY: Practice Searching Issues and Filtering Labels (Assignment - 10 minutes - 10 points)**
   - Practice [searching issues and filtering using labels](https://gitlab.com/gitlab-org/gitlab-foss/-/labels)
*  **Update Your LinkedIn Profile! (Assignment - 30 Minutes - 10 Points - Pre Work)**
   - Check out the [slide deck](https://drive.google.com/open?id=1UCRF6PC6al8XxT8E_4rDKkQjkW6WGPA6gybWeuRIg7A&authuser=0) on creating a great profile that will look good to your prospects and clients!
*  **Salesforce - Booking Orders (Material - Handbook - 10 Minutes)**
   - [Learn how to create](/handbook/business-ops/order-processing/#step-7--submitting-an-opportunity-for-approval) Accounts, Contacts, Opportunities, and [Quotes](/handbook/sales/field-operations/sales-operations/deal-desk/#zuora-quote-configuration-guide---standard-quotes) in Salesforce.
*  **Reference Edge (Material - Video - 10 Minutes)**
   - Reference Edge Software will help reps to easily see what available references are the best fit for their current opportunity. Check out the [video](/handbook/sales/training/#functional-skills--processes), and [Powerpoint](https://docs.google.com/presentation/d/18odHX0PTzifRJaeAr-yxa9jdg1_nw4RYDiBqAkYFAyM/edit?usp=sharing) on this new tool.


### 10. Sales and Customer Success Support
*  **The Product Teams (Material - Handbook - 60 minutes)**
   -Review the Product Stages Handbook page to see who you should reach out if you have questions or need assistance with a [particular product.](/handbook/product/categories/)
*  **The GitLab Legal Team (Material - Handbook - 10 Minutes)**
   - Familiarize yourself with submitting Contract Requests for Legal/Contract assistance. [Presentation](https://docs.google.com/presentation/d/1lesWNvPAFd1B3RuCgKsqQlE85ZEwLuE01QpVAKPhQKw/edit#slide=id.g5d6196cc9d_2_0), [Video](https://www.youtube.com/watch?v=CIWdsqRX7E0&amp=&feature=youtu.be) and [Handbook Page](/handbook/business-ops/order-processing/#process-for-agreement-terms-negotiations-when-applicable-and-contacting-legal)
   - You can reach out to the [Legal Team](/handbook/legal/) on the #legal Slack chat channel. The legal Slack chat channel is reserved for everyday legal questions. If you are making a request that requires some sort of deliverable, please do not use the legal Slack chat channel. Slack is reserved for immediate, informal communications. Also, please do not share confidential information on Slack that is not meant for the entire company to see, and do not use it to seek legal advice. You can email the legal team at legal@gitlab.com.
*  **Support for GitLab Team Members (Material - Handbook - 10 Minutes)**
   - [GitLab Support](/handbook/support/internal-support/#what-does-the-support-team-do) provides technical support to our Self-managed and GitLab.com customers for the GitLab product. We are not internal IT Support, so we probably can't help you with your MacBook, 1Password or similar issues.
* **GitLab Alliances (Material - Handbook - 10 Minutes)**
  - Watch the [video](https://www.youtube.com/watch?v=Mi3dtKxypkA&feature=youtu.be), check out the [handbook](/handbook/alliances/) page, and [slides](https://docs.google.com/presentation/d/1iSW8-h6nVaqIdHm5jJZqRrHMB-GCiOe-9ODFo0oke4E/edit?usp=sharing) to understand how we parter with other companies to accelerate GitLab’s trajectory by connecting the great work every team is doing with the outside world.
*  **GitLab Terms & Conditions (Material - Handbook - 10 Minutes)**
   - The following [terms and conditions](https://about.gitlab.com/terms/) govern all use of the GitLab.com website, or any other website owned and operated by GitLab which incorporate these terms and conditions) (the “Website”), including all content, services and support packages provided on via the Website. The Website is offered subject to your acceptance without modification of all of the terms and conditions contained herein and all other operating rules, policies (including, without limitation, procedures that may be published from time to time on this Website by GitLab (collectively, the “Agreement”).
*  **Where to Find Sponsored Marketing Events (Material - Handbook - 10 Minutes)**
   - Take a look at the [marketing issue board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/933457?&label_name[]=West) to see when and where we will be hosting marketing events.


### 11. GitLab Customer Support Team

- **Review the GitLab Customer Support Documentation (Material - Handbook - 5 minutes)**
  - The GitLab Support Team provides technical support to GitLab.com and Self-Managed GitLab customers. The Support Team Handbook is the central repository that explains our work processes and the reasons behind them. To learn more, check out the [Handbook](/handbook/support/).
- **Review the following workflows in the GitLab Customer Support Documentation (Material - Handbook - 15 minutes)**
  - Read the [Working with GitLab Support](https://about.gitlab.com/handbook/support/internal-support/) page as it details how to get in touch with us for common requests requiring Support involvement.
      - The [Support Tickets & Customer Information](https://about.gitlab.com/handbook/support/internal-support/#support-tickets--customer-information) and the [Common Requests](https://about.gitlab.com/handbook/support/internal-support/#common-requests) sections have a list of workflows that you might frequently encounter.
      - Check out this [Slack workflow](https://about.gitlab.com/handbook/support/internal-support/#i-want-to-escalate-a-ticket) if you have to escalate a ticket for a customer to understand the expectations around it.
         - Read the [Trials and Prospect Support](https://about.gitlab.com/handbook/support/internal-support/#trials-and-prospect-support) workflow that should be followed for prospects looking to evaluate Support experience.
      - Make note of Support's [internal-requests](https://gitlab.com/gitlab-com/support/internal-requests) project - you will be filing issues under this for some of the workflows listed in the above page.
- **Review the Zendesk Workflow (Material - Handbook - 15 minutes)**
  - Zendesk is the tool the GitLab Support Team uses to work tickets for customers. Please familiarize yourself with the associated workflows.
  - Review the workflows in this section of the [Handbook](https://about.gitlab.com/handbook/support/workflows/#Zendesk).
- **Sign up for Zendesk (Assignment - 5 Minutes - 10 Points - Pre Work)**
  - All GitLab staff can request a 'Light Agent' account so that you can see customer tickets in Zendesk and leave notes for the Support team. These accounts are free. To request a Light Agent Zendesk account, please send an email to [gitlablightagent.5zjj2@zapiermail.com]. You'll receive an automated reply with the result of your request. NOTE: you must send your request from your GitLab Google / Gmail account. No other addresses will work. The Subject and Body fields of the email can be empty.
      - Check out the additional information in the [handbook](https://about.gitlab.com/handbook/support/internal-support/#viewing-support-tickets), in [Zendesk](https://www.zendesk.com/company/collaboration-add-on-additional-features/), and on [gitlab.com](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request). Once the account is created, you'll need to wait 24 hours for it to be assigned in Okta. Once Zendesk is assigned, you should be able to login to Zendesk at https://gitlab.zendesk.com/agent.

### 12. Technical Deep Dive (SA, TAM, and PSE ONLY)
*  **GitLab Ultimate Demo (Material - Video - 60 minutes)**
   -Review the [GitLab Ultimate Demo](https://chorus.ai/meeting/1837148?tab=summary&call=5E63AE4EDCAD4960A59BEBBEF4CEE1BF)
* **Auto DevOps (Material - GitLab Docs - 15 minutes)**
   -Auto DevOps provides pre-defined CI/CD configuration which allows you to automatically detect, build, test, deploy, and monitor your applications. Leveraging CI/CD best practices and tools, Auto DevOps aims to simplify the setup and execution of a mature & modern software development lifecycle.
   -Review the [GitLab Doc on Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/)
*  **GitLab API (Material - GitLab Doc - 15 minutes)**
   -Automate GitLab via a simple and powerful API.
   -Review the [GitLab Doc on API](https://docs.gitlab.com/ee/api/README.html)
*  **GitLab for Agile (Material - 30 minutes)**
   -Agile development is iterative, incremental, and lean approaches to streamline and accelerate the delivery of projects. Ever wondered if GitLab supports Agile methodology? If you're considering using GitLab it might not be obvious how its features correspond with Agile artifacts, so we've broken it down for you in a blog and the GitLab Agile Planning page.
   -Review the [Agile Delivery page on the company website](https://about.gitlab.com/solutions/agile-delivery/)
   -Review the [Agile Blog](https://about.gitlab.com/blog/2018/03/05/gitlab-for-agile-software-development/)
*  **GitLab Runners (Material - GitLab Docs, Slides, and Video - 60 minutes)**
   -GitLab Runner is the open source project that is used to run your jobs and send the results back to GitLab. It is used in conjunction with GitLab CI, the open-source continuous integration service included with GitLab that coordinates the jobs.  Below are slides, the video presentation, and additional information about GitLab Runners.
   -[GitLab Runner Doc](https://docs.gitlab.com/runner/)
   -[GitLab Runner Deep Dive Slides](https://docs.google.com/presentation/d/1OhQllwfVNSbWymjf5MCxgWG86FnxjvqLUjL-8sQgZTg/edit#slide=id.g2823c3f9ca_0_9)
   -[GitLab Runner Deep Dive Reording](https://drive.google.com/file/d/1QdgNLxepxiNwUwykhOX6DBmUMp067ooQ/view)
*  **GitLab Reference Architectures and GitLab GEO (Material - 20 minutes)**
   -Review the GitLab Reference Architectures and Geo Replication Overview, then hear Brian Wald, Solution Architect Manager, break it all down.  
   -[GitLab Reference Architectures](https://docs.gitlab.com/ee/administration/reference_architectures/)
   -[GitLab High Availability and Geo Options Video](https://youtu.be/fji7nvmOHNQ)
*  **ACTIVITY: Let's Install GitLab! (Material - Handbook - 60 minutes)**
   -Choose one of the installation options mentioned in the link below. Once installed populate with some test data. Be sure to include: Populate with some test data: User account, Project, Issue.
   -Review the [About GitLab Installation Page](https://about.gitlab.com/install/)
*  **Kubernetes 101 (Material - 13 hours)**
   -It is essential that you understand the Kubernetes (k8s) fundamentals. Based on your existing Kubernetes experience you should either take the first course "Kubernetes for the Absolute Beginner" (6 hours) or "Learn DevOps: the Complete Kubernetes Course" (13 hours).
*  **Resources (Optional Material)
   -Bookmark these [AWS and CI references](https://docs.google.com/document/d/1bB7vlefsD_jwVgRxwaGuc8-AmR5sMyGSssS_alkNxuc/edit)
   -Bookmark these [Marketing Demos](/handbook/marketing/strategic-marketing/demo/)


### 13. Integrations (SA, TAM, and PSE Only)
* **Category Overview**
   - GitLab's vision is to be the best single application for every part of the DevOps toolchain. However, some customers use tools other than our built-in features–and we respect those decisions. The Integrations category was created specifically to better serve those customers. Currently, GitLab offers [30+ project services](https://docs.gitlab.com/ee/user/project/integrations/project_services.html#project-services) that integrate with a variety of external systems. Integrations are a high priority for GitLab, and the Integrations category was established to develop and maintain these integrations with key 3rd party systems and services.
* **Jira**
   - GitLab Issues are a powerful tool for discussing ideas and planning and tracking work. However, many organizations have been using Jira for these purposes and have extensive data and business processes built into it. While you can always migrate content and process from Jira to GitLab Issues, you can also opt to continue using Jira and use it together with GitLab through our [integration.](https://docs.gitlab.com/ee/user/project/integrations/jira.html) For a video demonstration of integration with Jira, watch [GitLab workflow with Jira issues and Jenkins pipelines.](https://youtu.be/Jn-_fyra7xQ)
* **Jenkins**
   - GitLab’s [Jenkins integration](https://docs.gitlab.com/ee/integration/jenkins.html) allows you to trigger a Jenkins build when you push code to a repository, or when a merge request is created. Additionally, it shows the pipeline status on merge request widgets and on the project’s home page. Videos are also available on [GitLab workflow with Jira issues and Jenkins pipelines](https://youtu.be/Jn-_fyra7xQ) and [Migrating from Jenkins to GitLab.](https://youtu.be/RlEVGOpYF5Y)
* **Github**
   - GitLab provides an integration for updating the pipeline statuses on GitHub. This is especially useful if using GitLab for CI/CD only. This project integration is separate from the [instance wide GitHub integration](https://docs.gitlab.com/ee/user/project/import/github.html#mirroring-and-pipeline-status-sharing) and is automatically configured on [GitHub import.](https://docs.gitlab.com/ee/integration/github.html)
*  **GitLab as OAuth 2 Authentication Service Provider (Material - GitLab Docs - 60 minutes)**
   -OAuth provides to client applications a ‘secure delegated access’ to server resources on behalf of a resource owner. In fact, OAuth allows an authorization server to issue access tokens to third-party clients with the approval of the resource owner, or the end-user.
   -Review this page to learn how to use [GitLab as an OAuth authentication service provider](https://docs.gitlab.com/ee/integration/oauth_provider.html)

### 14. SAs Only: Set Up Your Demo Environment
*  **ACTIVITY: Obtain Gold Level Access to Enable All Features on GitLabg.com (Assignment - 15 minutes - 10 points)**
   -An [example request](https://gitlab.com/gitlab-com/support/internal-requests/issues/310)
*  **ACTIVITY: Create a demo account on GitLab.demo.i20.online (Assignment - 10 points)**
   -Once you've created a login, contact your manager to be promoted to an Administrator. This will give you access to the Admin area of self-hosted installations, which is unavailable on GitLab.com.
*  **ACTIVITY: Set Up Standby Demo Environment (Assignment - 30 minutes - 10 points)**
   -Set up a standby demo environment as described in the Handbook. Request project access from project owners as needed. Make sure to add your demo project to the group you created above so all features are available.
   -Review the [Handbook](/handbook/customer-success/solutions-architects/#demo-readiness)
*  **ACTIVITY: Obtain a Kubernetes Cluster (Assignment - 10 minutes - 10 points)**
   -Obtain a Kubernetes cluster for your demo deployments. Contact Joel Krooswyk (mention @jkrooswyk in this issue) to request a GKE cluster be created for your personal use in demos. Cluster allocation and usage instructions can be found in the below Google sheet. Feel free to contact other SA's or your manager if you need help connecting your GitLab demo project to your cluster.
   -[Request access here](https://docs.google.com/spreadsheets/d/1UmAutBx76lb3gMZGJqtcgg9vowvk5d2KUUocPqQ__Xw/edit#gid=0)
*  **ACTIVITY: Deliver Your Demo (Assignment - 60 minutes - 10 points)**
   -Deliver a short demo of your choice in the SA Bootcamp Demo Scenarios below. Access to Jenkins and Jira and the associated GitLab.com project are available in the [SA Vault within 1Password](https://docs.google.com/document/d/1tIaZUU5YYyQtvQwZ2EOpeCoIK2TGDHbVfJv7jbAI0kk/edit)
   -Be sure to invite your manager and onboarding buddy.
   -Upload the below recorded scenarios to the below [Google Drive folder](https://drive.google.com/drive/folders/1EIN5iZnTWVUvWOsdCNkINIWOBfPGe4Bz)

### 15. TAMs Only: Customer Onboarding
*  **Customer Onboarding (Material - Handbook and Video - 60 Minutes)**
   - Customer on-boarding is a 45 day time period. Technical Account Managers and Professional Services Engineers should work closely together throughout the on-boarding process, with support from Solutions Architects and Strategic Account Leaders/Account Managers where appropriate. The Customer Kickoff is a moment that matters in the customer's journey. Below are resources you should familiarize yourself with as you get ready to start interacting with customers.
   - [Customer Onboarding and Gemstone Categorization](/handbook/customer-success/tam/gemstones/)
   - [Using SalesForce within Customer Success](/handbook/customer-success/using-salesforce-within-customer-success/)
   - [Customer Onboarding Dashboard](https://gitlab.my.salesforce.com/01Z4M000000skrQ)
   - [Customer Onboarding Custom Object Training Video](https://drive.google.com/file/d/1ZGezQKYmn0IyAIbh2ytz_GcCW2n431be/view)
   - [SalesForce POV Slides](https://docs.google.com/presentation/d/1SBQ_FbCsPfycTxdtoracQTAh7WOpGPPAqptXfRiZdvc/edit#slide=id.g29a70c6c35_0_68)
*  **ACTIVITY: Record a Customer Kickoff Call (Assignment - 60 minutes - 10 points)**
   -Using Zoom, record a short mock customer kickoff call. Be sure to invite your manager and your onboarding buddy.
   -The [Rubric for the call](https://docs.google.com/forms/d/e/1FAIpQLSeZgqf6cU0rR0wvoOneGGh0jNaC0PXCzN5TEf_IBbBn80VxfQ/viewform)

### 16. PSEs Only: Statement of Work
*  **The Statement of Work (SOW) Process (Material - Handbook - 20 minutes)**
   - Review the SOW Creation Process, and review the below reference deck for details on creating a Professional Services SOW.
   - [Workflow for the SOW](/handbook/customer-success/professional-services-engineering/selling/)
   - [SOW Creation Instructions](https://docs.google.com/presentation/d/1ro9wlLHsoOMC-iYJpxy_RTCD4PfjDFZEdbuMyDD6WOk/edit#slide=id.g2823c3f9ca_0_9)
*  **ACTIVITY: Create a Mock SOW (Assignment - 60 minutes - 10 points)**
   -Create a mock SOW using the calculator below and the automatic SOW creator on that page. This is for 1000 users. The prospect wants an HA deployment on AWS, admin training for 20 people, CI/CD training for 60 people and a migration of their existing data from their existing SVN system (assume this costs $15K). Send the link of the Google doc you create to your manager when it's complete.
   -[GitLab Services Calculator](https://services-calculator.gitlab.io/) Please note the SOW may have errors in it as the calculator is still WIP
   - Many example SOW's can be found in the [Google Drive Folder](https://drive.google.com/drive/folders/1J1HqK6lh36UYLnyRcpe3u-Hpl5xSy0Vp)

### In-Class Assignments
* **Essential Questions Exercise (Assignment - 30 Minutes - 10 Points - In-Class)**
   - Please work in small groups to answer the questions on the [attached document.](https://docs.google.com/document/d/1XvFKnDdueKeUhdk-x9vp9_ASS1rHIPmFhN63z0wlZcs/edit?usp=sharing)
* **Value Card Exercise - Group Activity (Assignment - 30 Minutes - 10 Points - In-Class)**
   - Please work in small groups to create a value card for your assigned differentiator with the [attached document.](https://docs.google.com/document/d/1XvFKnDdueKeUhdk-x9vp9_ASS1rHIPmFhN63z0wlZcs/edit?usp=sharing).
* **Discovery Questions Exercise (Assignment - 30 Minutes - 10 Points - In-Class)**
   - Please work in small groups to fill out the [attached document.](https://docs.google.com/document/d/1bKe4AGwg1gGhvUqovuQdm3b7y5bQb6EOvu5BxC4ZUSw/edit?usp=sharing) Think of a real opportunity that you or someone in your group is currently working and write some good discovery questions & expected responses.
* **Defensible Differentiator Exercise (Assignment - 30 Minutes - 10 Points - In-Class)**
   - Please work in small groups to fill out the [attached assignment](https://docs.google.com/document/d/1V2yMIaHvp_s1RzSlsAR2_AsExSloyLKbVuSlqVkbufg/edit?usp=sharing) on defensible differentiators.
*  **Searching GitLab Like a Pro (Assignment - 10 Minutes - 10 Points - In-Class)**
   - At GitLab, we're prolific at documenting what we do in the handbook, the website, and in GitLab documentation. This may make it difficult to find specific pieces of content. Google already indexes all our public facing pages and there is a search modifier google offers that will help. Take a look at the handbook page on how to [set up your search engine like the pros!](/handbook/tools-and-tips/searching/)
*  **Role Play Notes (Assignment - 10 Minutes - 10 Points - In-Class)**
   - Please submit your notes from one of the role plays that you observed using the [notes template](https://docs.google.com/document/d/1bzuO-ngACn4Z1S55Z7EtNOb2HYCZbcBZZRYIVpkeMtw/edit?usp=sharing). Dont forget to check out the [handbook page](/handbook/sales/#sales-note-taking) on how we take notes at GitLab.
*  **Group Activity: Positioning Value, Resolving Objections & Closing (Assignment - 30 Minutes - 10 Points - In-Class)**
   - Break into groups of 3-5 people that are different than your mock discovery call groups. Choose one person to fill out the [google form](https://docs.google.com/forms/d/e/1FAIpQLSfwZiy__U1cl0kB3vOKl7GiQmcDgnBptT8G4qL_ZSw0gdC_hA/viewform?usp=sf_link), and please answer the following questions as a team. Be prepared to share your answers with the class.
*  **Call Planning & Preparation - Group Activity (Assignment - 30 Minutes - 10 Points - In-Class)**
   - Break into groups of 3-5 people that are different than your mock discovery call groups. Choose one person to fill out the [google form](https://docs.google.com/forms/d/e/1FAIpQLSfwZiy__U1cl0kB3vOKl7GiQmcDgnBptT8G4qL_ZSw0gdC_hA/viewform?usp=sf_link), and please answer the following questions as a team. Be prepared to share your answers with the class.
*  **Territory Planning - Group Activity (Assignment - 30 Minutes - 10 Points - In-Class)**
   - Break into groups of 3-5 people that are different than your mock discovery call groups. Choose one person to fill out the [google form](https://forms.gle/EkJ6cfZZMqvPYGFTA), and please answer the following questions as a team. Be prepared to share your answers with the class
*  **Account Planning - Group Activity (Assignment - 30 Minutes - 10 Points - In-Class)**
   - Break into groups of 3-5 people that are different than your mock discovery call groups. Choose one person to fill out the [google form](https://forms.gle/3XJBawZ48jc3sFdr7), and please answer the following questions as a team. Be prepared to share your answers with the class

### Post-Class Assignments
*  **Outreach (Assignment - Classroom - 1 Hour - 10 Points - Post Work)**
   - Please complete the Outreach learning path. Time to complete is approximately 1 Hour.
   - Instructions to join the class on your own:
   - Go to [classroom.google.com](classroom.google.com).
   - On the top right of the Classes page, click + then > Join class.
   - Enter the code **rfo76ti** and click Join.
*  **Chorus.ai (Assignment - Classroom - 30 Minutes - 10 Points - Post Work)**
   - Please complete the Chorus.ai learning path. Chorus is a powerful conversation analytics platform that will help you expand your knowledge, ramp up faster, and adopt best practices! Time to complete is approximately 30 minutes.
   - Instructions to join the class on your own:
   - Go to [classroom.google.com](classroom.google.com).
   - On the top right of the Classes page, click + then > Join class.
   - Enter the code **3pyea4i** and click Join.
*  **Listen to Chorus Calls (Assignement - 10 points - Post Work)**
   - Using [Chorus](https://www.chorus.ai/) listen to 3 calls that were with another team member in your same role.
*  **Clari for Salespeople (Assignment - Classroom - 1 Hour - 10 Points - Post Work)**
   - The Clari assignment is only for Strategic Account Leaders, Mid-Market Account Executives, and SMB Customer Advocates. SAs, TAMs, SDRs, and ISRs may disregard and do not need to complete the course. Clari is a sales forecasting technology that helps salespeople drive more pipeline, accelerate revenue, and forecast accurately. Time to complete is approximately 1 hour.
   - Instructions to join the class on your own:
   - Go to [classroom.google.com](classroom.google.com).
   - On the top right of the Classes page, click + then > Join class.
   - Enter the code **3dfmc4w** and click Join.
*  **MEDDPPICC Opportunity Qualification (Assignment - Classroom - 1 Hour - 10 Points - Post Work)**
   - Use this [template](https://docs.google.com/document/d/17Rr03SHKQ2XXlOnvuhz68F8pTxNhr6nOK70TBwB63oc/edit?usp=sharing) to begin qualifying an account suggested by your manager.
*  **Create a Light Territory Plan (Assignment - 1 Hour - 10 Points)**
   - Create a light territory plan to review with your manager.
*  **Custom Pitch Deck (Assignment - 1 Hour - 10 Points)**
   - Create a custom Pitch Deck for an account that you're currently prospecting.
*  **Custom Pitch Deck Recording (Assignment - 1 Hour - 10 Points)**
   - Record yourself delivering your custom Pitch Deck with zoom to practice.
*  **Live Lead (Assignment - 1 Hour - 10 Points)**
   -Please submit the link to your live lead using Chorus. If your lead was not recorded with Chorus, please upload the Zoom recording to Drive and submit the Drive file.


### 17. Course Evaluation & Feedback
*  Every participant is asked to please let us know how we can do better by taking [this brief survey](https://docs.google.com/forms/d/e/1FAIpQLSch3PLSzmoPUCSyhHUVwUfFSSPwiVGl2lAhRIc_bCmqZ6us6g/viewform?usp=sf_link)
