---
layout: handbook-page-toc
title: Support Team Handbook
---

## Welcome to the GitLab Support Team Handbook
{: .no_toc}

The GitLab Support Team provides technical support to GitLab.com and Self-Managed GitLab customers. The GitLab Support Team Handbook is the central repository for why and how we work the way we do.

| Your Role | Your Need | Where You Should Look |
| --------- | --------- | --------------------- |
| A customer, or an advocate for a customer | Technical assistance | Public [Support Page](/support), which describes the best way to get the help you need and lists GitLab's paid service offerings |
| GitLab team member | Technical assistance | [Internal Support for GitLab Team Members page](internal-support) |
| New Support Team member | Onboarding / Learning | [Support Engineer Responsibilities](/handbook/support/support-engineer-responsibilities.html) page and [Support Learning Pathways](/handbook/support/training/) |

Know someone who might be a great fit for our team? Please refer them to the job-family descriptions below.

- [Support Engineering Job Family](https://about.gitlab.com/job-families/engineering/support-engineer/overview/)
- [Support Management Job Family](https://about.gitlab.com/job-families/engineering/support-management/)
- [Support Operations Job Family](https://about.gitlab.com/job-families/engineering/support-operations-specialist/)

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What does the Support Team do?

### We care for our customers

- Always assume you are the person responsible for ensuring success for the customer.
- When supporting a customer, any issue, incident or loss is _GitLab's loss_.
   - When a customer experiences trouble or downtime, take action with the same urgency you'd have if GitLab were experiencing downtime.
   - When a customer is losing productivity, take action with the same urgency you'd have if GitLab were losing productivity.
   - The rule of thumb is a customer down with 2,500 users gets the same urgency as if GitLab were losing $1,000,000 per day. This treatment is equal regardless of how much they are paying us.
- Escalate early.  Visibility across GitLab, up to and including the CEO, is always better earlier rather than later.  Ensure all resources needed are on the case for customers early.

Remember, as members of the support team we are the first to interact with someone when they have a problem or question. As such it is up to us to represent the company and make sure we present ourselves properly. Therefore we are expected to:

  - Always be friendly and respectful.
  - Be open to new ideas and points of view.
  - Be OK if you don't know something. You can always ask someone else.
  - Be comfortable saying no to a customer (but try to suggest a workaround and escalate to a Senior if necessary).

### Our role within GitLab

GitLab Support is part of the [Engineering division](/handbook/engineering/).
While most engineering departments are part of the R&D [cost center](/handbook/finance/financial-planning-and-analysis/#cost--reporting-structure),
Support is part of the Cost of Sales (or sometimes Cost of Goods Sold (COGS)) cost center.

This unique arrangement is expressed in our [Key Performance Indicators](/handbook/support/performance-indicators/),
which are largely focused around pursuing customer success and satisfaction
while driving efficiency by increasing output while keeping costs within a
predefined range.

This is also why [Support Engineer responsibilities](/handbook/support/support-engineer-responsibilities.html)
include contributing code and documentation and working alongside Product
Managers on our products: By using the knowledge gained from interacting with
our customers to make our products or docs better, we solve problems before
they become one. This reduces support case load while increasing efficiency for
the wider GitLab organization. For example, the Sales department can rely on
our docs to answer customer queries instead of leaning on Support or Customer
Success for help, freeing up more time to close sales.

### How we measure our performance

We use [Key Performance Indicators](/handbook/support/performance-indicators/) (KPIs) to keep track of how well each Engineering Department is doing, including the Support team, as a whole.

The KPI measurements can be found under the `Reporting` tab in Zendesk if you have the appropriate access, but progress on meeting these KPIs is also tracked via the aforementioned KPI link and visually through reports in the [Support KPIs Dashboard](https://app.periscopedata.com/app/gitlab/421422/).

We review these KPIs weekly in the [Support Week-in-Review](/handbook/support/#support-week-in-review).

### About the Support Team

- [Support Engineer responsibilities](/handbook/support/support-engineer-responsibilities.html)
- [Support Engineer knowledge areas](/handbook/support/workflows/knowledge_areas.html)
- [Support Managers](/handbook/support/managers/index.html), including areas of focus.

----

## Direction

The overall direction for Support in 2019 is set by our overall [strategic objectives](/strategy), with a particular emphasis on
- continued improvement of (Premium) Customer satisfaction.
- scaling the team through hiring and process improvements to pace ticket growth
- refining process, communication and team-training to improve efficiency as we scale

As can also be inferred from our [publicly visible OKR page](/company/okrs/), we anticipate 2019 will focus on the following elements:

### Ticket deflection through documentation

1. By building up a corpus of documentation informed by real-world problems we can ensure that customers can get the answers they need before they come into the queues.

   - *When learning, use the docs.*
   - *When troubleshooting, use the docs.*
   - *If something is missing, update the docs.*

1. By developing a [docs-first](https://docs.gitlab.com/ee/development/documentation/styleguide.html#docs-first-methodology) approach to answering, we can ensure that the documentation remains a highly useful [single source of truth](https://docs.gitlab.com/ee/development/documentation/styleguide.html#documentation-is-the-single-source-of-truth-ssot),
and that customers are more aware of where to find content on their own.

   - *Always respond with a link to the docs.*
   - *If docs content is missing, create it and link the customer to the MR.*

Beyond this work during specific support cases, the team is also asked to contribute their
expertise to GitLab's documentation by producing new guides and videos that would
be useful to our users — especially on emerging DevOps trends and use cases.

### Increase capacity & develop experts

In the past few years we expanded the support team significantly, and this trend will continue. As GitLab -- the product -- continues to expand, so will the skill and knowledge of all Support Engineers to be able to continue providing an excellent customer support experience.

1. Develop "Deep Dives", learning resources and other professional training programs to decrease the time it takes for a new team member to be effective.
1. Make sure that professional training sessions result in the production of publicly consumable resources:
docs fixes, tutorials and improvements to any related material.

### Experiment with support model

In late 2018 we moved from a "product-first" model to a "region-first" model. While this has affected the reporting structure, the lines
of work from the perspective of an individual Support team member continue to be aligned to product. In 2019 we want to experiment with a "concept-first"
model of support that will align individual strengths with customers needs regardless of which queue they come into.

Other areas we'd like to explore:
1. 24x7 capability beyond uptime support (i.e. weekend staffing)
1. Language support
1. US-only Federal Support

----

## Hazards & Challenges

### Team morale suffers

| Hazard                     | Commitment                              |
|----------------------------|-----------------------------------------|
| Ticket volume is too high | Build (and adjust) hiring model based on the best data available, hire as quickly as possible while keeping quality up. |
| Team knowledge doesn't match ticket difficulty | Develop training materials, provide time for professional development, encourage specializations.  |
| We aren't hitting our SLOs | Hire and train as per above. Add regularity to scheduling and encourage efficient work patterns.
| Leadership breaks trust    | Communicate in public channels, alert early and often of potential discussions, engender the GitLab value of Transparency. |
| Fear of conflict results in poor decisions | Provide focus on meta-issues by triaging issues in the [Active Now](#improving-our-processes---active-now-issue-board) board. Encourage buy-in and bravery. Truly listen and respond. Explicitly overrule: state reasoning and thank everyone for voicing opinions. |
| Team lacks commitment to results or implementing decisions | Ensure voices are heard. Re-enforce "disagree and commit". Build accountability. |
| There's no accountability in poor results or not meeting commitments | Reinforce GitLab value of Results by paying attention and following up. |
| Lack of trust as the team grows | Make an intentional effort to frequently do pairing sessions between regions.|

### Scaling

As we continue to increase in size, there's a number of challenges that we need to address and/or questions we need to answer:

As we grow:
1. What process inefficiencies do we need to address? (e.g. _Do all engineers need to know about every component/configuration/deployment strategy of GitLab, or are there ways we can specialize?_)
1. Which process inefficiencies do we need to intentionally keep because of a positive externality? (e.g. _Do we keep around a synchronous meeting because it helps build relationships?_)
1. What have we lost in scaling? How can we build it back in a way that scales? (e.g. _Smaller groups lead to more ideation sessions, how can we make sure we're creating spaces for ideas to build off of each other?_)
1. How can we make sure we're broadcasting things appropriately so no one misses out on an important piece of information?

----

## Communications

The GitLab Support Team is part of the wider Engineering function. Be sure to check the
[communications section in the Engineering handbook](/handbook/engineering/#communication)
for tips on how to keep yourself informed about engineering announcements and initiatives.

Here are our most important modes of communication:

  * [Support Week in Review](#support-week-in-review). Important updates for everyone in support.
    You should try to check the SWIR at least once a week. If you have something to share with the
    entire team this is the best place to do it. For example, if you have an issue for a common bug, an issue that requires feedback,
    or an issue about an external project you're working.
  * [Slack channels](#slack) for ["informal"](/handbook/communication/#slack) communication.
    Due to our data retention policy in Slack, things shared there will eventually be deleted. If you want to share something there, please make sure it also has a more permanent place in our docs, handbook, issue tracker, etc.
  * [Meta issue tracker](https://gitlab.com/gitlab-com/support/support-team-meta/issues/) for any issues regarding workflows,
    general team suggestions, tasks or projects related to support, etc.

Where we want to ensure that important messages are passed to the global support team, we will use this [messaging template](https://gitlab.com/gitlab-com/support/managers/leadership-sync/-/blob/master/.gitlab/issue_templates/support-message-plan.md). This ensures that these messages are delivered across our communications channels in a structured and documented manner.

### GitLab.com

#### Groups
{: .no_toc}

We use the following groups to notify or add support team members to issues and merge requests on
GitLab.com.

| Group                                                                          | Who                      |
|--------------------------------------------------------------------------------|--------------------------|
| [@gitlab-com/support](https://gitlab.com/gitlab-com/support)                   | All Support Team members |
| [@gitlab-com/support/amer](https://gitlab.com/gitlab-com/support/amer)         | AMER Support             |
| [@gitlab-com/support/apac](https://gitlab.com/gitlab-com/support/apac)         | APAC Support             |
| [@gitlab-com/support/emea](https://gitlab.com/gitlab-com/support/emea)         | EMEA Support             |
| [@gitlab-com/support/dotcom](https://gitlab.com/gitlab-com/support/dotcom)     | Support members with GitLab.com admin access |
| [@gitlab-com/support/dotcom/console](https://gitlab.com/gitlab-com/support/dotcom/console) | Support members with GitLab.com console access |
| [@gitlab-com/support/customers-console](https://gitlab.com/gitlab-com/support/customers-console) | Support members with CustomersDot console access |
| [@gitlab-com/support/licensing-subscription](https://gitlab.com/gitlab-com/support/licensing-subscription) | Support members focused on License and Renewals |
| [@gitlab-com/support/managers](https://gitlab.com/gitlab-com/support/managers) | All support managers     |

#### Projects
{: .no_toc}

Our team projects and issue trackers can be found in the
[Support parent group](https://gitlab.com/gitlab-com/support). Here are some selected projects
which are relevant to team communications.

| Project | Purpose |
|---------|---------|
| [support-team-meta](https://gitlab.com/gitlab-com/support/support-team-meta) | Issues to discuss and improve Support processes |
| [support-training](https://gitlab.com/gitlab-com/support/support-training) | Courses and training for Support team including onboarding |
| [support-pairing](https://gitlab.com/gitlab-com/support/support-pairing) | Record of pairing sessions where we work together on tickets |
| [feedback](https://gitlab.com/gitlab-com/support/feedback) | Collects SSAT survey responses from Zendesk in the form of issues |
| [support-operations](https://gitlab.com/gitlab-com/support/support-ops/support-ops-project) | Support Operations team project |


##### Support team meta issue tracker

We use the [Support meta issue tracker](https://gitlab.com/gitlab-com/support/support-team-meta/issues) for tracking issues
and creating issues that may require feedback around support.

If you're interested in working on a project or task related to
support feel free to create an issue and link to any external issues or projects so that we can:

* Be transparent to the entire team what we're working on
* Have the opportunity to collaborate on external projects or tasks with other team members who are interested
* Avoid having team members do duplicate work

Issues regarding documentation or features for GitLab, our FOSS project or any of the GitLab
components should not go in this issue tracker, but in their appropriate issue tracker.

If you have a proposed solution that is actionable, it's best to [start a merge request](/handbook/communication/#everything-starts-with-a-merge-request), tag the team for feedback and link in the [Support Week in Review](#support-week-in-review).

### Slack

We follow GitLab's [general guidelines for using Slack](/handbook/communication/#slack)
for team communications. As only 90 days of activity will be retained, make sure
to move important information into the team handbook, product documentation,
issue trackers or customer tickets.

#### Channels
{: .no_toc}

| Channel                                                                | Purpose                                                   |
|------------------------------------------------------------------------|-----------------------------------------------------------|
| [#support_team-chat](https://gitlab.slack.com/archives/CCBJYEWAW)      | Support team lounge for banter, chat and status updates   |
| [#support_gitlab-com](https://gitlab.slack.com/archives/C4XFU81LG)     | Discuss GitLab.com tickets and customer issues            |
| [#support_self-managed](https://gitlab.slack.com/archives/C4Y5DRKLK)   | Discuss self-managed tickets and customer issues          |
| [#support_managers](https://gitlab.slack.com/archives/CBVAE1L48)       | Discuss matters which require support managers' attention |
| [#spt_hiring](https://gitlab.slack.com/archives/CE9S6JW4S)             | Discuss support team hiring-related matters               |

#### User Groups
{: .no_toc}

| Group                    | Who                       |
|--------------------------|---------------------------|
| `@support-dotcom`        | Support Team Members with GitLab.com Admin Access  |
| `@support-selfmanaged`   | Support Team focused on Self-Managed tickets |
| `@support-team-apac`     | Support Team APAC         |
| `@support-team-emea`     | Support Team EMEA         |
| `@support-team-americas` | Support Team AMER         |
| `@supportmanagers`       | Support Managers          |
| `@support-managers-apac` | Support Managers APAC     |
| `@support-managers-emea` | Support Managers EMEA     |

If you need to be added to one or more of these groups, please open an issue in
the [access requests project](https://gitlab.com/gitlab-com/access-requests).


### Google Calendar

We use the following team calendars to coordinate events and meetings:

- [GitLab Support](https://calendar.google.com/calendar/embed?src=gitlab.com_9bs159ehrc5tqglur88djbd51k%40group.calendar.google.com) Calendar ID `gitlab.com_9bs159ehrc5tqglur88djbd51k@group.calendar.google.com`
- [Support - Time off](https://calendar.google.com/calendar/embed?src=gitlab.com_as6a088eo3mrvbo57n5kddmgdg%40group.calendar.google.com) Calendar ID `gitlab.com_as6a088eo3mrvbo57n5kddmgdg@group.calendar.google.com`

Add these calendars to your GitLab Google calendar by clicking on the "+" sign next to "other calendars", and choose "subscribe to calendar". Enter the relevant ID mentioned above. If you need access to these calendars, ask a support team member for help.


### Weekly Meetings

The Support Team has several meetings each week. These allow us to coordinate and
help us all grow together. Each meeting has its own agenda and is led by a different member of the team each week.

Discussions are encouraged to be kept in issues or merge requests so the entire team can collaborate, regardless of time zone.

Any demos or announcements that need to be shared with the entire team should be shared in the [Support Week in Review](#support-week-in-review).

|  Weekday  |   Region  |      Meeting Name     |                                        Purpose                                      |
|:---------:|:---------:|:---------------------:|:-----------------------------------------------------------------------------------:|
|  Tuesday  |    APAC   |      Support Call     |              Discuss metrics, demos, upcoming events, and ask questions             |
|  Tuesday  |    AMER   |  Casual Catch up |         Agendaless, a space for the team to banter and chat. If you have work to talk about, feel free.        |
| Wednesday | AMER,EMEA,APAC | Dotcom Support Sync | For anyone who is interested in (or has a point of interest for) those who work with GitLab.com customers and processes - rotates through regions |
|  Thursday  |    EMEA   |      Support Call     |              Discuss metrics, demos, upcoming events, and ask questions             |

The regions listed above are the regions for which each call may be the most convenient, but all are welcome on any call. Every call is recorded and notes are taken on the agenda for each one. If you miss a meeting or otherwise can't make it, you can always get caught up.

All Zoom and agenda links can be found on the relevant calendar entry in the Support Calendar.

#### Support Leadership Meetings

The Support management team meet regularly. Details of these calls are on the [Support Managers page](/handbook/support/managers)

#### Senior Support Engineer Office Hours

Senior and Staff Support Engineers are encouraged to host office hours. These office hours are intended to strengthen
the team through mentoring. It is up to each Senior/Staff Support Engineer whether they schedule office hours, and how
often. Please see the "GitLab Support" Team calendar to view office hours and invite yourself.

We encourage hosts to include what they will cover in the calendar event description and optionally a document to track.

Some ideas of what one can expect at a Senior/Staff Support Engineers'

- Troubleshooting a difficult ticket
- Trying out a GitLab feature (Geo, CI, SAST, k8s, etc.) or a new workflow
- Reproducing a particular bug
- Fixing a bug
- Creating or updating documentation
- Thinking through a particular problem
- Hosting a ticket crush session

#### Meeting Roles
##### Role of the Chair
{: .no_toc}
The main role of the chair is to start the meeting, keep the meeting moving along, and end the meeting when appropriate. There is generally little preparation required, but depending on the meeting, the chair will include choosing a "feature of the week" or similar. Please check the agenda template for parts marked as "filled in by chair."

During the meeting, the chair:

* will ensure that each point in the agenda is covered by the listed person,
* may ask the team to move a discussion to a relevant issue when appropriate,
* copy the agenda template for the following week and tag the next chair/secretary.

If a chair is not available, it is their responsibility to find a substitute.

##### Role of the Notetaker
{: .no_toc}
The notetaker should take notes during the meeting and if action is required, creates a comment and assigns it to the appropriate person.

If the notetaker is not available when it is their turn, they should find a substitute.

### Support Week in Review

Every Friday, we do a week in review, inspired by the [greater Engineering organization week in review](https://drive.google.com/drive/u/0/search?q=type:document%20title%20%22Engineering%20week-in-review%22).  You can add topics any time to the [support week in review google document](https://drive.google.com/drive/u/0/search?q=%22Support%20Week%20in%20Review%22%20-archive%20-Senior%20parent:1eBkN9gosfqNVSoRR9LkS2MHzVGjM5-t5).

Any workflow changes or announcements should be shared in the SWIR and we recommend you check at least once a week to stay up to date on recent changes.
Ideally, the information shared here should have a permanent location such as an issue or merge request.

We encourage anyone in the team to share. We currently have the following topics:

* **Actionable**. For items that require a decision to be made or action to be taken (such as, asking for feedback on an issue).
* **Kudos**. Give a special kudos to other team members or highlight something they did.
* **Things to know about**. Share items that you would like to share with the team, like projects you're working on, known bugs, new workflows, cool articles you found, etc.
* **Metrics report**. Review the support metrics for the span of the week.
* **Music/TV/Movie Sharing**. Share and recommend a song, a show, or a movie with the team.
* **Personal Updates /  Weekend Plans / Random**. Share awesome pics from recent trips, what you're up to on the weekend, or something else.

On Fridays at 3pm EST, [Support Slackbot](#support-slackbot) will remind the team with a link to the week in review and we can use a thread there for banter. Bear in mind that chat older than 90 days will no longer be retained.

You can read about how we got this started
[in this issue](https://gitlab.com/gitlab-com/support/support-team-meta/issues/1394).

### Support Stable Counterparts
As a result of our direct interactions with customers, the Support Team occupies a unique position in GitLab that gives us the opportunity to connect product managers with customer feedback. To take advantage of this opportunity, we've adopted a model that is known within GitLab as "Stable Counterparts." In brief, a "stable counterpart" is an assigned, permanent contact for a GitLab Team Member within another function in the company. See the [Stable counterparts](/handbook/leadership/#stable-counterparts) item on the Leadership page, and [An ode to stable counterparts](https://about.gitlab.com/blog/2018/10/16/an-ode-to-stable-counterparts/) for more information.

How do we align Support Team people to the product?
- Development of the product is broken down into [sections, stages and groups; that page](/handbook/product/product-categories/#devops-stages) is the single source of truth about who is performing this role, and what vacancies there are.
- Each section should have a Support manager counterpart.
- Each group should have an individual contributor counterpart.
- Currently (2019-11) to make it easier for product managers, each PM will get the same individual contributor counterpart for all the parts of the product they manage. That might mean a whole stage, multiple groups, or just a single group.

Support Stable Counterparts, briefly, will:
- Subscribe to the pertinent trackers / labels to be aware of new issues.
- Be especially aware of severity::1/severity::2 issues in their areas of the product, including workarounds.
- Surface issues likely to generate tickets to the wider team.
- Attend product meetings in order to understand the priorities/challenges/upcoming features of the product group.
- Be aware of the issues raised by customers pertinent to their category, surfacing and advocating for them.
- Be a subject matter expert in the use of the features they cover.
- Be the owner of any special processes or troubleshooting workflows that might pertain to the features in their group.

Managers will:
- Fill in for team members who are out.
- Facilitate communication between Support and the assigned group.
- Help triage and broadcast important issues in weekly communications with the larger support team.
- Catalyze training materials and sessions.

#### How to be a counterpart

If you're interested in becoming a stable counterpart for a group, please create an MR to add your name under 'Support' for the relevant team on [data/stages.yml](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/stages.yml) and add your name to the list on the [source/includes/product/_categories-names.erb](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/includes/product/_categories-names.erb) file, then assign to your manager.

There's a [video on google drive](https://drive.google.com/drive/u/0/search?q=parent:1-Cb1GVf5KfsehPDtCWjz6Q1u1b4xE37D) (internal link) with some discussion on how to perform the role.

Tips:
- Subscribe to issues at the [group level](https://gitlab.com/groups/gitlab-org/-/issues) to pick up all the projects that make up GitLab.
- There are [labels for each section (devops::) and group (group::)](/handbook/product/product-categories/#hierarchy). Search for, say, [group::](https://gitlab.com/groups/gitlab-org/-/labels?&search=group%3A%3A) and each label is provided with a link to a view of issues, MRs and epics.
- [Subscribe to labels](/blog/2016/04/13/feature-highlight-subscribe-to-label/) and [set up email notifications.](https://docs.gitlab.com/ee/user/profile/notifications.html)
- For issues only (not MRs or Epics) RSS is also an option. From the issue search results, you can select the RSS feed button to get a custom RSS feed.
- [Engineering team pages](/handbook/engineering/#engineering-departments-sections--teams) usually have information about their labels, slack channels, boards, and meetings under 'useful links' at the end.
- Consider setting up a coffee chat with the product manager and engineering manager(s). Find out the best way to work with them and their teams.
- Ask for invitations to their synchronous meetings.
- Join the slack channels the teams use.
- Keep an eye out for interesting tickets affecting the groups you are covering for.

### Cross Functional Non-Product Counterparts

Some functions don't fit as cleanly into the Support Stable Counterparts model.  As such we have support counterparts for non-product areas to help surface issues and smooth the interface between such groups.

If you're missing from this list (and want to be on it) please let the Support Managers know in `#support_managers`

If you're on the Support Team and have something you'd like to surface, or would like to attend one of these meetings, feel free to post in `#support_managers`.


| Section    | Group               | Group Contact       | Support Manager     | Support Counterpart        | Frequency                         |
|:----------:|:-------------------:|:-------------------:|:-------------------:|:--------------------------:|:---------------------------------:|
| UX         | UX                  | Christie Lenneville | Lyle Kozloff        | Cynthia Ng                 | weekly team meeting |
| UX         | Docs/Tech Writing   | Mike Lewis          | Tom Atkins          | Cynthia Ng & Greg Myers    | weekly team meeting |
| Production | .com Infrastructure | Dave Smith          | Lyle Kozloff        | Vlad Stoianovici           | every 2 weeks |
| Security   | Abuse               | Roger Ostrander     | Lyle Kozloff        | TBD                        | N/A |
| Security   | Security Operations | Jan Urbanc          | Lyle Kozloff        | Brie Carranza              | N/A |
| Performance| Performance         | Stan Hu             | Lee Matos           | N/A                        | N/A |
| Legal      | Legal               | Robin Schulman      | Lyle Kozloff        | N/A                        | N/A |
| Finance    | Budget              | Chase Wright        | Tom Cooney          | N/A                        | 1x Qtr on budget + once per month |
| Finance    | Accounts            | Cristine Tomago     | TDB                 | N/A                        | N/A |
| PeopleOps  | Recruiting          | Cyndi Walsh         | Tom Cooney          | N/A                        | weekly |
| PeopleOps  | After-hire care     | Jessica Mitchell    | Tom Cooney          | N/A                        | every 2 weeks |
| Sales      | Sales               | TBD                 | Tom Atkins          | N/A                        | weekly on Fri join EMEA scrum |
| Sales      | Customer Success    | Kristen Lawrence    | Tom Atkins          | N/A                        | weekly on Fri join EMEA scrum |
| Sales      | Community Relations | David Planella      | Tom Atkins          | Greg Myers                 | weekly team meeting |
| Meltano    | Meltano             | Danielle Morrill    | Jason Colyer        | TBD                        | N/A |


## Processes

### Updating Support Team documents using merge requests

The Support Team records our institutional knowledge, processes and workflows
in multiple places, such as the handbook and project issues templates. When
updating such documents, make sure to have visible artifacts of approval on
your merge requests before merging even if you have received approval somewhere
else. This avoids the impression of changes being made without any oversight
or accountability.

Artifacts of approval can include:

* Getting a peer or manager to review and merge your MR
* A peer or manager showing their approval using [MR approvals](https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html)
* A peer or manager commenting "looks good to me"

### Support Workflows

- [Support Workflows](/handbook/support/workflows)
   - [Internal Policies and Procedures Wiki](https://gitlab.com/gitlab-com/support/internal-requests/-/wikis/home)
   - [How to Work with Tickets](/handbook/support/workflows/working-on-tickets.html)
   - [How to Submit issues to Product/Development](/handbook/support/workflows/working-with-issues.html)
   - [How to Submit Code to the GitLab Application](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md)
   - [How to Submit Docs when working on customer issues](/handbook/documentation) (see 'docs-first methodology')
- [License & Renewals Workflows](/handbook/support/license-and-renewals/workflows)

### Slack Workflows

Each Slack channel within Support has a number of [Workflows](https://slack.com/help/articles/360035692513-Guide-to-Workflow-Builder) attached to them that are used to provide information to users. The source files for each workflow live in the [slack-workflows](https://gitlab.com/gitlab-com/support/toolbox/slack-workflows) project.

#### Issue Notification

Some workflows are meant to notify the team of new issues created in the relevant project.
In these cases, a [project webhook](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html) passes information to [Zapier](https://zapier.com/app/zaps/folder/210292),
which then sends the information to a Slack workflow.

- `#support_gitlab-com`
    - CMOC [Gitlab Project](https://gitlab.com/gitlab-com/support/dotcom/cmoc-handover/), [Zap](https://zapier.com/app/zap/100087156), [Slack workflow](https://gitlab.com/gitlab-com/support/toolbox/slack-workflows/-/blob/master/cmoc_handover.slackworkflow)
- `#support_licensing-subscription`
    - L&R related internal requests [Gitlab Project](https://gitlab.com/gitlab-com/support/internal-requests/), [Zap](https://zapier.com/app/zap/98925072), [Slack workflow](https://gitlab.com/gitlab-com/support/toolbox/slack-workflows/-/blob/master/support_licensing_subscription_internal_requests.slackworkflow)

#### Emoji Reaction

Providing information by reacting to a message with a specific emoji.

- `#support_managers`
  - [Ticket Escalation](https://gitlab.com/gitlab-com/support/toolbox/slack-workflows/-/blob/master/support_managers_ticket_escalation.slackworkflow) - `:escalate:` - Directs the user to follow the proper procedure to escalate a support ticket or internal issue.
- `#support_gitlab-com`
  - [Ticket Escalation](https://gitlab.com/gitlab-com/support/toolbox/slack-workflows/-/blob/master/support_gitlab_com_ticket_escalation.slackworkflow) - `:escalate:` - Directs the user to follow the proper procedure to escalate a support ticket or internal issue.
  - [Question Redirect](https://gitlab.com/gitlab-com/support/toolbox/slack-workflows/-/blob/master/support_gitlab_com_question_redirect.slackworkflow) - `:leftwards_arrow_with_hook:` - Directs the user to post their question in a more appropriate Slack channel.
  - [Remove Link Preview](https://gitlab.com/gitlab-com/support/toolbox/slack-workflows/-/blob/master/support_gitlab_com_remove_link_preview.slackworkflow) - `:slack:` - Politely asks the user to remove any unfurled link previews in their message.
  - [Welcome](https://gitlab.com/gitlab-com/support/toolbox/slack-workflows/-/blob/master/support_gitlab_com_welcome.slackworkflow) - This automated workflow automatically sends a direct message to new members of the channel that contains helpful information.
- `#support_self-managed`
  - [Ticket Escalation](https://gitlab.com/gitlab-com/support/toolbox/slack-workflows/-/blob/master/support_self_managed_ticket_escalation.slackworkflow) - `:escalate:` - Directs the user to follow the proper procedure to escalate a support ticket or internal issue.
- `#support_licensing-subscription`
  - [Ticket Escalation](https://gitlab.com/gitlab-com/support/toolbox/slack-workflows/-/blob/master/support_licensing_subscription_ticket_escalation.slackworkflow) - `:escalate:` - Directs the user to follow the proper procedure to escalate a support ticket or internal issue.

### Support Fixes

When working on merge requests (on the gitlab-org group) for code or for documentation, apply the
`Support Team Contributions` label so that we can track product contributions by support team
members. An issue is created in the `support-team-meta` issue tracker at the
end of each week with a list of support fixes merged in the past week. View the
[list of summary issues here](https://gitlab.com/gitlab-com/support/support-team-meta/issues?label_name%5B%5D=Support%20Team%20Contributions).

### Zendesk Instances
At GitLab, the Support Team currently manages 2 different [Zendesk Instances](/handbook/support/workflows/zendesk-instances.html):

- GitLab Support Instance:  [gitlab.zendesk.com](https://gitlab.zendesk.com)
- GitLab US Federal Support Instance: [gitlab-federal-support.zendesk.com](https://gitlab-federal-support.zendesk.com)

The customer URL for the Federal Support Instance is [federal-support.gitlab.com](https://federal-support.gitlab.com) which is a CNAME for [gitlab-federal-support.zendesk.com](https://gitlab-federal-support.zendesk.com)

All our Support Engineers have access to the GitLab Support Instance, whereas only Support Engineers who are US Citizens have access to the GitLab US Federal Support Instance.

### Ticketing Style Guide
A collection of best practices and suggestions for [styling and responding to Zendesk tickets](/handbook/support/workflows/how-to-respond-to-tickets.html).

### Time Off

*See the [Support Time Off page](/handbook/support/support-time-off.html)*

### Onboarding

*See the [Support Onboarding page](/handbook/support/training)*

### What if I Feel Threatened or Harassed While Handling a Support Request?
Just as Support Team are expected to adhere to GitLab's [Code of Conduct](/handbook/people-group/code-of-conduct), we also expect customers to treat the Support Team with the same level of respect. The [GitLab Community Code of Conduct](https://about.gitlab.com/community/contribute/code-of-conduct/) outlines the standards to which we hold the wider GitLab Community.

If you notice a threatening or hostile ticket, please tag the [Manager On-Call](/handbook/support/on-call/#manager-on-call) to respond with the following guidelines in our [Statement of Support](/support/#please-dont-use-language-intended-to-threaten-or-harass).

### Improving our processes - 'Active Now' issue board
The Support team use ['support-team-meta' project issues](https://gitlab.com/gitlab-com/support/support-team-meta/issues/) to track ideas and initiatives to improve our processes. The ['Active Now' issue board](https://gitlab.com/gitlab-com/support/support-team-meta/-/boards/580661) shows what we're currently working on. It uses three labels:

1. **Blocked** - waiting for another team or external resource before we can move ahead
1. **Discussing this week** - under active discussion to arrive at a decision
1. **In Progress** - actively being worked on

Some principles guide how these labels are used:

1. A **maximum of six issues** at any time for each label (18 total issues)
1. All issues with one of the above labels must be **assigned** to one or more support team members
1. All issues with one of the above labels must have a **due date** no longer than a week ahead
1. If an issue is too big to complete in a week it should be **split into smaller parts that can be completed in a week** (a larger 'parent' issue is OK to keep in the project, but it shouldn't make it onto the 'In Progress' column)

**Each week we look at the board and discuss the issues to keep things moving forward.**

By keeping a maximum of six issues for each label, we **limit work in progress** and make sure things are completed before starting new tasks.

**Adding and managing items on the board:**

Support managers will regularly review the board to keep items moving forward.

1. The team can **vote on issues not on the board** by giving a 'thumbs up' emoji so we can see [popular issues](https://gitlab.com/gitlab-com/support/support-team-meta/issues?sort=popularity&state=opened).
1. Support managers will look at popular issues and add them to the board when there is room.
1. Support managers will **curate** issues to prevent a large backlog. Unpopular or old issues can be closed / merged to keep the backlog manageable.

#### Support Slackbot
The [Support Slackbot](https://gitlab.com/gitlab-com/support/support-ops/gitlab-support-bot) can be used to quickly view the status of our ZenDesk queues.

Below are the most common commands, the [bot's repo](https://gitlab.com/gitlab-com/support/support-ops/gitlab-support-bot) has additional details.

``` rb
sb p # All queues
sb sm # Self-Managed queues
sb s # GitLab.com queues
```

## <i class="fas fa-book fa-fw icon-color font-awesome" aria-hidden="true"></i> Support Resources

### Handbook links

- [GitLab Team page](/company/team/)
- [Product Categories](/handbook/product/categories) - Find out what team handles what
- [Statement Of Support](https://about.gitlab.com/support/statement-of-support.html)
- [Support Managers](/handbook/support/managers)
- [Support Hiring](/handbook/support/managers/hiring.html)
- [Support Channels](/handbook/support/channels)
- [On-Call](/handbook/on-call/)
- [License & Renewals](/handbook/support/license-and-renewals)
- [Support Ops](/handbook/support/support-ops)
- [Advanced Topics](/handbook/support/advanced-topics)

### Documentation

- GitLab
  - [GitLab.com Status](https://status.gitlab.com/)
  - [GitLab Releases](/blog/categories/releases/)
- Writing docs
  - [GitLab Documentation guidelines](https://docs.gitlab.com/ee/development/documentation/index.html)
  - [Documentation Style Guide](https://docs.gitlab.com/ee/development/documentation/styleguide.html)
  - [GitLab Markdown](https://docs.gitlab.com/ee/user/markdown.html)
- Setting up GitLab
  - [GitLab Architecture Overview](https://docs.gitlab.com/ee/development/architecture.html)
  - [Requirements](https://docs.gitlab.com/ee/install/requirements.html)
  - [Installation methods for GitLab](/install/)
  - [Backing up and restoring GitLab](https://docs.gitlab.com/ee/raketasks/backup_restore.html)
  - [Omnibus configuration settings](https://docs.gitlab.com/omnibus/settings/README.html)
  - [Omnibus Configuration options](https://docs.gitlab.com/omnibus/settings/configuration.html)
  - [Omnibus Database settings](https://docs.gitlab.com/omnibus/settings/database.html#seed-the-database-fresh-installs-only)
- Debugging GitLab
  - [Log system](https://docs.gitlab.com/ee/administration/logs.html)
  - [Rake tasks](https://docs.gitlab.com/ee/raketasks/README.html)
  - [Maintenance Rake Tasks](https://docs.gitlab.com/ee/administration/raketasks/maintenance.html)
  - [Debugging Tips](https://docs.gitlab.com/ee/administration/troubleshooting/debug.html)
  - [Debugging resources for GitLab Support Engineers](https://docs.gitlab.com/ee/administration/index.html#support-team-docs)
  - [GitLab Rails Console Cheat Sheet](https://docs.gitlab.com/ee/administration/troubleshooting/gitlab_rails_cheat_sheet.html)
- GitLab features
  - [Install GitLab Runner](https://docs.gitlab.com/runner/install/)
  - [GitLab CI example projects](https://gitlab.com/gitlab-examples)
  - [Elasticsearch](https://docs.gitlab.com/ee/integration/elasticsearch.html)
  - [Connecting GitLab with a Kubernetes cluster](https://docs.gitlab.com/ee/user/project/clusters/)
- Developing GitLab
  - [GitLab development utilities](https://docs.gitlab.com/ee/development/utilities.html)
  - [Feature flags](https://docs.gitlab.com/ee/development/feature_flags.html)
  - [What requires downtime?](https://docs.gitlab.com/ee/development/what_requires_downtime.html)

### Internal tools

- [Support Toolbox](https://gitlab.com/gitlab-com/support/toolbox) - Includes tools such as `json_stats` (analyze JSON logs), `strace_parser` (analyze `strace` output), `gitlabsos` (get all logs and other data from customers), etc.
- [Dev Resources](https://gitlab.com/gitlab-com/dev-resources) - Create a test instance
- [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit)
- [LicenseDot](https://license.gitlab.com/) - Details of all licenses
- [CustomersDot admin](https://customers.gitlab.com/admins)
- [GitLab Regressions](https://regressions.gitlab.io/)

### External tools

- [ExplainShell](https://explainshell.com/) - Break down a terminal command

### Useful Browser Extensions

- Copy As Markdown - Used to copy the element in current page as markdown format ([Chrome](https://chrome.google.com/webstore/detail/copy-as-markdown/dgoenpnkphkichnohepecnmpmihnabdg?hl=en)/[Firefox](https://addons.mozilla.org/en-US/firefox/addon/copy-as-markdown/))
- Zendesk Download Router - Automatically routes Zendesk downloads into separate folders by ticket number ([Chrome](https://chrome.google.com/webstore/detail/zendesk-download-router/pgfhacdbkdeppdjgighdeejjfneifkml)/[Firefox](https://addons.mozilla.org/en-GB/firefox/addon/zendesk-download-router/)/[Opera](https://addons.opera.com/en-gb/extensions/details/zendesk-download-router/))
- GitLab Web Debugger - Aides in identifying the root cause of page load errors on GitLab.com and internal GitLab instances ([Chrome](https://gitlab.com/gitlab-com/gl-infra/gitlab-web-debugger))
- Zendesk Quicktab - Opens Zendesk tickets in a single browser tab ([Chrome](https://chrome.google.com/webstore/detail/zendesk-quicktab/imgmkpifcfhbfdklogcpdnkohifklebb))
  - Note: Zendesk Quicktab was [removed from the Chrome marketplace in July 2020](https://support.tymeshift.com/hc/en-us/articles/360003993613-Install-Zendesk-Quicktab-Chrome-Extension) while ownership of the extension was in negiations. Please read the [Zendesk support forum thread](https://support.zendesk.com/hc/en-us/community/posts/360001108948/comments/360012160754) for information on the current state of the extension and installing Zendesk Quicktab from the GitHub project.
- Calendly Meeting Scheduling Software - ad-hoc meetings, one-click booking ([Chrome](https://chrome.google.com/webstore/detail/calendly-meeting-scheduli/cbhilkcodigmigfbnphipnnmamjfkipp))
- GitLab Screenshare mode - allows to hide confidential information on your GitLab screen ([Chrome](https://gitlab.com/leipert-projects/gitlab-screenshare-mode#chrome)/[Firefox](https://gitlab.com/leipert-projects/gitlab-screenshare-mode#firefox))
