---
layout: handbook-page-toc
title: "Success Plans"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

----

## Overview

A success plan is a roadmap that connects a customer's business outcomes to GitLab solutions. It is a living document, developed by the TAM.  The input for a success plan comes in the first instance from the value-based conversations with the customer in the presales process and documented in the commend plan, focusing on business needs and metrics. The success plan echoes these goals and metrics, and creates a roadmap for implementation via milestones.  It is the strategic vision for the customer to realize success and value with GitLab.

These are some of the reasons we use a success plan:

- Document the customer’s desired outcomes (e.g., KPIs, problems to solve, benefits to achieve)
- Align GitLab’s product adoption plan to customer outcomes
- Define activity (e.g., change management, training) roadmaps and timeframes for a successful adoption journey
- Develop shared understanding and commitment to the plan between GitLab and customer stakeholders
- Serve as baseline to [track delivery of outcomes and adoption](/handbook/customer-success/tam/stage-adoption/)
- Provide foundation for supporting processes like [health checks](/handbook/customer-success/tam/health-score-triage/) and [Executive Business Reviews](/handbook/customer-success/tam/ebr/)

## Understand the Customer’s Motivation

Before we can develop a success plan, we need to understand what is driving the _customer's_ usage of GitLab. This goes beyond the features they are interested in, and extends to the business value GitLab provides.

- Review the opportunity [Command Plan](/handbook/sales/command-of-the-message/command-plan/) in Salesforce
- Work with the SAL/AE and SA to understand the customer's reasons for purchasing (e.g. [Value Driver](/handbook/sales/command-of-the-message/#customer-value-drivers)) and their expectations or Positive Business Outcomes (PBOs), as well as any potential risks or barriers, key stakeholders, and any other relevant information regarding their desire to use GitLab
- [Prepare discovery questions to discuss with the customer](/handbook/customer-success/tam/success-plans/questions-techniques/) to collect any missing information
- Review [account onboarding](/handbook/customer-success/tam/onboarding/) and [account engagement](/handbook/customer-success/tam/engagement/) for additional information that is useful to collecting information and developing a relationship

## Talk with the Stakeholders

All stakeholders should agree on the business objectives that the customer is pursuing, which products and services will help them reach that goal, and to keep the scope focused to 1-3 objectives. As you close out objectives, you can restart the process to define and add new ones.

[Prepare questions](/handbook/customer-success/tam/success-plans/questions-techniques/) to validate your existing knowledge of the customer's business outcomes and pain points, and learn about business outcomes you don't know yet.

For each business objective, review with the customer:

- What is the objective's priority relative to the others'?
- Which stakeholders will benefit?
- How should we prioritize efforts for your teams?
- Are there any quick wins?
- Are there deadlines for individual steps or overall completion?
- Key contributions & responsibilities of everyone involved

## Success Plans are Living Documents

It's important to share progress with everyone involved as time goes on. The Sales team and the customer should both be kept up to date on where the success plan objectives and tasks stand, so they can continue working on new tasks and in turn sharing the progress with anyone else they think should be aware.

A customer’s business and strategies will change, so the value that they need from you will change with that. To stay up to date, show the success plan to the customer regularly to help keep a fresh understanding of their needs. You can email a report to the customer once a month (or other frequency), listing the objectives and inviting them to reply if they’re out of date. Follow the instructions on [how to share a Gainsight ROI success plan](#share-a-roi-success-plan).

It's also helpful to identify key times when you interact with a customer that would be good opportunities to review and refresh the success plan. This would ideally be when a discussion of business goals feels appropriate and the right stakeholders are at the table, for example: key handoffs between teams, [EBRs](/handbook/customer-success/tam/ebr/), or executive check-ins.

It's recommended for TAMs to use [EBRs](/handbook/customer-success/tam/ebr/) and/or other recurring meetings such as [cadence calls](/handbook/customer-success/tam/cadence-calls/)) to review steps achieved thus far and set next steps or new objectives as needed. After these meetings, it's important to log an "Activity" in the relevant objective to record how the customer is trending towards their goals. Before closing an objective, get confirmation from a customer (ideally in writing) that it has been achieved and include that in the activity log.

To keep track that the success plan is up to date, use the custom date field on the Objective in the success plan, “Last Validated”. The TAM will update it when they get confirmation from the customer that it is still a business priority for them.

Internally, TAMs can use the data to track their own trends and objectives achieved over time (e.g. quarter over quarter reports) and use the progress of the success plan to measure the ROI health scorecard.

## ROI Success Plan

The ROI success plan is the "public-facing" plan that we develop and maintain in collaboration with the customer. It is intended to be shared between GitLab and the customer, and should be considered the foundation of our strategic engagement and the reference for an [Executive Business Review](/handbook/customer-success/tam/ebr/).

This success plan contains a few key components that combine to provide a summary of the customer, their business outcomes, and what we are doing to achieve them with GitLab.

**Note:** as you read through each of the sections, you'll likely notice that each part is described as part of a living document - this is intentional! A success plan is **not** a "set it and forget it" exercise. It should be something that the TAM maintains on an ongoing basis, and [iterates](/handbook/values/#iteration) as they learn new information.

### Strategy

The strategy section is a high-level overview of what **GitLab** is doing with the customer. At a glance, the reader should be able to see what we plan to do with the customer to drive value and success, and also to improve adoption and growth. This does not need to be as detailed as the [objectives](#objectives), but it should still let the reader understand what our overall plan is.

### Highlights

Customer highlights, like the [strategy section](#strategy), is a high-level overview of the **customer**. This can include:

- The business the customer is in
- Major focuses, interests, pain points, etc.
- Our relationship with the customer
- Risk factors to their continued relationship with us

Reviewing the highlights, the reader should be able to quickly understand the customer's business, why they bought GitLab, and what they are interested in achieving.

### Objectives

Objectives are at the heart of a strong success plan, because they describe both the intended business outcomes and the actions we will take to achieve those outcomes. Well-crafted objectives provide an actionable plan that resonates with everyone involved, both within GitLab and on the customer side.

A good objective should contain three components:

- The customer's stated business outcome (the "why")
- The end state we will deliver (the "what")
- Metrics to determine whether we've succeeded

With these three elements, you can develop an objective that allows you to track progress on the actual implementation, while measuring progress and tying the results back to a strategic business outcome. You are able to address the needs of the customer's operations team, as well as their business and strategic interests.

#### Objective Categories

There are two types of objectives: **Stage Adoption** and **ROI**.

- **Stage Adoption** objectives are intended to be used only when the end result of the objective is [measurable adoption of a GitLab product stage](/handbook/customer-success/tam/stage-adoption/). For example, if you are going to be driving adoption of GitLab CI (Verify stage) to achieve the stated business outcome, you would categorize the objective as Stage Adoption. Please [reference the enablement material](#open-and-categorize-a-stage-adoption-objective-within-a-success-plan-in-gainsight) for more information, and guidance on creating a stage adoption objective.
- **ROI** objectives are essentially for anything that isn't explicitly stage adoption. Examples include implementing HA architecture, or enabling integration with existing tools.

#### Business Outcomes vs. Value Drivers

When talking to customers about their objectives, we want to [level up the conversation to strategic outcomes](/handbook/customer-success/tam/success-plans/questions-techniques/#leveling-up-from-operationaltechnical-to-strategic). Ideally, the customer will provide us a business outcome in their own words. However, we won't always be able to get that from the customer, and in which case we can use our [value drivers](/handbook/sales/command-of-the-message/#customer-value-drivers) to describe the strategic outcome.

**Tip:** When evaluating the customer's articulated outcome, a good way to know if it's a strategic outcome is whether you can connect it directly to one of our value drivers. In practice, if you can describe the connection between the outcome and a value driver with little to no intermediary explanation, it's likely a strategic outcome.

#### Example Objectives

Now that we've gone through the parts of a good objective, let's review some examples of good ones as well as ones that could use improvement.

##### Good

> **Title:** Reduce SDLC times with higher reliability by implementing HA
>
> **Category:** ROI
>
> **Metrics:** Reduce SDLC time by 30%; Achieve 99.999% uptime; Scale to 5,000 users
>
> **Comments:** The customer's GitLab instance struggles with poor performance and instability, sometimes going offline during peak usage times and costing development teams time, slowing their release cycles.

This example provides the three main components. We have the "what" (higher reliability by implementing HA), the "why" (Reduce SDLC times), and the metrics that will let us determine whether we've achieved the objecive, both strategic (SDLC time reduction target) and operational (uptime and scalability).

> **Title:** Reduce security risk and operating costs by adopting SAST
>
> **Category:** Stage Adoption
>
> **Metrics:** Add SAST scanning to 90% of projects; replace existing scanning tool
>
> **Comments:** The customer wants to adopt the "shift left" security model, and conduct static code scanning earlier in the SDLC. They have an existing tool that runs late in the cycle and is expensive.

Here we have all of the elements, and actually even have two strategic outcomes with one objective! The customer has given us reducing risk and cost reductions (efficiency). Since this is a new part of GitLab that the customer wants to use it's a Stage Adoption objective. We have both a metric related to the adoption across projects, as well as a "binary metric" of replacing a different tool with GitLab. The additional details in the comments help us to shape the narrative of the value we're providing.

> **Title:** Deliver better products faster by adopting GitLab CI
>
> **Category:** Stage Adoption
>
> **Metrics:** All new projects use GitLab CI; 75% of existing projects are migrated to GitLab CI from the existing CI tool within 12 months
>
> **Comments:** The customer's current CI system is cumbersome and requires a lot of administrative overhead to maintain, which slows down the SDLC. They want to replace it with GitLab CI, and conduct a phased migration as well as standardizing on GitLab CI for all new projects.

This objective is using one of our [value drivers](/handbook/sales/command-of-the-message/#customer-value-drivers) to describe the strategic outcome. The metrics allow us to track both new projects and existing projects, but as two separate items since they require a different approach. The comments let us describe additional reasons for doing this.

##### Needs Improvement

> **Title:** GitLab CI
>
> **Category:** Stage Adoption
>
> **Metrics:** Increase GitLab CI usage
>
> **Comments:** Customer wants to use GitLab CI more.

This objective is lacking in general detail, and provides no strategic outcome for the listed action. There is no definitive metric. At best we can call this an operational outcome since it focuses on tool usage, but since we don't know why they want to use it we can't determine whether they're getting the desired result and value.

> **Title:** Improve reliability with HA
>
> **Category:** ROI
>
> **Metrics:** Uptime of 99.99%
>
> **Comments:** Customer is having downtime issues under heavy load, and needs to be able to scale to the demand.

This is a good operational objective, but we have no articulated strategic outcome. This gives us a "what" and metrics, but there's no "why" that lets us show business value. It's a good starting point, but we'd want to dig deeper into the impact of doing this (and of _not_ doing this) on the business.

#### Tasks

The tasks for an objective are the actual steps we will take to acheive the objective. These should be actionable, and they should be granular enough to provide a clear path to success. If your objective is likely going to take a long time and require coordination between a lot of different groups, incorporate the need for check-ins and planning meetings into the tasks, so that you can stay on track by regularly reviewing progress with the customer.

Here is an example of a task list that is too broad:

> - Initial discovery meeting
> - Provide implementation plan
> - Implement GitLab CI

The time between each of these tasks is likely weeks, and there is no detail about _how_ we're actually going to implement CI.

Here's a task list that's a little more actionable and detailed:

> - Initial discussion about adopting GitLab CI
> - Schedule discovery call with stakeholders
> - Complete discovery call
> - Send follow-ups and review the interests discussed during discovery call
> - Schedule implementation plan review meeting
> - Finalize the implementation plan with the customer during review meeting
> - Progress check-in meeting
> - Evaluate metrics from the first phase of implementation
> - Progress check-in meeting
> - Evaluate metrics from the second phase of implementation
> - Progress check-in meeting
> - Complete implementation in accordance with implementation plan
> - Evaluate metrics after implementation plan has been completed

The level of detail provides a step-by-step path to moving the customer from the starting point to completion, and accounts for the need to monitor progress on a regular basis.

## Internal Success Plan

When planning for future initiatives you want to work on with your customer that aren't yet on the roadmap, the internal success plan is the right approach. Unlike the [ROI Success Plan](#roi-success-plan), the internal plan is (as the name suggests) internal to GitLab only. It is for use by the TAM and the rest of the account team to keep track of things you plan to do at a later point.

## Differences between Collaboration Projects and Success Plans

### Collaboration Projects

Collaborations projects are meant for continuous use with our customers to track/create issues, collaborate with product/SAs/others on feature requests, engagements, blockers, etc. The collaboration project is often focused on the tactical, day-to-day, and is more specific to Developers, DevOps Leads, Engineering Managers, and Admins, because they use it to talk about issues to solve as opposed to an overall business objectives.

Within the collaboration project, TAMs focus on new/closed/open issues and collaboration with the customer on these issues. It's the primary communication method for the TAM to communicate with the customer on a day-to-day basis and is a way to help the customers adopt using GitLab.

For additional details, see [Account Engagement](/handbook/customer-success/tam/engagement/).

### Success Plans

Gainsight ROI Success Plans are a separate entity, as they are meant for articulating and tracking business objectives, typically with executive sponsors, decision-makers, and economic buyers. The succcess plans focus on high-level strategic objectives, instead of the technical and tactical initiatives that are covered in the collaboration project.

TAMs use success plans as part of their EBRs to share insights, progress, and objectives to offer deeper insight into their organization and the value of GitLab, as success plans can track progress towards goals and report on it to the executives.

## Gainsight Howto

### Create a Success Plan in Gainsight

[Gainsight](/handbook/customer-success/tam/gainsight/) is the platform we use to develop and manage success plans. Applying the information outlined above, we can follow these steps to create a Gainsight success plan.

**Note:** When a TAM is assigned to an account, a draft ROI Success Plan will be automatically created with a due date 365 days from the day it is created.

To create an additional success plan in Gainsight, perform the following steps:

1. From the Gainsight NXT home page, navigate to the customer's page
1. From the menu on the left-hand side of the screen, choose "Success Plan"
1. In the upper right-hand side, click "+ Success Plan"
1. Add a Name for the success plan, e.g. "Customer Name ROI Success Plan" for customer-facing plans and "Customer Name Stage Adoption Success Plan" for internal plans
1. Add Type as "ROI Success Plan" for customer-facing plans or "Stage Adoption" for internal plans
   1. As noted below, a `Stage Adoption` objective can be listed under an ROI Success Plan if that is an explicit customer goal
1. Set the "Due Date" as end of GitLab's fiscal calendar or a date logical given the content of the success plan
1. Click "Save"

After creating a success plan, you will need to input the objectives that you've collected. Success plans can have multiple objectives, though it’s best to limit it to 1-3 for focus and achievability. Key components of the objectives in the success plan include:

- **Name**: Objective title, such as “Cut Time to First Commit by 20%”
- **Owner**: Who is invested in this objective
- **Due Date**: Due date for the objective
- **Objective Category**:  Indicate if the objective is Stage Adoption, Rev Expansion, or ROI Success
- **Stage Name**: if the objective category above is `Stage Adoption`, then select from this dropdown which stage you're helping the customer adopt
- **Status**: Mark the objective “New” or “Work in Progress”
- **Priority**: Indicate if the priority is Low, Medium, or High
- **Playbook**: If the objective is for Stage Adoption, choose the corresponding playbook (or customize your own!)
- **Success Criteria**: When you first create the Objective (and each time you check in with the customer), log an Activity update on it to capture the progress
- **Comments**: Share any info that might be relevant to the objective (e.g. potential blockers, architecture details, or other important details worth mentioning)

Objectives should be actionable, and Gainsight provides a way to create action items as part of the objective, called tasks. A task in Gainsight is equivalent to a milestone in GitLab's historical success plan terminology. In short, the objective is the goal and the set of tasks are the way to get there (similar to Objectives and Key Results).

To create a task, perform the following steps:

1. Click on the objective so that the side panel appears on the right
1. To the right of the due date, click the three vertical dots, and click "Add Task"
1. Fill out the details of the task:
   - **Name**: The title of the task, such as "Establish baseline metrics for comparision"
   - **Owner**: The person who is responsible for this task
   - **Due Date**: When the task should be completed by
   - **Status**: Is the task ongoing or completed?
   - **Priority**: In relation to other tasks for the objective, how important is it?
   - **Description**: Any additional details that are helpful to understanding what the task is, such as external references, risk factors that could impact the task, etc.
   - **Milestone Details**: Additional details specific to our success plan structure
     - **Customer DRI**: Who on the customer side is responsible for the task?
     - **Milestone Risk**: How significant are the risk factors that could impact being able to complete the task?
     - **Progress (%)**: How far along is the task?

Tasks will affect the overall completion of the objective, and provide more granular visibility into progress on the objective when looking at the Gantt chart. To do this, navigate to the Gantt chart tab (next to the Objectives tab) in the success plan and confirm the representation captures the plan. You can adjust dates accordingly (for example, if a task actually started in the past but the entry defaulted its start date to today's date).

Finally, next to the success plan due date, change the "Status" of the success plan from "Draft" to "Active".

### Open and Categorize a Stage Adoption Objective within a Success Plan in Gainsight

Please review this [3-minute video](https://youtu.be/gWW3t45QCFs) on how to open a stage adoption objective and categorize it correctly in Gainsight to enable reporting on our team's progress (Gitlab only). Video includes:

1. How to determine when to use an ROI Success Plan and when to use a Stage Adoption Success Plan (as defined above)
1. How to create the stage adoption objective
1. How to tag the stage adoption objective within the CTA so that it reflects the correct stage
1. What is not considered stage adoption

### Share a ROI Success Plan

To share a ROI Success Plan, click the link icon next to the success plan due date and status, search for the users you want to share it with, then click "Preview and Send" and send the email. Alternatively, you can export the success plan by clicking "Export" at the top right.
