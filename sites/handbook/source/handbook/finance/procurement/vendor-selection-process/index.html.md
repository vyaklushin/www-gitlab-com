---
layout: handbook-page-toc
title: Vendor Selection Process
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc}

- TOC
{:toc}

## Procurement toolkit
<div class="flex-row" markdown="0" style="height:110px;">
  <a class="btn cta-btn purple" style="width:250px;margin:5px;display:flex;align-items:center;height:100%;">YOU ARE HERE: Vendor selection process</a>  
  <a href="/handbook/finance/procurement/purchase-request-process/" class="btn cta-btn ghost-purple" style="width:250px;margin:5px;display:flex;align-items:center;height:100%;">Purchase request process</a>
  <a href="/handbook/finance/procurement/vendor-guidelines/" class="btn cta-btn ghost-purple" style="width:250px;margin:5px;display:flex;align-items:center;height:100%;"><span style="margin-left: auto; margin-right: auto;">Vendor guidelines</span></a>
</div>

### Vendor Selection Process
Please review the below guidelines:
1. Review the market capabilities defined by your overall spend *before* selecting your vendor.
1. Before sharing details and/or confidential information regarding GitLab business needs, obtain a [Mutual Non-Disclosure Agreement](https://drive.google.com/file/d/1kQfvcnJ_G-ljZKmBnAFbphl-yFfF7W5U/view?usp=sharing) from the potential vendor(s). Refer to the [Signature Authorization Matrix](/handbook/finance/authorization-matrix/) for signing authority. 
   - Create a vendor contract request issue to facilitate this process.
   - The issue creator is responsible for obtaining signature and uploading the signed NDA to Contract Works
1. Use a scorecard to compare vendor capabilities and/or response to RFP.
   - [Supplier Evaluation Scorecard Template](https://docs.google.com/spreadsheets/d/1DLU8NvpX-ksTcoIDxTYansffCLpXGiPHQTZMgh5VUQk/edit#gid=781943083) 
1. All vendors must adhere to the [GitLab Partner Code of Ethics](/handbook/people-group/people-policy-directory/#partner-code-of-ethics). It is mandatory all vendors contractually adhere to this if they would like to do business with us. (Note these are typically not required in event related agreements unless the vendor is providing services).
1. Identify your bid requirements based on your estimated spend:
     >$0-$100K: No bid

     >$101K - $250: 2-3 Bids

     >Greater than $250K: RFP
