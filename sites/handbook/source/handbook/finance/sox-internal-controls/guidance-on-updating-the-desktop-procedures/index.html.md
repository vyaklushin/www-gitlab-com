---
layout: handbook-page-toc
title: "Guidance on Updating Desktop Procedures"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Changes to content in the Desktop Procedure pages
[Link to tutorial video](https://drive.google.com/file/d/1DyAJ3QSwnosS_xsj24pQFhh9dhQfNprj/view?usp=sharing)

1. Changes to the process are made through a merge request and should be assigned to the CFO to merge with the handbook. Please ensure that the Internal Audit team is informed of the the merge in the `#internal_audit` slack channel
1. Based on intimation received by the Internal Audit through the merge request, we will reach out to the Process Owner(s) to schedule a call and understand the new change in process and will perform a design assessment if required. Internal Audit will incorporate the changes in the future audits planned for the process.
1. When changes are made to the process, the existing set of controls will be reviewed by the Process Owner to verify if they stand valid after the process change and which should be confirmed with the Internal Audit team during the call scheduled by Internal Audit team.
1. The Process Owner can work with Internal Audit to make changes to the Internal Controls if any control description is not valid as a result of the change in the process as this will affect any future audits for the process. The change may require any/all of the following actions with respect to internal controls:
    1. Modifications to the exiting Control Descriptions 
    1. Adding new Internal Controls 
    1. Elimination of existing controls since they would no longer be valid
   The Internal Audit team will take a final sign off on the internal control descriptions once the changes have been completed.

### Changes to flowcharts and screenshots in the Desktop Procedure pages
[Link to tutorial video](https://drive.google.com/file/d/1dYK1gCDdLC30koz-cjLQbFIqUW7QvBSc/view?usp=sharing)

1. Links to flowcharts can be found under each sub-process in the Desktop Procedure page. Should the process/control number/control points in the process change, the flowchart must be updated by the Process Owner to reflect the current process per the respective Desktop Procedure page.
1. Links to screenshots that have been embedded within the Desktop Process documentation are stored in a [shared folder](https://drive.google.com/drive/folders/0AP9INZls7LFQUk9PVA) and the access is restricted to GitLab team members only.
1. If any additional screenshots should be added to the Desktop Process, please add the screenshots under the respective sub-process within this [shared folder](https://drive.google.com/drive/folders/0AP9INZls7LFQUk9PVA) and embed the link to the screenshot in the respective Desktop Process page.
1. If any screenshots are to be changed, simply click on the embedded screenshot link in the Desktop Procedure and this will open a Google Sheet where the screenshot can be changed. However, this will not work for the Procure to Pay Desktop Procedure alone, since the screenshots are linked to an image file that would need to be deleted and replaced completely from the [shared folder](https://drive.google.com/drive/folders/0AP9INZls7LFQUk9PVA).
1. If any screenshot files are to be deleted permanently, please remove the respective file from the [shared folder](https://drive.google.com/drive/folders/0AP9INZls7LFQUk9PVA).

If you still have any questions, please reach out to the Internal Audit team via slack channel `#internal_audit`.
