[//]: # TIP: Create the schedule in a temporary spreadsheet, and then copy/paste the rows into an online markdown generator (https://www.google.com/search?q=copy-table-in-excel-and-paste-as-a-markdown-table)

| Date       | Stage groups    |                               |                 |             | Host      |
| ---------- | --------------- | ----------------------------- | --------------- | ----------- | --------- |
| 2020-10-28 | Plan            | Manage                        | Secure & Protect | Monitor     | Jacki     |
| 2020-11-11 | Create          | Release: Progressive Delivery | Growth          | **Open**    | Taurie    |
